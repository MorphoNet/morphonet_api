import os

#READ CURRENT VERSION
API_Version=open('version.txt','r').read().strip()

#UPDATE VERSION IN SETUP.PY
setup=""
for line in open("setup.py","r"):
	if line.find("version")>=0: setup+='    version="'+API_Version+'",\n'
	else : setup+=line

print(" --> API VERSION "+API_Version)
open("setup.py","w").write(setup)

#UPDATE VERSION IN DOC CONF.PY
confpy=""
for line in open("docs/source/conf.py","r"):
	if line.find("release")>=0: confpy+="release='"+API_Version+"'\n"
	else: confpy+=line
open("docs/source/conf.py","w").write(confpy)
