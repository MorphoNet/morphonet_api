Plugins
=======

.. toctree::
   :maxdepth: 10
   
   morphonet.plugins.Seeds
   morphonet.plugins.Watershed
   morphonet.plugins.deletion
   morphonet.plugins.spliting
   morphonet.plugins.temporal

