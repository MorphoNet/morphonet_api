Segmentation from Seeds Plugins
===============================

.. image:: ../../morphonet/plugins/icons/Wata.png
  :width: 50

Wata plugin
-----------

.. image:: ../../morphonet/plugins/images/Wata.png
  :width: 600

.. automodule:: morphonet.plugins.SegmentationFromSeeds.Wata
   :members:
   :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Wati.png
  :width: 50

Wati plugin
-----------

.. image:: ../../morphonet/plugins/images/Wati.png
  :width: 600

.. automodule:: morphonet.plugins.SegmentationFromSeeds.Wati
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Watio.png
  :width: 50

Watio plugin
------------

.. image:: ../../morphonet/plugins/images/Watio.png
  :width: 600

.. automodule:: morphonet.plugins.SegmentationFromSeeds.Watio
   :members:
   :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Wato.png
  :width: 50

Wato plugin
-----------

.. image:: ../../morphonet/plugins/images/Wato.png
  :width: 600

.. automodule:: morphonet.plugins.SegmentationFromSeeds.Wato
  :members:
  :show-inheritance:
