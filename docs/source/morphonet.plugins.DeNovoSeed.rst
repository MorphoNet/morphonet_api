De Novo Seed plugins
====================

.. image:: ../../morphonet/plugins/icons/Seedax.png
  :width: 50

Seedax plugin
-------------

.. image:: ../../morphonet/plugins/images/Seedax.png
  :width: 600

.. automodule:: morphonet.plugins.DeNovoSeed.Seedax
   :members:
   :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Seedero.png
 :width: 50

Seedero plugin
--------------

.. image:: ../../morphonet/plugins/images/Seedero.png
  :width: 600

.. automodule:: morphonet.plugins.DeNovoSeed.Seedero
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Seedin.png
  :width: 50

Seedin plugin
-------------

.. image:: ../../morphonet/plugins/images/Seedin.png
  :width: 600

.. automodule:: morphonet.plugins.DeNovoSeed.Seedin
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Seedio.png
   :width: 50

Seedio plugin
-------------

.. image:: ../../morphonet/plugins/images/Seedio.png
  :width: 600

.. automodule:: morphonet.plugins.DeNovoSeed.Seedio
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Seedis.png
  :width: 50

Seedis plugin
-------------

.. image:: ../../morphonet/plugins/images/Seedis.png
  :width: 600

.. automodule:: morphonet.plugins.DeNovoSeed.Seedis
  :members:
  :show-inheritance:
