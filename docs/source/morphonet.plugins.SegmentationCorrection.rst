Segmentation correction Plugins
===============================

Delete plugin
-------------

.. image:: ../../morphonet/plugins/images/Delete.png
  :width: 600

.. automodule:: morphonet.plugins.SegmentationCorrection.Delete
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Deli.png
  :width: 50

Deli plugin
-----------

.. image:: ../../morphonet/plugins/images/Deli.png
  :width: 600

.. automodule:: morphonet.plugins.SegmentationCorrection.Deli
   :members:
   :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Disco.png
  :width: 50

Disco plugin
------------

.. image:: ../../morphonet/plugins/images/Disco.png
  :width: 600

.. automodule:: morphonet.plugins.SegmentationCorrection.Disco
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Fuse.png
  :width: 50

Fuse plugin
-----------

.. image:: ../../morphonet/plugins/images/Fuse.png
  :width: 600

.. automodule:: morphonet.plugins.SegmentationCorrection.Fuse
   :members:
   :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Gaumi.png
  :width: 50

Gaumi plugin
------------

.. image:: ../../morphonet/plugins/images/Gaumi.png
  :width: 600

.. automodule:: morphonet.plugins.SegmentationCorrection.Gaumi
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Splax.png
  :width: 50

Splax plugin
------------

.. image:: ../../morphonet/plugins/images/Splax.png
  :width: 600

.. automodule:: morphonet.plugins.SegmentationCorrection.Splax
   :members:
   :show-inheritance:

.. image:: ../../morphonet/plugins/icons/CopyPaste.png
  :width: 50

Copy-paste plugin
-----------------

.. image:: ../../morphonet/plugins/images/CopyPaste.png
  :width: 600

.. automodule:: morphonet.plugins.SegmentationCorrection.CopyPaste
 :members:
 :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Match.png
  :width: 50

Match plugin
------------

.. image:: ../../morphonet/plugins/images/Match.png
  :width: 600

.. automodule:: morphonet.plugins.SegmentationCorrection.Match
  :members:
  :show-inheritance:
