Edit temporal links Plugins
===========================

.. image:: ../../morphonet/plugins/icons/Addlink.png
  :width: 50

Addlink Plugin
--------------

.. image:: ../../morphonet/plugins/images/Addlink.png
  :width: 600

.. automodule:: morphonet.plugins.EditTemporalLinks.Addlink
   :members:
   :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Delink.png
  :width: 50

Delink Plugin
-------------

.. image:: ../../morphonet/plugins/images/Delink.png
  :width: 600

.. automodule:: morphonet.plugins.EditTemporalLinks.Delink
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Tracko.png
  :width: 50

Tracko Plugin
-------------

.. image:: ../../morphonet/plugins/images/Tracko.png
  :width: 600

.. automodule:: morphonet.plugins.EditTemporalLinks.Tracko
  :members:
  :show-inheritance:
