.. MorphoNet documentation master file, created by
   sphinx-quickstart on Fri Sep 18 14:36:10 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MorphoNet's API documentation!
=========================================

`MorphoNet <https://www.morphonet.org/>`_ is an interactive anatomical browser for 3D, 3D+t segmented datasets. It provides a comprehensive palette of interaction tools to explore the structure, dynamics and variability of biological shapes, from fossils to developing embryos. Some datasets are also linked to single-cell gene expression data. MorphoNet handles both natural or simulated morphological data and fills a gap which has until now limited the quantitative understanding of morphodynamics and its genetic underpinnings by contributing to the creation of morphological atlases.

In order to interact with MorphoNet, we provide a python Application Programming Interface.

This API has two main modules :

* **Net** : provides various functions to upload data and interact with the MorphoNet database (stored in the morphonet.org server)
* **Plot** : provides various functions to directly plot your own data on the MorphoNet windows without sending anything to the MorphoNet database

It also has a **tools** module, which contains several useful functions for the API, most of which you can use outside of the Net or Plot modules.

Installation
============

This API works with python 3.

You can find more information on how to install python `here <https://www.python.org/downloads/>`_

You also have to install the python package installer : `pip <https://pypi.org/project/pip/>`_.

The MorphoNet Package requires several librariries that are automatically installed :

    * `numpy <https://pypi.org/project/numpy/>`_
    * `scikit-image <https://pypi.org/project/scikit-image/>`_
    * `scipy <https://pypi.org/project/scipy/>`_
    * `vtk  <https://pypi.org/project/vtk/>`_ (to convert the segmentation in meshes)
    * `requests <https://pypi.org/project/requests/>`_ (to interrogate the morphonet server)
    * `imageio <https://pypi.org/project/imageio/>`_ (for Image Handling)
    * `nibabel <https://pypi.org/project/nibabel/>`_ (to load and save in nii format)

The installation should be simple using this command line : ::

  pip install morphonet

For any information go on https://pypi.org/project/morphonet/


.. toctree::
   :maxdepth: 2

   morphonet.pagenet



.. toctree::
   :maxdepth: 2

   morphonet.pageplot

.. toctree::
   :maxdepth: 2

   morphonet.pagedata

.. toctree::
  :maxdepth: 2

  morphonet.pagetutorials



.. toctree::
   :maxdepth: 2

   morphonet.pageexamples
