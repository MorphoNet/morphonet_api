Plugins
=======

.. toctree::
   :maxdepth: 10

   morphonet.plugins.DeNovoSegmentation
   morphonet.plugins.DeNovoSeed
   morphonet.plugins.SegmentationFromSeeds
   morphonet.plugins.SegmentationCorrection
   morphonet.plugins.ShapeTransform
   morphonet.plugins.EditTemporalLinks
   morphonet.plugins.PropagateSegmentation
