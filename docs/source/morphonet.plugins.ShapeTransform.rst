Shape Transform Plugins
=======================

.. image:: ../../morphonet/plugins/icons/Close.png
  :width: 50

Close plugin
------------

.. image:: ../../morphonet/plugins/images/Close.png
  :width: 600

.. automodule:: morphonet.plugins.ShapeTransform.Close
   :members:
   :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Convex.png
 :width: 50

Convex plugin
-------------

.. image:: ../../morphonet/plugins/images/Convex.png
 :width: 600

.. automodule:: morphonet.plugins.ShapeTransform.Convex
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Dilate.png
 :width: 50

Dilate plugin
-------------

.. image:: ../../morphonet/plugins/images/Dilate.png
 :width: 600

.. automodule:: morphonet.plugins.ShapeTransform.Dilate
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Erode.png
 :width: 50

Erode plugin
------------

.. image:: ../../morphonet/plugins/images/Erode.png
 :width: 600

.. automodule:: morphonet.plugins.ShapeTransform.Erode
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Open.png
 :width: 50

Open plugin
-----------

.. image:: ../../morphonet/plugins/images/Open.png
 :width: 600

.. automodule:: morphonet.plugins.ShapeTransform.Open
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Deform.png
  :width: 50

Deform plugin
-------------

.. image:: ../../morphonet/plugins/images/Deform.png
  :width: 600

.. automodule:: morphonet.plugins.ShapeTransform.Deform
   :members:
   :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Delete.png
 :width: 50
