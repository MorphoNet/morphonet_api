Propagate segmentation Plugins
==============================

.. image:: ../../morphonet/plugins/icons/Propa.png
  :width: 50

Propa Plugin
------------

.. image:: ../../morphonet/plugins/images/Propa.png
  :width: 600

.. automodule:: morphonet.plugins.PropagateSegmentation.Propa
   :members:
   :show-inheritance:


.. image:: ../../morphonet/plugins/icons/Prope.png
  :width: 50

Prope Plugin
------------

.. image:: ../../morphonet/plugins/images/Prope.png
  :width: 600

.. automodule:: morphonet.plugins.PropagateSegmentation.Prope
  :members:
  :show-inheritance:
