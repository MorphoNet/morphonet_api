De Novo Segmentation plugins
============================

.. image:: ../../morphonet/plugins/icons/CellposePredict.png
  :width: 50

Cellpose-Predict plugin
-----------------------

.. image:: ../../morphonet/plugins/images/CellposePredict.png
  :width: 600

.. automodule:: morphonet.plugins.DeNovoSegmentation.CellposePredict
   :members:
   :show-inheritance:

.. image:: ../../morphonet/plugins/icons/CellposeTrain.png
  :width: 50

Cellpose-Train plugin
---------------------

.. image:: ../../morphonet/plugins/images/CellposeTrain.png
  :width: 600

.. automodule:: morphonet.plugins.DeNovoSegmentation.CellPoseTrain
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Mars.png
  :width: 50

Mars plugin
-----------

.. image:: ../../morphonet/plugins/images/Mars.png
  :width: 600

.. automodule:: morphonet.plugins.DeNovoSegmentation.Mars
  :members:
  :show-inheritance:


.. image:: ../../morphonet/plugins/icons/StardistPredict.png
  :width: 50

Stardist-Predict plugin
-----------------------

.. image:: ../../morphonet/plugins/images/StardistPredict.png
  :width: 600

.. automodule:: morphonet.plugins.DeNovoSegmentation.StardistPredict
 :members:
 :show-inheritance:

.. image:: ../../morphonet/plugins/icons/StardistTrain.png
   :width: 50

Stardist-Train plugin
---------------------

.. image:: ../../morphonet/plugins/images/StardistTrain.png
   :width: 600

.. automodule:: morphonet.plugins.DeNovoSegmentation.StardistTrain
   :members:
   :show-inheritance:

.. image:: ../../morphonet/plugins/icons/Binarize.png
   :width: 50

Binarize plugin
---------------

.. image:: ../../morphonet/plugins/images/Binarize.png
  :width: 600

.. automodule:: morphonet.plugins.DeNovoSegmentation.Binarize
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/BinCha.png
  :width: 50

BinCha plugin
-------------

.. image:: ../../morphonet/plugins/images/BinCha.png
  :width: 600

.. automodule:: morphonet.plugins.DeNovoSegmentation.BinCha
  :members:
  :show-inheritance:

.. image:: ../../morphonet/plugins/icons/BinBox.png
  :width: 50

BinBox plugin
-------------

.. image:: ../../morphonet/plugins/images/BinBox.png
  :width: 600

.. automodule:: morphonet.plugins.DeNovoSegmentation.BinBox
  :members:
  :show-inheritance:
