MorphoPlugin
============

.. automodule:: morphonet.plugins.MorphoPlugin
   :members:
   :undoc-members:
   :show-inheritance:


Default Plugins
===============

.. toctree::
   :maxdepth: 10

   morphonet.plugins
