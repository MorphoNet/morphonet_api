
import numpy as np

class Volume(object):

    #def __init__(self):
    #    pass

    def construct(self, originalMesh):
        """ Initialize the volume around the original mesh """
        pass

    def applyTransformation(self, originalData, originalMesh, deformedData, deformedMesh):
        """ Generate a new image wich is the original image deformed by the mesh's transformation """
        pass
