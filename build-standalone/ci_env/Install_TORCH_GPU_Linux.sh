
#Remove previous drivers
sudo apt-get purge nvidia*
sudo apt-get autoremove

#Desactivate GUI
sudo systemctl set-default multi-user.target
sudo reboot 0

#Installl NVIDIA DRIVERS (USING SSH)
#https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&Distribution=Ubuntu&target_version=20.04&target_type=deb_network
sudo sh cuda_12.1.0_530.30.02_linux.run

#Reactivate GUI
sudo systemctl set-default graphical.target
sudo reboot 0

#INSTALLATION REPORT
Please make sure that
 -   PATH includes /usr/local/cuda-12.1/bin
 -   LD_LIBRARY_PATH includes /usr/local/cuda-12.1/lib64, or, add /usr/local/cuda-12.1/lib64 to /etc/ld.so.conf and run ldconfig as root


#CREATE ENVIRONNEMT
#Install Torch with pip : https://pytorch.org/get-started/locally/
pip3 install torch torchvision torchaudio


#Link your cuda lib to morphonet_env
ln -s $HOME/miniconda3/envs/morphonet_env_torchgpu/lib/python3.9/site-packages/torch/lib/libtorch_cuda /home/emmanuelfaure/miniconda3/envs/test/lib/python3.9/site-packages/torch/lib/
ln -s $HOME/miniconda3/envs/morphonet_env_torchgpu/lib/python3.9/site-packages/torch/lib/libtorch_cuda_linalg.so /home/emmanuelfaure/miniconda3/envs/test/lib/python3.9/site-packages/torch/lib/



#For the CI we have to link the environnemnt to the build path
sudo ln -s /home/ci/miniconda3 /builds/
sudo chown -R ci:ci /builds/miniconda3