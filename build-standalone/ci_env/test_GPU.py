import os

import torch

print("torch.cuda.is_available():"+str(torch.cuda.is_available()))
print("torch.version.hip "+str(torch.version.hip ))
prop = torch.cuda.get_device_properties(0)
print(prop)


os.system("nvidia-smi")

import numpy as np
expected = [torch.tensor([1.0]), 2.0, np.array(3.0)]
actual = tuple(expected)
torch.testing.assert_close(actual, expected)


print("Start MorphoNet API TEST")


print(" --> list installed packages ")
import pkg_resources
installed_packages = pkg_resources.working_set
installed_packages_list = sorted(["%s==%s" % (i.key, i.version) for i in installed_packages])
print(installed_packages_list)

print(" --> Import libraries")
from skimage.io import imread,imsave


print(" --> import Torch ")
import torch
which ="GPU" if torch.cuda.is_available() else "CPU"
print(" --> Torch (CellPose,..)  will run on "+which)


print(" --> import CellPose")
from cellpose import models, core
import logging
logging.basicConfig(level=logging.INFO) #To have cellpose log feedback on the terminalk
if  core.use_gpu(): print("cell pose use gpu ")
else : print("cell pose do not use gpu ")


print(" --> im read raw data  ( need raw.tiff in the save folder )")
rawdata = imread('/home/emmanuelfaure/Bureau/raw.tiff')


print(" --> run CellPose")
downsampling = 4
diameter = 30
model_type ="cyto"
t=0
rawdata=rawdata[::downsampling,::downsampling,::downsampling]
print("load the pretrained model "+model_type)
model = models.Cellpose(gpu=True,  model_type=model_type)

print("predict the segmentation masks with diameter "+str(diameter))
masks = model.eval(rawdata,diameter=diameter,  do_3D=True)
imsave('cellpose.tiff',masks[0])
