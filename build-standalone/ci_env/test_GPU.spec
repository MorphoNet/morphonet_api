# -*- mode: python ; coding: utf-8 -*-
#binaries=[('/builds/miniconda3/envs/morphonet_env/lib/python3.9/site-packages/imagecodecs.libs/libjpeg-c2033c77.so.8','.'),
#          ('/home/emmanuelfaure/miniconda3/envs/morphonet_env_torchgpu/lib/python3.9/site-packages/torch/lib/libtorch*','.'),
#          ('/home/emmanuelfaure/miniconda3/envs/morphonet_env_torchgpu/lib/python3.9/site-packages/nvidia/cudnn/lib/libcudnn*','.'),
#          ('/builds/miniconda3/envs/morphonet_env/lib/libmkl_vml_avx.so.2','.'),
#          ('/builds/miniconda3/envs/morphonet_env/lib/python3.9/site-packages/imagecodecs/*.so','imagecodecs')],

block_cipher = None


a = Analysis(
    ['test_GPU.py'],
    pathex=[],
    binaries=[('C:\\Users\\tlaurent\\Anaconda3\\envs\\py39\\Lib\\site-packages\\imagecodecs\\*','.'),
              ('C:\\Users\\tlaurent\\Anaconda3\\envs\\py39\\Lib\\site-packages\\torch\\lib\\*','.'),
              ('C:\\Users\\tlaurent\\Anaconda3\\envs\\py39\\Lib\\site-packages\\scipy\\special\\*','.')],
    datas=[('../morphonet/plugins/images/*.png','morphonet/plugins/images'),('../morphonet/plugins/icons/*.png','morphonet/plugins/icons')],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    noarchive=False,
)
pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='test_GPU',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
coll = COLLECT(
    exe,
    a.binaries,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='test_GPU',
)
