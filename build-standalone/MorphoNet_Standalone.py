from multiprocessing import freeze_support

if __name__ == "__main__":
    freeze_support()

    #print("Start MorphoNet Standalone")
    import numpy as np
    from subprocess import Popen
    from morphonet import Plot, Net
    from os.path import join, dirname, exists,isfile
    from platform import system
    from http.server import HTTPServer, BaseHTTPRequestHandler
    from morphonet.tools import purge_temporary_images,_get_param, imread, RemoveLastTokensFromPath, fast_convert_to_OBJ, parse_header \
        , convert_temporary_data, printred
    from threading import Thread, Event
    import sys
    from os import _exit, sep, getpid, remove, rename
    from io import BytesIO
    from psutil import Process, process_iter, wait_procs,ZombieProcess,NoSuchProcess
    from shutil import rmtree
    from traceback import format_exc
    from imageio.v3 import imread as imioread
    from imageio.v2 import get_writer
    from glob import glob
    from pathlib import Path
    from re import match
    import traceback
    import json



    #print(" --> finished importing python headers!")

    COMM_SERVER_PORT = 9874
    COMM_SERVER_SEND_PORT = 9873
    LINEAGE_SEND_PORT = 9800
    MORPHONET_SEND_PORT = 9801
    LINEAGE_RECEIVE_PORT = 9802
    MORPHONET_RECEIVE_PORT = 9803


    def check_local_dataset_integrity(dataset_path):
        g = [d for d in dataset_path.glob("[0-9]") if d.is_dir()]
        g.extend([d for d in dataset_path.glob("[0-9][0-9]") if d.is_dir()])
        g.extend([d for d in dataset_path.glob("[0-9][0-9][0-9]") if d.is_dir()])
        for t in g:
            f = [d for d in t.glob("*") if d.suffix in {".npz", ".obj", ".pickle"} or d.is_dir()]
            for file in f:
                if file.is_dir():
                    if not match(".*,[0-9]+", str(file.absolute())):
                        try:
                            rename(file.absolute(), "{},{}".format(file.absolute(), 0))
                        except Exception as e:
                            print("ERROR, could not rename directory {} properly. EXCEPTION: {}".format(file.absolute(),
                                                                                                        e))
                else:
                    if not match(".*_ch[0-9]+", str(file.absolute())):
                        try:
                            fs = str(file.stem) + "_ch0"
                            if len(file.suffixes) > 1:
                                fs = file.stem.replace(file.suffixes[0], '_ch0' + file.suffixes[0])
                            rename(file.absolute(),
                                   str(file.parent) + sep + fs + str(file.suffix))
                        except Exception as e:
                            print("ERROR, could not rename file {} properly. EXCEPTION: {}".format(file.absolute(), e))


    def check_local_files_integrity(dir_path):
        if exists(dir_path):
            pl = Path(dir_path)
            datasets = [f for f in pl.iterdir() if f.is_dir()]
            for d in datasets:
                check_local_dataset_integrity(d)



    class _MorphoLineageServer(Thread):
        def __init__(self, ploti, todo, host="", port=9875):
            Thread.__init__(self)
            self.ploti = ploti
            self.todo = todo
            self.host = host
            self.port = port
            self.server_address = (self.host, self.port)
            self.available = Event()  # For Post Waiting function
            self.lock = Event()
            self.lock.set()
            # prevent init bug
            self.obj = None
            self.cmd = None

        def run(self):  # START FUNCTION
            if self.todo == "send":
                handler = _MorphoLineageSendHandler(self.ploti, self)
            else:  # recieve
                handler = _MorphoLineageRecieveHandler(self.ploti, self)

            self.httpd = HTTPServer(self.server_address, handler)
            self.httpd.serve_forever()

        def reset(self):
            self.obj = None
            self.cmd = None
            self.available = Event()  # Create a new watiing process for the next post request
            self.lock.set()  # Free the possibility to have a new command

        def wait(self):  # Wait free request to plot (endd of others requests)
            self.lock.wait()

        def post(self, cmd, obj):  # Prepare a command to post
            self.lock = Event()  # LOCK THE OTHER COMMAND
            self.available.set()
            self.cmd = cmd
            self.obj = obj

        def stop(self):
            self.lock.set()
            self.available.set()
            self.httpd.shutdown()


    class _MorphoLineageSendHandler(BaseHTTPRequestHandler):

        def __init__(self, ploti, ms):
            self.ploti = ploti
            self.ms = ms

        def __call__(self, *args, **kwargs):  # Handle a request
            super().__init__(*args, **kwargs)

        def _set_headers(self):
            self.send_response(200)
            self.send_header("Access-Control-Allow-Origin", "*")  # To accept request from morphonet
            self.end_headers()

        def do_GET(self):  # NOT USED
            self._set_headers()
            self.wfile.write(b'OK')

        def do_POST(self):
            self.ms.available.wait()  # Wait the commnand available
            try:
                self._set_headers()
                content_length = int(self.headers['Content-Length'])
                command = self.rfile.read(content_length)
                if self.ms.cmd is not None and self.ms.cmd != "":
                    response = BytesIO()
                    response.write(bytes(self.ms.cmd, 'utf-8'))
                    response.write(b';')  # ALWAYS ADD A SEPARATOR
                    if self.ms.obj is not None:
                        response.write(bytes(self.ms.obj, 'utf-8'))
                    self.wfile.write(response.getvalue())
            except Exception as e:
                print("WARNING: during CMD : " + str(self.ms.cmd) + " : Exception is : \n" + str(e))
            self.ms.cmd = ""
            self.ms.obj = None
            self.ms.reset()  # FREE FOR OTHERS COMMAND

        def log_message(self, format, *args):
            return


    class _MorphoLineageRecieveHandler(BaseHTTPRequestHandler):

        def __init__(self, ploti, ms):
            self.ploti = ploti
            self.ms = ms

        def __call__(self, *args, **kwargs):  # Handle a request
            super().__init__(*args, **kwargs)

        def _set_headers(self):
            self.send_response(200)
            self.send_header("Access-Control-Allow-Origin", "*")  # To accept request from morphonet
            self.end_headers()

        def do_GET(self):  # NOT USED
            # "get received command")
            self._set_headers()
            self.wfile.write(b'OK')

        def do_POST(self):
            self._set_headers()
            response = BytesIO()  # Read
            content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
            command = self.rfile.read(content_length)
            action = _get_param(command, "action")
            current_time = int(_get_param(command, "time"))
            objects = _get_param(command, "data")
            if action == "lineage_info":
                self.ploti.send_lineage(objects)
            elif action == "refresh_lineage":
                self.ploti.refresh_lineage()
            elif action == "select_cells":
                self.ploti.send_select_cells(objects)
            elif action == "send_times":
                self.ploti.send_times(objects)
            elif action == "redraw":
                self.ploti.send_redraw()
            elif action == "load_info":
                self.ploti.send_info(objects)
            elif action == "clear_info":
                self.ploti.clear_info(objects)
            elif action == "load_color":
                self.ploti.send_color(objects)
            elif action == "load_hide":
                self.ploti.send_hide(objects)
            elif action == "load_picked":
                self.ploti.send_picked(objects)
            elif action == "back_load_picked":
                self.ploti.back_send_picked(objects)
            elif action == "clear_picked":
                self.ploti.clear_picked(objects)
            else:
                print(">> Did not recognize action : [" + str(action)+"]")

            response.write(bytes("DONE", 'utf-8'))
            self.wfile.write(response.getvalue())

        def log_message(self, format, *args):
            return


    class LineageMediator(Thread):
        def __init__(self, port_send_lineage=LINEAGE_SEND_PORT, port_send_morphonet=MORPHONET_SEND_PORT,
                     port_receive_lineage=LINEAGE_RECEIVE_PORT, port_receive_morphonet=MORPHONET_RECEIVE_PORT):
            self.setup_mediator(port_send_lineage=port_send_lineage, port_send_morphonet=port_send_morphonet,
                                port_receive_lineage=port_receive_lineage,
                                port_receive_morphonet=port_receive_morphonet)

        def setup_mediator(self, port_send_lineage=LINEAGE_SEND_PORT, port_send_morphonet=MORPHONET_SEND_PORT,
                           port_receive_lineage=LINEAGE_RECEIVE_PORT, port_receive_morphonet=MORPHONET_RECEIVE_PORT):
            self.server_send_lineage = _MorphoLineageServer(self, "send",
                                                            port=port_send_lineage)  # Instantiate the local MorphoNet server
            self.server_send_lineage.daemon = True
            self.server_send_lineage.start()

            self.server_send_morphonet = _MorphoLineageServer(self, "send",
                                                              port=port_send_morphonet)  # Instantiate the local MorphoNet server
            self.server_send_morphonet.daemon = True
            self.server_send_morphonet.start()

            self.server_recieve_lineage = _MorphoLineageServer(self, "recieve",
                                                               port=port_receive_lineage)  # Instantiate the local MorphoNet server
            self.server_recieve_lineage.daemon = True
            self.server_recieve_lineage.start()

            self.server_recieve_morphonet = _MorphoLineageServer(self, "recieve",
                                                                 port=port_receive_morphonet)  # Instantiate the local MorphoNet server
            self.server_recieve_morphonet.daemon = True
            self.server_recieve_morphonet.start()
            # self.wait_for_servers()

        def send_message(self, message):
            self.send_to_morphonet("send_message", message)

        def send_lineage(self, lineage_string):
            self.send_to_lineage("lineage_info", lineage_string)

        def send_select_cells(self, cell_list):
            self.send_to_morphonet("select_cells", cell_list)

        def send_lineage_stopped(self):
            self.send_to_morphonet("lineage_stop", "")

        def send_times(self, time):
            self.send_to_lineage("send_times", time)

        def send_redraw(self):
            self.send_to_lineage("redraw", "")

        def clear_info(self, info):
            self.send_to_lineage("clear_info", info)

        def send_info(self, info):
            self.send_to_lineage("load_info", info)

        def send_color(self, info):
            self.send_to_lineage("load_color", info)

        def send_hide(self, info):
            self.send_to_lineage("load_hide", info)

        def send_picked(self, info):
            self.send_to_lineage("load_picked", info)

        def back_send_picked(self, info):
            self.send_to_lineage("back_load_picked", info)

        def clear_picked(self, info):
            self.send_to_lineage("clear_picked", info)

        def refresh_lineage(self):
            self.send_to_morphonet("refresh_lineage", "")

        def send_to_lineage(self, cmd, obj=None):
            """ Send a command to the lineafe window

            Examples
            --------
            >>> mc.send_to_lineage("hello")
            """
            self.server_send_lineage.wait()  # Wait the commnand available
            if cmd is not None:
                cmd = cmd.replace(" ", "%20")
            if obj is not None:
                if type(obj) == str:
                    obj = obj.replace(" ", "%20")
            self.server_send_lineage.post(cmd, obj)

        def send_to_morphonet(self, cmd, obj=None):
            """ Send a command to the 3D viewer

            Examples
            --------
            >>> mc.send_to_morphonet("hello")
            """
            self.server_send_morphonet.wait()  # Wait the commnand available
            if cmd is not None:
                cmd = cmd.replace(" ", "%20")
            if obj is not None:
                if type(obj) == str:
                    obj = obj.replace(" ", "%20")
            self.server_send_morphonet.post(cmd, obj)

        def wait_for_servers(self):
            if self.server_send_morphonet is not None:
                self.server_send_morphonet.join()
            if self.server_recieve_morphonet is not None:
                self.server_recieve_morphonet.join()
            if self.server_send_lineage is not None:
                self.server_send_lineage.join()
            if self.server_recieve_lineage is not None:
                self.server_recieve_lineage.join()

        def kill_servers(self):
            if self.server_send_morphonet is not None:
                self.server_send_morphonet.stop()
                self.server_send_morphonet.httpd.shutdown()
                self.server_send_morphonet.httpd.server_close()
            if self.server_recieve_morphonet is not None:
                self.server_recieve_morphonet.stop()
                self.server_recieve_morphonet.httpd.shutdown()
                self.server_recieve_morphonet.httpd.server_close()
            if self.server_send_lineage is not None:
                self.server_send_lineage.stop()
                self.server_send_lineage.httpd.shutdown()
                self.server_send_lineage.httpd.server_close()
            if self.server_recieve_lineage is not None:
                self.server_recieve_lineage.stop()
                self.server_recieve_lineage.httpd.shutdown()
                self.server_recieve_lineage.httpd.server_close()


    class _SendHandler(BaseHTTPRequestHandler):

        def __init__(self, ms):
            self.ms = ms

        def __call__(self, *args, **kwargs):  # Handle a request
            super().__init__(*args, **kwargs)


        def _set_headers(self):
            self.send_response(200)
            self.send_header("Access-Control-Allow-Origin", "*")  # To accept request from morphonet
            self.end_headers()

        def do_GET(self):  # NOT USED
            self._set_headers()
            self.wfile.write(b'OK')

        def do_POST(self):
            self._set_headers()
            content_length = int(self.headers['Content-Length'])
            command = self.rfile.read(content_length)
            response = BytesIO()
            response.write(bytes(self.ms.cmd, 'utf-8'))
            self.wfile.write(response.getvalue())
            self.ms.cmd = ""

        def log_message(self, format, *args):
            return


    class InputHandler(BaseHTTPRequestHandler):

        def do_GET(self):
            self._set_headers()
            self.wfile.write(b'OK')

        def do_POST(self):

            self._set_headers()
            content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
            command = self.rfile.read(content_length)
            action = _get_param(command, "action")
            objects = _get_param(command, "objects").split(";")
            response = BytesIO()
            if action == "set_dataset":
                if len(objects) >= 3: # 3 and 4 begin min and max time



                    ds = None
                    with open(objects[1],"rb") as jf:
                        data = json.loads(jf.read())
                        for d in data["LocalDatasetItems"]:
                            if d["FullPath"]==objects[0]:
                                ds = d
                                break

                    if ds is not None:

                        print("\n\n\n#################################")
                        print("OPENING DATASET {}".format(ds["Name"]))
                        print("#################################\n")

                        xml = None
                        if ds["XMLFile"] != "":
                            xml = ds["XMLFile"]
                        # TODO : reactivate later
                        import_temp = False
                        import_path=""
                        mintime = ds["MinTime"]
                        maxtime = ds["MaxTime"]
                        if len(objects) >= 5:
                            mintime = int(objects[3])
                            maxtime = int(objects[4])
                        self.server.plot_instance = PlotThread(ds["PortSend"], ds["PortRecieve"], mintime,
                                                               maxtime, ds["SegmentedData"],
                                                               ds["IntensityData"],
                                                               xml, factor=ds["DownScale"],
                                                               background=ds["Background"],
                                                               fullpath=ds["FullPath"],
                                                               name=ds["Name"],
                                                               verbose=int(objects[2]),
                                                               z_factor=ds["ZDownScale"],
                                                               smoothing=ds["Smoothing"],
                                                               smooth_passband=ds["SmoothPassband"],
                                                               smooth_iterations=ds["SmoothIterations"],
                                                               quadric_clustering=ds["QuadricClustering"],
                                                               qc_divisions=ds["QCDivisions"],
                                                               decimation=ds["Decimation"],
                                                               decimate_reduction=ds["DecimateReduction"],
                                                               auto_decimate_threshold=ds["AutoDecimateThreshold"],
                                                               raw_factor=ds["RawDownScale"],
                                                               z_raw_factor=ds["ZRawDownScale"],
                                                               import_temp=import_temp, temp_archive_path=import_path)
                        self.server.plot_instance.daemon = True
                        self.server.plot_instance.start()
                    else:
                        print("Could not parse JSON file to find dataset parameters")
                else:
                    print("Not Enough parameters were passed from the MorphoNet App, Aborting...")

            elif action == "remove_temp_data":
                if len(objects) >= 1:
                    tempfoldername = objects[0]

                    if exists(tempfoldername):
                        rmtree(tempfoldername, ignore_errors=True)

                    try:
                        readyserver = SendServerThread(("localhost", COMM_SERVER_SEND_PORT))
                        readyserver.start()
                        readyserver.post("DELETE_DONE")
                        readyserver.close()
                    except:
                        traceback.print_exc()

                    set_name = Path(tempfoldername).name
                    print("#################################")
                    print("REMOVED DATASET {}".format(set_name))
                    print("#################################")


            elif action == "login_morphonet":
                if len(objects) >= 2:
                    print("logging into morphonet...")
                    # add logon to morphonet
                    self.server.net_instance = Net(token=objects[0], id_user=int(objects[1]))

            elif action == "logout_morphonet":
                print("logging out...")
                self.server.net_instance = None

            elif action == "export_dataset":
                # self.server.net_instance = None
                if len(objects) >= 6:

                    ds = None
                    with open(objects[1], "rb") as jf:
                        data = json.loads(jf.read())
                        for d in data["LocalDatasetItems"]:
                            if d["FullPath"] == objects[0]:
                                ds = d
                                break

                    if ds is not None:

                        print("#################################")
                        print("EXPORTING DATASET {}".format(ds["Name"]))
                        print("#################################")

                        xml = None
                        if ds["XMLFile"] != "":
                            xml = ds["XMLFile"]

                        #TODO : reimplement later
                        """if objects[3].lower() in ["true", "1"]:
                            export_temp = True
                        else:
                            export_temp = False
                        """
                        export_prop_format = int(objects[5])
                        export_skprop_format = int(objects[6])
                        self.server.plot_instance = PlotThread(9999, 9998, ds["MinTime"],
                                                               ds["MaxTime"], ds["SegmentedData"],
                                                               ds["IntensityData"],
                                                               xml, factor=ds["DownScale"],
                                                               name=ds["Name"],
                                                               background=ds["Background"],
                                                               fullpath=ds["FullPath"],
                                                               verbose=int(objects[2]),
                                                               export=True, export_path=objects[3],
                                                               export_format=objects[4],
                                                               export_temp=False, export_prop=export_prop_format,
                                                               export_skprop=export_skprop_format)

                    self.server.plot_instance.daemon = True
                    self.server.plot_instance.start()

            elif action == "recompute_data":
                # self.server.net_instance = None
                if len(objects) >= 3:
                    ds = None
                    with open(objects[1], "rb") as jf:
                        data = json.loads(jf.read())
                        for d in data["LocalDatasetItems"]:
                            if d["FullPath"] == objects[0]:
                                ds = d
                                break

                    if ds is not None:

                        print("#################################")
                        print("RECOMPUTING DATASET {}".format(ds["Name"]))
                        print("#################################")

                        xml = None
                        if ds["XMLFile"] != "":
                            xml = ds["XMLFile"]

                        if objects[3].lower() in ["true", "1"]:
                            bkg = True
                        else:
                            bkg = False

                        if objects[4].lower() in ["true", "1"]:
                            meshgen = True
                        else:
                            meshgen = False

                        if objects[5].lower() in ["true", "1"]:
                            raw = True
                        else:
                            raw = False

                        if objects[6].lower() in ["true", "1"]:
                            seg = True
                        else:
                            seg = False

                        self.server.plot_instance = PlotThread(9999, 9998, ds["MinTime"],
                                                               ds["MaxTime"], ds["SegmentedData"],
                                                               ds["IntensityData"],
                                                               xml, factor=ds["DownScale"],
                                                               name=ds["Name"],
                                                               background=ds["Background"],
                                                               fullpath=ds["FullPath"],
                                                               verbose=int(objects[2]),
                                                               z_factor=ds["ZDownScale"],
                                                               smoothing=ds["Smoothing"],
                                                               smooth_passband=ds["SmoothPassband"],
                                                               smooth_iterations=ds["SmoothIterations"],
                                                               quadric_clustering=ds["QuadricClustering"],
                                                               qc_divisions=ds["QCDivisions"],
                                                               decimation=ds["Decimation"],
                                                               decimate_reduction=ds["DecimateReduction"],
                                                               auto_decimate_threshold=ds["AutoDecimateThreshold"],
                                                               raw_factor=ds["RawDownScale"],
                                                               z_raw_factor=ds["ZRawDownScale"],
                                                               recompute=True,
                                                               export=False,
                                                               recompute_raw=raw,recompute_seg=seg,
                                                               recompute_meshgen=meshgen,recompute_bkg=bkg)

                    self.server.plot_instance.daemon = True
                    self.server.plot_instance.start()
            elif action == "parse_headers":
                if len(objects) >= 1:
                    file_path = objects[0]
                    headers = parse_header(file_path)
                    h_values = [str(h) for h in headers.values()]
                    str_headers = "/".join(h_values)
                    readyserver = SendServerThread(("localhost", COMM_SERVER_SEND_PORT))
                    readyserver.start()
                    readyserver.post("STOP_HEADERS_"+str(str_headers))
                    readyserver.close()

            elif action == "update_local_sets":
                if len(objects) >= 1:
                    json_path = objects[0]
                    print("Uploading local dataset file... Please wait")
                    #completely update local dataset with proper function
                    convert_temporary_data()
                    print("update finished!")


                    readyserver = SendServerThread(("localhost", COMM_SERVER_SEND_PORT))
                    readyserver.start()
                    readyserver.post("STOP_UPDATE_LOCAL_JSON_")
                    readyserver.close()

            elif action == "upload_dataset":
                if self.server.net_instance is not None:
                    if len(objects) >= 4:
                        # init plot instance for dataset:

                        ds = None
                        with open(objects[1], "rb") as jf:
                            data = json.loads(jf.read())
                            for d in data["LocalDatasetItems"]:
                                if d["FullPath"] == objects[0]:
                                    ds = d
                                    break

                        if ds is not None:
                            xml = None
                            if ds["XMLFile"] != "":
                                xml = ds["XMLFile"]

                            print("#################################")
                            print("UPLOADING DATASET {}".format(ds["Name"]))
                            print("#################################")

                            self.server.plot_instance = PlotThread(9999, 9998, ds["MinTime"],
                                                                   ds["MaxTime"], ds["SegmentedData"],
                                                                   ds["IntensityData"],
                                                                   xml, upload=True, export=False, recompute=False,
                                                                   verbose=int(objects[2]),
                                                                   fullpath=ds["FullPath"],
                                                                   factor=ds["DownScale"],
                                                                   background=ds["Background"],
                                                                   name=ds["Name"],
                                                                   new_name=objects[3],
                                                                   z_factor=ds["ZDownScale"],
                                                                   smoothing=ds["Smoothing"],
                                                                   smooth_passband=ds["SmoothPassband"],
                                                                   smooth_iterations=ds["SmoothIterations"],
                                                                   quadric_clustering=ds["QuadricClustering"],
                                                                   qc_divisions=ds["QCDivisions"],
                                                                   decimation=ds["Decimation"],
                                                                   decimate_reduction=ds["DecimateReduction"],
                                                                   auto_decimate_threshold=ds["AutoDecimateThreshold"],
                                                                   raw_factor=ds["RawDownScale"],
                                                                   z_raw_factor=ds["ZRawDownScale"],
                                                                   net_instance=self.server.net_instance)

                        self.server.plot_instance.daemon = True
                        self.server.plot_instance.start()
                else:
                    print("ERROR : tried to upload a dataset while logged off")



            elif action == "show_lineage":
                print("-> Opening lineage window")

                app_path = ""
                short_path = RemoveLastTokensFromPath(self.server.app_path, 2)
                if system() == "Linux":
                    app_path = join(short_path, "LINEAGE", "MorphoNetLineage", "MorphoNetLineage")
                elif system() == "Windows":
                    app_path = join(short_path, "LINEAGE", "MorphoNetLineage", "MorphoNetLineage.exe")

                elif system() == "Darwin":  # macOS
                    short_path = RemoveLastTokensFromPath(self.server.app_path, 4)
                    app_path = join(short_path, "LINEAGE", "MorphoNetLineage.App", "Contents", "MacOS",
                                    "MorphoNetLineage")

                try:

                    self.server.lineage_process = Popen([app_path])

                    if system() == "Linux":
                        process_name = "MorphoNetLineage"
                    elif system() == "Windows":
                        process_name = "MorphoNetLineage.exe"
                    elif system() == "Darwin":  # macOS
                        process_name = "MorphoNetLineage"

                    lineageprocess = find_process_by_name(process_name)
                    if lineageprocess is None:
                        printred(" ERROR: could not find lineage process!")
                    else:
                        subprocesstask = Thread(target=child_process_lineage_manager, args=(lineageprocess,))
                        subprocesstask.start()
                except Exception as e:
                    print("ERROR during show_lineage : " + str(e))
                    traceback.print_exc()



            elif action == "hide_lineage":

                if self.server.lineage_process is not None:
                    print("-> Closing lineage window")
                    try:
                        pro = Process(self.server.lineage_process.pid)
                        if pro is not None:
                            pro.kill()
                            self.server.lineage_process = None
                    except:
                        printred("could not end lineage process! may not exist any more")

                #else:  printred("hide lineage: could not find process!") #The lineage window is just not open...

            elif action == "init_lineage":
                if self.server.server_lineage_thread is None:
                    self.server.server_lineage_thread = LineageMediator(port_send_lineage=9800,
                                                                        port_send_morphonet=9801,
                                                                        port_receive_lineage=9802,
                                                                        port_receive_morphonet=9803)
                else:
                    print("Mediator already started")
            elif action == "end_lineage":
                if self.server.server_lineage_thread is not None:
                    self.server.server_lineage_thread.kill_servers()
                    self.server.server_lineage_thread = None
            elif action == "record_movie":
                if len(objects) >= 1:
                    writer = get_writer(join(objects[0], '.movie.mp4'))
                    for file in self.natural_sort(glob(join(objects[0], 'movie_*.png'))):
                        im = imioread(file)
                        writer.append_data(im)
                    writer.close()
                    for file in self.natural_sort(glob(join(objects[0], 'movie_*.png'))):
                        remove(file)
                    if isfile(join(objects[0], '.movie.mp4')):
                        rename(join(objects[0], '.movie.mp4'),join(objects[0], 'movie.mp4'))

            else:
                print("ACTION DOES NOT EXIST : " + action)

        def natural_sort(self, l):
            import re
            convert = lambda text: int(text) if text.isdigit() else text.lower()
            alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
            return sorted(l, key=alphanum_key)

        def log_message(self, format, *args):
            return

        def _set_headers(self):
            self.send_response(200)
            self.send_header("Access-Control-Allow-Origin", "*")  # To accept request from morphonet
            self.end_headers()


    class ServerThread(Thread):
        def __init__(self, adress_server, input_handler, params, application_path):
            Thread.__init__(self)
            self.adress_server = adress_server
            self.input_handler = input_handler
            self.params = params
            self.application_path = application_path
            self.server = None

        def start(self):
            self.server = HTTPStandaloneServer(self.adress_server, self.input_handler, self.params,
                                               self.application_path)


    class PlotThread(Thread):
        def __init__(self, port_send, port_recieve, begin, end, segment, raw, xml, factor, background, fullpath,
                     export=False, recompute=False, upload=False, name=None,
                     export_path=".", export_format="nii.gz", verbose=1,
                     z_factor=4, raw_factor=4, z_raw_factor=4,
                     smoothing=True, smooth_passband=0.01, smooth_iterations=25,
                     quadric_clustering=True, qc_divisions=1,
                     decimation=True, decimate_reduction=0.8, auto_decimate_threshold=30, import_temp=False,
                     temp_archive_path="", export_temp=False, new_name=None, net_instance=None, export_prop=None,
                     export_skprop=None ,recompute_raw=False, recompute_bkg=False, recompute_meshgen=False,
                     recompute_seg=False):

            Thread.__init__(self)
            self.port_send = port_send
            self.port_recieve = port_recieve
            self.begin = begin
            self.end = end
            self.segment = segment
            self.raw = raw
            self.xml = xml
            self.factor = factor
            self.raw_factor = raw_factor
            self.z_raw_factor = z_raw_factor
            self.mc = None
            self.background = background
            self.fullpath = fullpath
            self.export = export
            self.recompute = recompute
            self.upload = upload
            self.export_path = export_path
            self.export_temp = export_temp
            self.export_format = export_format
            self.verbose = verbose
            # mesh gen params
            self.z_factor = z_factor
            self.smoothing = smoothing
            self.smooth_passband = smooth_passband
            self.smooth_iterations = smooth_iterations
            self.quadric_clustering = quadric_clustering
            self.qc_divisions = qc_divisions
            self.decimation = decimation
            self.decimate_reduction = decimate_reduction
            self.auto_decimate_threshold = auto_decimate_threshold
            self.import_temp = import_temp
            self.temp_archive_path = temp_archive_path
            self.new_name = new_name
            self.net_instance = net_instance
            self.export_prop = export_prop
            self.export_skprop = export_skprop
            self.segname = name
            self.recompute_raw=recompute_raw
            self.recompute_meshgen=recompute_meshgen
            self.recompute_bkg=recompute_bkg
            self.recompute_seg=recompute_seg

        def run(self):
            #check file integrity
            p = Path(self.fullpath).parent
            check_local_files_integrity(p)
            if not self.export and not self.recompute and not self.upload:
                self.mc = Plot(start_browser=False, port_send=self.port_send, port_recieve=self.port_recieve,
                               temp_folder=self.fullpath, full_path=True, verbose=self.verbose, check_version=False)
                self.mc.set_dataset_with_dict(begin=self.begin, end=self.end, background=self.background,
                                    segment_data=self.segment,
                                    raw_data=self.raw,
                                    xml_file=self.xml, import_temp=self.import_temp,
                                    temp_archive_path=self.temp_archive_path, segname=self.segname,
                                    factor=self.factor, z_factor=self.z_factor,
                                    raw_factor=self.raw_factor, z_raw_factor=self.z_raw_factor,
                                    )
                self.mc.set_mesh_parameters(self.factor, self.z_factor,
                                            self.raw_factor,self.z_raw_factor,
                                            self.smoothing,
                                            self.smooth_passband, self.smooth_iterations,
                                            self.quadric_clustering, self.qc_divisions, self.decimation,
                                            self.decimate_reduction, self.auto_decimate_threshold)
                self.mc.curate()
                print("Leaving curation mode for dataset {}".format(self.segname))
            elif self.recompute:
                self.mc = Plot(start_browser=False, port_send=self.port_send, port_recieve=self.port_recieve,
                               temp_folder=self.fullpath, full_path=True, start_servers=False, verbose=self.verbose,
                               check_version=False)
                self.mc.set_dataset_with_dict(begin=self.begin, end=self.end, background=self.background,
                                              segment_data=self.segment,
                                              raw_data=self.raw,
                                              xml_file=self.xml, import_temp=self.import_temp,
                                              temp_archive_path=self.temp_archive_path, segname=self.segname,
                                              factor=self.factor, z_factor=self.z_factor,
                                              raw_factor=self.raw_factor, z_raw_factor=self.z_raw_factor,
                                              )
                self.mc.set_mesh_parameters(self.factor, self.z_factor,
                                            self.raw_factor,self.z_raw_factor,
                                            self.smoothing,
                                            self.smooth_passband, self.smooth_iterations,
                                            self.quadric_clustering, self.qc_divisions, self.decimation,
                                            self.decimate_reduction, self.auto_decimate_threshold)
                rp = self.recompute_bkg or self.recompute_seg
                seg = self.recompute_seg
                raw = self.recompute_raw
                meshes = self.recompute_bkg or self.recompute_meshgen or self.recompute_seg
                self.mc.recompute_data(region_props=rp, meshes=meshes,raw=raw,seg=seg)
                try:
                    readyserver = SendServerThread(("localhost", COMM_SERVER_SEND_PORT))
                    readyserver.start()
                    readyserver.post("STOP_RECOMPUTE")
                    readyserver.close()
                    print("Finished recomputing dataset {}".format(self.segname))
                except:
                    traceback.print_exc()
            elif self.export:
                try:
                    self.mc = Plot(start_browser=False, port_send=self.port_send, port_recieve=self.port_recieve,
                                   temp_folder=self.fullpath, full_path=True, start_servers=False, verbose=self.verbose,
                                   check_version=False)
                    self.mc.set_dataset_with_dict(begin=self.begin, end=self.end, background=self.background,
                                                  segment_data=self.segment,
                                                  raw_data=self.raw,
                                                  xml_file=self.xml, import_temp=self.import_temp,
                                                  temp_archive_path=self.temp_archive_path, segname=self.segname,
                                                  factor=self.factor, z_factor=self.z_factor,
                                                  raw_factor=self.raw_factor, z_raw_factor=self.z_raw_factor,
                                                  )
                    self.mc.dataset.export(self.export_path, self.export_format, export_temp=self.export_temp,
                                           export_prop=self.export_prop, export_skprop=self.export_skprop)
                    self.mc = None
                except Exception as e:
                    print("Error during export : " + str(e))
                    traceback.print_exc()
                try:
                    readyserver = SendServerThread(("localhost", COMM_SERVER_SEND_PORT))
                    readyserver.start()
                    readyserver.post("STOP_EXPORT")
                    readyserver.close()
                    print("Finished exporting dataset {}".format(self.segname))
                except Exception as e:
                    traceback.print_exc()
            elif self.upload:
                try:
                    self.mc = Plot(start_browser=False, port_send=self.port_send, port_recieve=self.port_recieve,
                                   temp_folder=self.fullpath, full_path=True, start_servers=False, verbose=self.verbose,
                                   check_version=False)
                    self.mc.set_dataset_with_dict(begin=self.begin, end=self.end, background=self.background,
                                                  segment_data=self.segment,
                                                  raw_data=self.raw,
                                                  xml_file=self.xml, import_temp=self.import_temp,
                                                  temp_archive_path=self.temp_archive_path, segname=self.segname,
                                                  factor=self.factor, z_factor=self.z_factor,
                                                  raw_factor=self.raw_factor, z_raw_factor=self.z_raw_factor,
                                                  )
                    self.mc.set_mesh_parameters(self.factor, self.z_factor,
                                                self.raw_factor,self.z_raw_factor,
                                                self.smoothing,
                                                self.smooth_passband, self.smooth_iterations,
                                                self.quadric_clustering, self.qc_divisions, self.decimation,
                                                self.decimate_reduction, self.auto_decimate_threshold)
                    if self.new_name is not None and self.net_instance is not None:
                        self.mc.upload(self.new_name, net_instance=self.net_instance,raw_factor=self.raw_factor,
                                       raw_z_factor=self.z_raw_factor)
                    elif self.name is None:
                        print("ERROR: No dataset name given for upload")
                    elif self.net_instance is None:
                        print("ERROR: net_instance in null")
                except Exception as e:
                    print("Error during upload : " + str(e))
                    traceback.print_exc()
                try:
                    readyserver = SendServerThread(("localhost", COMM_SERVER_SEND_PORT))
                    readyserver.start()
                    readyserver.post("STOP_UPLOAD")
                    readyserver.close()
                except:
                    traceback.print_exc()


    class HTTPStandaloneServer(HTTPServer):

        def __init__(self, addr, handler, params, app_path):
            super().__init__(addr, handler)
            self.add_params = params
            self.plot_instance = None
            self.net_instance = None
            self.serving = True
            self.restart = False
            self.add_params = False
            self.app_path = app_path
            self.lineage_process = None
            self.server_lineage_thread = None


    # classes to send initial message that morphonet has been init properly
    class HTTPStandaloneSendServer(HTTPServer):

        def __init__(self, addr, handler):
            super().__init__(addr, handler)


    class SendServerThread(Thread):
        def __init__(self, adress_server):
            Thread.__init__(self)
            self.adress_server = adress_server
            self.input_handler = _SendHandler(self)
            self.server = None

        def start(self):
            self.server = HTTPStandaloneSendServer(self.adress_server, self.input_handler)
            self.server.timeout = 2

        def post_init(self):  # Prepare a command to post
            self.cmd = "INIT"
            self.server.handle_request()

        def post(self, cmd):
            self.cmd = cmd
            self.server.handle_request()

        def close(self):
            # self.server.shutdown()
            self.server.server_close()


    def main_process_manager(process, server):
        wait_procs([process], callback=lambda p: on_main_process_killed(server))


    def on_main_process_killed(server):
        print("process has been killed!")
        # server.shutdown()
        server.server_close()
        if server.lineage_process is not None:
            if server.server_lineage_thread is not None:
                server.server_lineage_thread.kill_servers()
                server.server_lineage_thread = None

            pro = Process(server.lineage_process.pid)
            try:
                pro.kill()
                server.lineage_process = None
            except:
                print("could not kill lineage child server : was probably already killed")
        _exit(0)


    def child_process_lineage_manager(process):
        wait_procs([process], callback=lambda p: on_lineage_process_killed())


    def on_lineage_process_killed():
        try:
            print("Lineage window was closed")
            readyserver = SendServerThread(("localhost", COMM_SERVER_SEND_PORT))
            readyserver.start()
            readyserver.post("LINEAGE_STOP")
            readyserver.close()
        except:
            traceback.print_exc()
        return


    def find_process_by_name(name):
        for proc in process_iter():
            try :
                if proc.name() == name:
                    return proc
            except ZombieProcess:
                pass
            except NoSuchProcess:
                pass
        return None


    try:

        if getattr(sys, 'frozen', False):
            application_path = dirname(sys.executable)
        elif __file__:
            application_path = dirname(__file__)

        process_name = ""
        selfprocess_name = ""
        if system() == "Linux":
            process_name = "MorphoNet"
            selfprocess_name = "MorphoNet_Standalone"
        elif system() == "Windows":
            process_name = "MorphoNet.exe"
            selfprocess_name = "MorphoNet_Standalone.exe"
        elif system() == "Darwin":  # macOS
            process_name = "MorphoNet"
            selfprocess_name = "MorphoNet_Standalone"

        selfexistingprocess = find_process_by_name(selfprocess_name)


        if selfexistingprocess is not None:
            if selfexistingprocess.pid != getpid():
                print(" --> Previous MorphoNet_Standalone process found, killing the process")
                selfexistingprocess.kill()

        pid = -1
        morphonetprocess = find_process_by_name(process_name)

        if morphonetprocess is None:
            print("could not find morphonet parent process : quitting...")
        else:
            pid = morphonetprocess.pid

        if pid != -1:

            purge_temporary_images()

            # send message that morphonet is ready
            try:
                readyserver = SendServerThread(("localhost", COMM_SERVER_SEND_PORT))
                readyserver.start()
                readyserver.post_init()
                readyserver.close()
            except:
                traceback.print_exc()

            subprocessserver = ServerThread(("localhost", COMM_SERVER_PORT), InputHandler, [], application_path)
            subprocessserver.daemon = True
            subprocessserver.start()
            s = subprocessserver.server

            subprocesstask = Thread(target=main_process_manager, args=(morphonetprocess, s))
            subprocesstask.start()

            print("MorphoNet is ready to use!")
            s.serve_forever()

            quit()
    except:

        trace = format_exc()
        print("ERROR FOUND : " + str(trace))

        with open("endlogfile.log", "w") as endlog:
            endlog.write(str(trace))

        _exit(0)
