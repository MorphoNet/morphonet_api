import os,sys
import shutil
from os.path import isfile,join


def mkdir(path):
    os.system("mkdir -p "+path)

def execute(cmd):
    print(cmd)
    return os.system(cmd)==0


def read_secrets(filename="/Users/ci/secrets.txt"):
    if not isfile(filename): filename="/builds/secrets.txt" #LINUX
    if not isfile(filename): filename=join("C:\\","Users","ci","secrets.txt") #WINDOWS
    if not isfile(filename): filename = "/Users/morphoci/secrets.txt"  # M1
    #Read Password and Users
    Secrets={}
    for line in open(filename,"r"):
        Secrets[line.strip().split(";")[0]]=line.strip().split(";")[1]
    return Secrets

def rmrf(path):
    import glob
    folders = glob.glob(path)
    for fold in folders:
        if os.path.exists(fold):
            if os.path.isfile(fold) or os.path.islink(fold):
                os.unlink(fold)
            else:
                res = shutil.rmtree(fold)


def push_build(branch,osmn,pyname,option=' -o ProxyCommand="ssh -o StrictHostKeychecking=no -W %h:%p efaure@jumpbox.lirmm.fr" -o StrictHostKeychecking=no ', server='ci@193.49.107.243',recursive=False):
    deploy_path = "/Users/ci/Desktop/builds/" + str(branch.lower()) + "/" + str(osmn) + "/" #DISTANCE PATH IN THE RUNNER OSX SILLICON
    addr=" -r " if recursive else ""
    if execute('ssh '+option+server+' "mkdir -p ' + deploy_path+'"'):
        if execute("scp "+addr+option+" " + pyname + "  "+server+":" + deploy_path):
            return True
    print(" --> Error pushing the "+pyname+" build to "+server)
    return False

def get_build(branch,osmn,pyname,architecture="",option=' -o ProxyCommand="ssh -o StrictHostKeychecking=no -W %h:%p efaure@jumpbox.lirmm.fr" -o StrictHostKeychecking=no ', server='ci@193.49.107.243'):
    if architecture == "silicon":osmn=architecture
    if architecture == "GPU": osmn =osmn+"_" +architecture
    deploy_path = "/Users/ci/Desktop/builds/" + str(branch.lower()) + "/" + str(osmn) + "/"
    execute('scp '+option+server+':' + join(deploy_path,pyname)+ " "+pyname)
    execute("tar -xf "+pyname)


class bcolors:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def printblue(strs):
    print(bcolors.BLUE + strs + bcolors.ENDC)


def printred(strs):
    print(bcolors.RED + strs + bcolors.ENDC)


def printgreen(strs):
    print(bcolors.GREEN + strs + bcolors.ENDC)


def printyellow(strs):
    print(bcolors.YELLOW + strs + bcolors.ENDC)

def printc(msg, c):
    msg = str(msg)
    if c == 0:  printgreen( msg)
    if c == 1:  print("-> " + msg)
    if c == 2:  printblue("---> " + msg)
    if c == -1: printred("-----> " + msg)
