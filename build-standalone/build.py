import os,sys
from os.path import dirname, basename, isdir, join, isfile
from functions import read_secrets, mkdir, execute, rmrf, push_build, printc
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-o',"--os", default="", help="Which OS (OSX,LINUX,WINDOWS,ANDROID,IOS)", required=True)
parser.add_argument('-b',"--branch", default="", help="Which MorphoNet branch (sable,dev,beta)", required=True)
parser.add_argument('-a',"--architecture", default="amd64", help="Which architecture (silicon or other...)", required=False)
parser.add_argument('-p',"--processor", default="CPU", help="GPU or CPU ? ", required=False)
args = parser.parse_args()
if args.os=="": sys.exit(1)
if args.branch=="": sys.exit(1)
osmn=args.os
branch=args.branch
architecture=args.architecture
if branch=="main": branch="beta"


processor=args.processor

printc(" ->> DEPLOY MORPHONET WITH PYTHON from "+branch+" git branch ",0)

Secrets=read_secrets() #Read Password and Users

# SET PATH STANDALONE
main_path = os.getcwd()
entitlements =  "entitlements.plist"

if osmn=="OSX":
    morphonet_app = "MorphoNet.app"
    lineage_app = "MorphoNetLineage.app"
elif osmn=="LINUX" or osmn=="WINDOWS":
    morphonet_app = "MorphoNet"
    lineage_app = "MorphoNetLineage"

#CLEAN PREVIOUS BUILDS
rmrf("dist")
rmrf("build")
rmrf("../examples") #We don't need the example to build ...
rmrf("../notebooks") #We don't need the example to build ...
#rmrf("MorphoNet_Standalone.spec")

#DEPLOY PATH AND NAME
pyname="Py-MorphoNet-"+osmn.upper()+".tar.gz"


# CREATE THE STANDALONE APPLICATION WITH PY INSTALLER

if osmn=="OSX":
    if architecture == "silicon":
        execute('pyinstaller MorphoNet_Standalone_silicon.spec')
    else:
        execute('pyinstaller MorphoNet_Standalone_osx.spec')
    if not isdir("dist/MorphoNet_Standalone.app"):
        printc(" --> ERROR compilation dist/MorphoNet_Standalone app does not exist ",-1)
        execute("ls -laF dist")
        sys.exit(1)
    rmrf("build")
    execute("cd  dist; tar -cf " + pyname + " MorphoNet_Standalone.app")
    rmrf("dist/MorphoNet_Standalone.app")
    pyname = join("dist", pyname)
    if architecture=="silicon":
        push_build(branch, architecture,pyname)
    else:
        push_build(branch, osmn,pyname)

elif osmn=="LINUX":

    #TO RUN ON RUNNER CIONA WITH GPU
    if processor == "GPU":
        execute('pyinstaller MorphoNet_Standalone_linux_GPU.spec')
        if not isdir("dist/MorphoNet_Standalone"):
            printc(" --> ERROR compilation dist/MorphoNet_Standalone does not exist ",-1)
            execute("ls -laF dist")
            sys.exit(1)
        rmrf("build")
        execute('cd dist/MorphoNet_Standalone/_internal;ln -s nvidia/cudnn/lib/libcudnn_engines_precompiled.so.9 .')
        execute('cd dist/MorphoNet_Standalone/_internal;ln -s nvidia/cudnn/lib/libcudnn_engines_runtime_compiled.so.9 .')
        execute('cd dist/MorphoNet_Standalone/_internal;ln -s nvidia/cudnn/lib/libcudnn_heuristic.so.9 .')

        execute("cd  dist; tar -cf " + pyname + " MorphoNet_Standalone")
        rmrf("dist/MorphoNet_Standalone")
        pyname = join("dist", pyname)
        push_build(branch, osmn+"_"+processor,pyname)
    else:
        execute('pyinstaller MorphoNet_Standalone_linux.spec')
        if not isdir("dist/MorphoNet_Standalone"):
            printc(" --> ERROR compilation dist/MorphoNet_Standalone does not exist ",-1)
            execute("ls -laF dist")
            sys.exit(1)
        rmrf("build")
        execute("cd  dist; tar -cf " + pyname + " MorphoNet_Standalone")
        rmrf("dist/MorphoNet_Standalone")
        pyname = join("dist", pyname)
        push_build(branch, osmn, pyname)

elif osmn=="WINDOWS":
    if processor == "GPU":
        execute('pyinstaller MorphoNet_Standalone_windows_GPU.spec')
        if not isdir(join("dist", "MorphoNet_Standalone")):
            printc(" --> ERROR compilation dist/MorphoNet_Standalone.exec does not exist ",-1)
            execute("dir dist")
            sys.exit(1)
        rmrf("build")
        execute("move dist\MorphoNet_Standalone .")
        execute("tar -cf " + pyname + " MorphoNet_Standalone")
        rmrf("MorphoNet_Standalone")
        push_build(branch, osmn+"_"+processor, pyname)

    else:
        execute('pyinstaller MorphoNet_Standalone_windows.spec')
        if not isdir(join("dist","MorphoNet_Standalone")):
            printc(" --> ERROR compilation dist/MorphoNet_Standalone.exec does not exist ",-1)
            execute("dir dist")
            sys.exit(1)
        rmrf("build")
        execute("move dist\MorphoNet_Standalone .")
        execute("tar -cf " + pyname + " MorphoNet_Standalone")
        rmrf("MorphoNet_Standalone")
        push_build(branch, osmn, pyname)

# CLEAN SOME SPACE
rmrf(pyname)
rmrf("dist")
