# How To Build the MorphoNet Python API ?

Notice : The API is now build automaticaly wit gitlab runner 

##############################################################################################################################
############################################################### LINUX ########################################################
##############################################################################################################################


### How to Install a new gitlab Runner
- In Continous Integration tools (https://ci.inria.fr/project/morphonet/slaves)
- Create a new slave  (ex : Ubuntu 20.LTS  with 2Gb and 2 Cores) 
- Connect to Slave (ssh ci@...)
- - sudo apt-get install curl
  - Download the binary for your system sudo : ```sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64  ```
  - Give it permission to execute : ``` sudo chmod +x /usr/local/bin/gitlab-runner ```
  - 
### IMPORTANT: Runners and users

By default, the gitlab-runner "services" run with a default user called gitlab-runner, which does not really
exist on the host machine. If you want to make it run on a specific user that exists on host machine, make sure you use
the --user and --password arguments. For windows, the command looks as is:

```
.\gitlab-runner.exe install --user ENTER-YOUR-USERNAME --password ENTER-YOUR-PASSWORD
```

(source : https://docs.gitlab.com/runner/install/windows/ for WINDOWS)

  - Create a GitLab Runner user : ```sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash ``` 
  - Install as a service : ```sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner ```
  - Run the service : ``` sudo gitlab-runner start ```
  - To get the token:
    - We need to add the runner in the ci :
      - Go to https://gitlab.inria.fr/groups/MorphoNet/-/runners
      - New Group Runners 
      - Select the command line from Step 1
  - Register the token : ``` sudo gitlab-runner register  --url https://gitlab.inria.fr  --token $REGISTRATION_TOKEN  ```
  - Choose shell as the runner
  - Following : https://docs.gitlab.com/runner/shells/index.html#shell-profile-loading
  - sudo pico /home/gitlab-runner/.bash_logout 
    - comment the lines 
      ```
      if [ "$SHLVL" = 1 ]; then
       [ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q
      fi
      ```
  - Copy ssh keys (id_rsa.pub and id_rsa) in  /home/gitlab-runner/.ssh/
  - sudo chown gitlab-runner:gitlab-runner /home/gitlab-runner/.ssh/id_rsa
  - sudo chown gitlab-runner:gitlab-runner /home/gitlab-runner/.ssh/id_rsa.pub

### Prepare the runner for the API build (log on the ssh ci@ of your machine)
- sudo apt-get install python3-venv #In python3 isn't install by default 
- sudo ln -s /usr/bin/python3 / usr/bin/python
- sudo rm -r /home/gitlab-runner/.bash_logout ( To avoid issue of failing environnment prepration  ) 
- sudo apt install python3-pip 
- sudo ln -s /usr/bin/pip3 /usr/bin/pip 
- pip install virtualenv
- pip install wheel
- sudo apt-get install pandoc # For building the documentation 
- sudo mkdir /home/gitlab-runner/.ssh/  #cReate for gitlab runner user
- sudo chown gitlab-runner:gitlab-runner /home/gitlab-runner/.ssh
- scp id_rsa runnerapi:/builds/ #FROm OUTSIE 
- sudo mv /builds/id_rsa /home/gitlab-runner/.ssh/ (The key correspond to the one on MorphoNet.org)
- sudo chown gitlab-runner:gitlab-runner /home/gitlab-runner/.ssh/id_rsa
- echo "morphonet.org ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILkfXNQw7qNCe0+FjvySGMmaAxIw5wMXX1CEauAJ17hl" > known_hosts # To avoid asking if server is ok )
- sudo mv known_hosts /home/gitlab-runner/.ssh/ 
- sudo chown gitlab-runner:gitlab-runner /home/gitlab-runner/.ssh/known_hosts

### Prepare the runner with Conda (best option..) (log on the ssh ci@ of your machine)
- install conda https://docs.conda.io/en/latest/miniconda.html
- Add the ssh power keys ...
-  sudo apt-get install git



### Install ubuntu builder with GPU :
- Tutos : https://github.com/ashutoshIITK/install_cuda_cudnn_ubuntu_20
- Tutos : https://gist.github.com/ksopyla/bf74e8ce2683460d8de6e0dc389fc7f5
- 
- sudo apt install nvidia-driver-535 #To be sure to have to good driver ...
- Need Cuda 11 to be installed https://developer.nvidia.com/cuda-11-8-0-download-archive?target_os=Linux&target_arch=x86_64&Distribution=Ubuntu&target_version=20.04&target_type=deb_network
    sudo apt install cuda-toolkit-11-8 # FOR ISSUE tensorflow/compiler/xla/stream_executor/platform/default/dso_loader.cc:64] Could not load dynamic library 'libcudart.so.11.0'; dlerror: libcudart.so.11.0: cannot open shared object file: No such file or directory
- export LD_LIBRARY_PATH="/usr/local/cuda/lib64"
- 
- install conda https://docs.conda.io/en/latest/miniconda.html

- FOR ISSUE Could not load dynamic library 'libnvinfer.so.7'; dlerror: libnvinfer.so.7: cannot open shared object file: No such file or directory; LD_LIBRARY_PATH: /usr/local/cuda/lib64
ln -s /home/emmanuelfaure/miniconda3/envs/morphonet_env/lib/python3.9/site-packages/tensorrt_libs/libnvinfer.so.8 /home/emmanuelfaure/miniconda3/envs/morphonet_env/lib/python3.9/site-packages/tensorrt_libs/libnvinfer.so.7
ln -s /home/emmanuelfaure/miniconda3/envs/morphonet_env/lib/python3.9/site-packages/tensorrt_libs/libnvinfer_plugin.so.8 /home/emmanuelfaure/miniconda3/envs/morphonet_env/lib/python3.9/site-packages/tensorrt_libs/libnvinfer_plugin.so.7
$ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/emmanuelfaure/miniconda3/envs/morphonet_env/lib/python3.9/site-packages/tensorrt_libs/

- Need scikit-image==0.22.0 to avoid issue : "FileNotFoundError: /tmp/_MEIYsUTBF/skimage/feature/orb_descriptor_positions.txt not found."

sudo apt-get install cudatoolkit 

#### INSTALL DEV ENVIRONNEMNT  (UBUNTU 20) CPU ONLY
conda create -n morphonet-env python=3.9
conda activate morphonet-env
conda install pytorch torchvision torchaudio cpuonly -c pytorch
pip install cellpose==2.2.3
pip install tensorflow-cpu==2.18.0
pip install stardist==0.9.1
pip install imageio-ffmpeg==0.5.1
pip install nibabel==4.0.2 
pip install vtk==9.2.6
pip install psutil==5.9.5
pip install importlib-resources==5.12.0 
pip install pyinstaller==6.11.1
pip install scikit-learn==1.3.0 
pip install SimpleITK==2.3.1
pip install aicsimageio==4.14.0
pip install "aicsimageio[nd2]"
pip install "aicsimageio[dv]"
pip install "aicspylibczi==3.1.1" 
pip install "readlif==0.6.4"
pip install "aicsimageio[base-imageio]" 
pip install edist==1.2.2
pip install tqdm



#### INSTALL DEV ENVIRONNEMNT  (UBUNTU 20) WITH TORCH GPU 
conda create -n morphonet-env-torchgpu python=3.9
conda activate morphonet-env-torchgpu
pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
pip install "cellpose<3"
pip install tensorflow-cpu==2.18.0
pip install stardist==0.9.1
pip install imageio-ffmpeg==0.5.1
pip install nibabel==4.0.2 
pip install vtk==9.2.6
pip install psutil==5.9.5
pip install importlib-resources==5.12.0 
pip install pyinstaller==6.11.1
pip install scikit-learn==1.3.0 
pip install SimpleITK==2.3.1
pip install aicsimageio==4.14.0
pip install "aicsimageio[nd2]"
pip install "aicsimageio[dv]"
pip install "aicspylibczi==3.1.1" 
pip install "readlif==0.6.4"
pip install "aicsimageio[base-imageio]" 
pip install edist==1.2.2
pip install tqdm



## CANNOT PACKAGE TENSORFLOW GPU (TO HEAVY FOR PYINSTALLER)



#### Create conda init for runner 
- sudo mkdir /builds/
- sudo chown ci:ci /builds
- nano /builds/conda_init.sh 
__conda_setup="$('/home/ci/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
eval "$__conda_setup"
- copy secrets in /builds/secrets

##### GPU 
- in order to have tensorflow working with GPU , install tensorflow localy and then run export LD_LIBRARY_PATH="/home/ci/miniconda3/envs/morphonet_env/lib/python3.9/site-packages/tensorrt_libs




##############################################################################################################################
############################################################### WINDOWS  #####################################################
##############################################################################################################################

### How to Install a new gitlab Runner 
- In Continous Integration tools (https://ci.inria.fr/project/morphonet/slaves)
- Create a new slave  (ex : Windows ...  ) 
- Connect to Slave (ssh ci@...)

### Run PowerShell: 
https://docs.microsoft.com/en-us/powershell/scripting/windows-powershell/starting-windows-powershell?view=powershell-7#with-administrative-privileges-run-as-administrator
- mkdir C:\GitLab-Runner
- cd C:\GitLab-Runner   

### Download binary from the webpage
https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe" 
- Move in  C:\GitLab-Runner
- Rename it in gitlab-runner.exe

### Register the runner (steps below), then run
.\gitlab-runner.exe install
.\gitlab-runner.exe start
.\gitlab-runner.exe register  --url https://gitlab.inria.fr  --token $REGISTRATION_TOKEN # (TOKEN FOUND IN https://gitlab.inria.fr/groups/MorphoNet/-/runners/7455/register?platform=windows )
  -Name it morphonet-py-window
 - Choose powershell as the runner 

### Additional actions
- install git for windows
- install miniconda
 - ajouter dans %PATH% (system, not user + restart machine)
 - conda init powershell
 - attention mettre les variables dans path

## TRES IMPORTANT!!!
pour s'assurer que toutes les variables path soient correctes:
sur powershell:

- cd C:\Windows\System32
- .\where.exe conda
- bien ajouter TOUS les chemins sur le path SYSTEME
dans le cas du omen : 
C:\ProgramData\miniconda3\Library\bin\conda.bat
C:\ProgramData\miniconda3\Scripts\conda.exe
C:\ProgramData\miniconda3\condabin\conda.bat

- créer l'environnement à la main : 
 - conda env create -f python-environment-windows.yml
- remettre les secrets dans le path correspondant
- IMPORTANT : pour installer des paquets python localement, faire attention à être sur un terminal admin, sinon les path seront dans les répertoires de l'utilisateur connecté, ce qui peut faire des incohérences dans l'environnement.



##### INSTALL DEV ENVIRONNEMNT  (WINDOWS) WITH GPU FOR TORCH
###### Duplicate morphonet_env environnemnt 
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu121


#### Change User to Admin (WINDOWS)
-net user administrator /active:yes #From CMD 

##############################################################################################################################
############################################################### OSX  #####################################################
##############################################################################################################################


#### FOR OSX  M1
- install conda https://docs.conda.io/en/latest/miniconda.html
- conda env create -f python-environment-silicon.yml


#### INSTALL DEV ENVIRONNEMNT  (OSX SILICON) WITH GPU FOR TORCH
#https://developer.apple.com/metal/pytorch/
#https://developer.apple.com/metal/tensorflow-plugin/
conda create -n morphonet-env python=3.9
conda activate morphonet-env
conda install pytorch torchvision torchaudio -c pytorch-nightly 
pip install "cellpose<3"
pip install tensorflow==2.15.0
#pip install tensorflow-metal==1.1.0 #NOT WORKING FOR STARDIST 
pip install stardist==0.9.1
pip install imageio-ffmpeg==0.5.1
pip install nibabel==4.0.2 
pip install vtk==9.2.6
pip install psutil==5.9.5
pip install importlib-resources==5.12.0 
pip install pyinstaller==5.12.0 
pip install scikit-learn==1.3.0  #ALREDY INSTALLED BUT DIFFERENT VERSIN 
pip install scipy==1.10.1 #DOWNGRADED SCIPY IS REQUIRED (Error : ModuleNotFoundError: No module named 'scipy.special._cdflib'de)
pip install SimpleITK==2.3.1
pip install aicsimageio==4.14.0
pip install "aicsimageio[nd2]"
pip install "aicsimageio[dv]"
pip install "aicspylibczi==3.1.1" 
pip install "readlif==0.6.4"
pip install "aicsimageio[base-imageio]" #Downgrade scikit image in 0.22.0 
pip install edist==1.2.2
pip install tqdm


#### INSTALL DEV ENVIRONNEMNT  (OSX INTER)
conda create -n morphonet-env python=3.9
conda activate morphonet-env
pip install torch torchvision torchaudio
pip install tensorflow==2.15.0
pip install cellpose==2.2.3
pip install stardist==0.8.5
pip install imageio-ffmpeg==0.5.1
pip install nibabel==4.0.2 
pip install vtk==9.2.6
pip install psutil==5.9.5
pip install importlib-resources==5.12.0 
pip install pyinstaller==6.9.0 
pip install scikit-learn==1.3.0 
pip install SimpleITK==2.3.1
pip install aicsimageio==4.14.0
pip install "aicsimageio[nd2]"
pip install "aicsimageio[dv]"
pip install "aicspylibczi==3.1.1" 
pip install "readlif==0.6.4"
pip install "aicsimageio[base-imageio]" 
pip install edist==1.2.2
pip install tqdm


##### BUILD LOCALY THE STANDALONE
#FOLLOW previous installation to create the environnement
cd morphonet_api
pip install -e . #TO install MorphoNet API inside the environnement
python3 update_version.py
cd build-standalone
rm -rf build; rm -rf dist; 
pyinstaller MorphoNet_Standalone_silicon.spec
cd dist
tar -cf Py-MorphoNet-OSX.tar.gz MorphoNet_Standalone.app
mv Py-MorphoNet-OSX.tar.gz ../../../morphonet_unity/build/builds



#### INSTALL BUILD ENVIRONNEMENT FOR API PIP (without tensorflow,pytorch,cellpose and stardist)
conda create -n morphonet-env python=3.9
conda activate morphonet-env
pip install imageio-ffmpeg==0.5.1
pip install nibabel==4.0.2 
pip install vtk==9.2.6
pip install psutil==5.9.5
pip install importlib-resources==5.12.0 
pip install scikit-learn==1.3.0 
pip install scipy==1.10.1 #DOWNGRADED SCIPY IS REQUIRED (Error : ModuleNotFoundError: No module named 'scipy.special._cdflib'de)
pip install SimpleITK==2.3.1
pip install aicsimageio==4.14.0
pip install "aicsimageio[nd2]"
pip install "aicsimageio[dv]"
pip install "aicspylibczi==3.1.1" 
pip install "readlif==0.6.4"
pip install "aicsimageio[base-imageio]"
pip install tqdm





##############################################################################################################################
############################################################### CREATE STABLE VERSION (API+LINEAGE+UNITY)  #####################################################
##############################################################################################################################

In this order (API + LINEAGE + UNITY) : 

API : 
  - pull DEV branch 
  - update morphonet_api/version.txt (+ 0.0.1, except if Major Release )
  - push on DEV

LINEAGE :
  - pull DEV branch 
  - update morphonet_lineage/MorphoNetLineage/Assets/version.txt (+ 0.0.1, except if Major Release )
  - push on DEV

UNITY:
  - pull DEV branch 
  - update morphonet_unity/MorphoNet/Assets/version.txt (+ 0.0.1, except if Major Release )
  - push on DEV


Create the MR for each git from DEV to STABLE using the website of each git
(Left Menu) -> Code -> Merge Request  -> New Merge Request
  Source : DEV  Target : STABLE
  Click on Compare branches and continue
  Title : DevToStable
  Assign To yourself
  Merge Options : 
    * Uncheck "Delete source branch when merge request is accepted."
    * Check : "Squash commits when merge request is accepted"
  Click on Create Merge Requests

In case of conflicts ,2 possibilities :
  - If conflict only on version.txt -> Direct edit the conflict on the webIDE (more recente version)
  - More complex case : 
      * Merge first STABLE in DEV 
        * git checkout dev
        * git pull 
        * git checkout stable
        * git pull
        * git checkout dev
        * git merge stable
        -> usually accept modification from DEV (and not STABLE)
        * git commit + push
        
And finally MERGE (Immediately)  


After Stable is built
    * Create for each git (in the same order), a Merge Request from DEV To Main (no need to update version number)

HOTFIX : can be performed on DEV
    


