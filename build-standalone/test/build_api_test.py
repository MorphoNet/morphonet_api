import os,sys
from os.path import dirname, basename, isdir, join, isfile
sys.path.append("..")
from functions import mkdir, execute, rmrf, push_build
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-o',"--os", default="", help="Which OS (OSX,LINUX,WINDOWS,ANDROID,IOS)", required=True)
parser.add_argument('-b',"--branch", default="", help="Which MorphoNet branch (sable,dev,beta)", required=True)
parser.add_argument('-a',"--architecture", default="amd64", help="Which architecture (silicon or other...)", required=False)
parser.add_argument('-p',"--processor", default="CPU", help="GPU or CPU ? ", required=False)
args = parser.parse_args()
if args.os=="": sys.exit(1)
if args.branch=="": sys.exit(1)
osmn=args.os
branch=args.branch
architecture=args.architecture
if branch=="main": branch="beta"

processor=args.processor

print(" ->> BUILD API TEST MORPHONET from "+branch+" git branch ")

#CLEAN PREVIOUS BUILDS
rmrf("dist")
rmrf("build")

#DEPLOY PATH AND NAME
pyname="API_Test.tar.gz"


# CREATE THE STANDALONE APPLICATION WITH PY INSTALLER
if osmn=="OSX":
    if architecture=="silicon":
        execute('pyinstaller API_Test_silicon.spec')
    else:
        execute('pyinstaller API_Test_osx.spec')

    if not isdir("dist/API_Test.App"):
        print(" --> ERROR compilation dist/API_Test does not exist ")
        execute("ls -laF dist")
        sys.exit(1)

    execute("cd  dist; tar -cf " + pyname + " API_Test.app")
    pyname = join("dist", pyname)

elif osmn=="LINUX":
    if processor == "GPU":
        execute('pyinstaller API_Test_linux_GPU.spec')
    else:
        execute('pyinstaller API_Test_linux.spec')

    if (not isfile("dist/API_Test/API_Test") and processor == "GPU") or (  processor != "GPU" and isfile("dist/API_Test")):
        print(" --> ERROR compilation dist/API_Test does not exist ")
        execute("ls -laF dist")
        sys.exit(1)


    if processor == "GPU":
        execute('cd dist/API_Test/_internal;ln -s nvidia/cudnn/lib/libcudnn_engines_precompiled.so.9 .')
        execute('cd dist/API_Test/_internal;ln -s nvidia/cudnn/lib/libcudnn_engines_runtime_compiled.so.9 .')
        execute('cd dist/API_Test/_internal;ln -s nvidia/cudnn/lib/libcudnn_heuristic.so.9 .')
    execute("cd  dist; tar -cf " + pyname + " API_Test")
    pyname = join("dist", pyname)

elif osmn=="WINDOWS":
    if processor == "GPU":
        execute('pyinstaller API_Test_windows_GPU.spec')
    else:
        execute('pyinstaller API_Test_windows.spec')

    if not isfile(join("dist","API_Test.exe")):
        print(" --> ERROR compilation dist/API_Test.exe does not exist ")
        execute("dir dist")
        sys.exit(1)

    execute("move dist\API_Test .")
    execute("tar -cf " + pyname + " API_Test")


if processor == "GPU":
    push_build(branch, osmn+"_"+processor,pyname)
else:
    push_build(branch, osmn , pyname)

