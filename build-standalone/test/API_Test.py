from multiprocessing import freeze_support
from os.path import join, isfile, isdir


class bcolors:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def printblue(strs):
    print(bcolors.BLUE + strs + bcolors.ENDC)


def printred(strs):
    print(bcolors.RED + strs + bcolors.ENDC)


def printgreen(strs):
    print(bcolors.GREEN + strs + bcolors.ENDC)


def printyellow(strs):
    print(bcolors.YELLOW + strs + bcolors.ENDC)




if __name__ == "__main__":
    freeze_support()

    errors=False
    import sys,os
    import time
    print("Start MorphoNet API TEST")

    printblue(" --> list installed packages inside the packaged environnment")
    try:
        import importlib.metadata
        installed_packages = sorted(x.name for x in importlib.metadata.distributions())
        installed_packages_list=[]
        for name in installed_packages:installed_packages_list.append(name + "=="+str(importlib.metadata.version(name)))
        print(installed_packages_list)
    except Exception as e:
        printred(" --> ERROR IN PACKAGES LIST")
        print(e)
        errors=True


    def try_import_lib(libimported):
        try:
            ts = time.time()
            exec(libimported)
            print(" --> sucessfully import "+str(libimported) + " in "+str(time.time() - ts) +" seconds")
        except Exception as e:
            printred(" --> ERROR with "+str(libimported))
            print(e)


    import os

    #os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

    printblue(" --> Import libraries")
    try_import_lib("import numpy as np")
    try_import_lib("from os.path import join, dirname, exists")
    try_import_lib("from platform import system")
    try_import_lib("from http.server import HTTPServer, BaseHTTPRequestHandler")
    try_import_lib("from threading import Thread, Event")
    try_import_lib("from io import BytesIO")
    try_import_lib("from psutil import Process, process_iter, wait_procs")
    try_import_lib("from shutil import rmtree")
    try_import_lib("from traceback import format_exc")
    try_import_lib("from imageio import get_writer")
    try_import_lib("from imageio import imread as imioread")
    try_import_lib("from glob import glob")
    try_import_lib("import traceback")
    try_import_lib("import skimage")
    try_import_lib("import sklearn")
    try_import_lib("import torch")
    try_import_lib("import morphonet")
    try_import_lib("import cellpose")
    try_import_lib("import tensorflow")
    try_import_lib("import csbdeep")
    try_import_lib("import stardist")


    print(" --> finished importing python headers!")


    printblue("\n\n --> eval Torch configuration")
    try:
        import torch
        which ="GPU" if torch.cuda.is_available() else "CPU"
        print(" --> Torch (CellPose,..)  will run on "+which)
    except Exception as e:
        printred(" --> TORCH CINFIGURATION ERROR")
        print(e)
        errors = True



    printblue("\n\n --> eval tensorflow configuration")
    try:
        import tensorflow as tf
        which ="GPU" if len(tf.config.list_physical_devices('GPU'))>=1 else "CPU"
        print(" --> Tensorflow (Stardist,..)  will run on "+which)
        ts = time.time()
    except Exception as e:
        printred(" --> TENSORFLOW CONFIGURATION ERROR")
        print(e)
        errors = True


    printblue("\n\n --> eval test several imread ")
    import morphonet
    from urllib.request import urlretrieve
    for f in open(join("ImagesForMorphoNetTest","image_tested.csv"),"r"):
        try:
            url = f.strip()
            filename = join("ImagesForMorphoNetTest",os.path.basename(url).replace("%20","_"))
            if not isfile(filename):
                print(" DOWNLOAD " + filename)
                urlretrieve(url,filename)
            if not isfile(filename):
                print("--> miss '"+str(filename)+"'")
            else:
                if filename.endswith(".mat") or filename.endswith(".lsm"):
                    print("This format is not yet supported "+filename)
                else:
                    im=morphonet.tools.imread(filename,verbose=2)
                    if im is None:
                        print(" --> Cannot read " + str(f))
                    else:
                        print("image shape is "+str(im.shape))
        except Exception as e:
            printred(" --> ERROR imread... "+str(e))
            errors = True



    printblue("\n\n --> eval CellPose")
    try:
        import morphonet
        from cellpose import models, core
        import logging

        logging.basicConfig(level=logging.INFO)  # To have cellpose log feedback on the terminalk

        which = "CPU"
        if core.use_gpu():  which = "GPU"
        device = None
        if torch.backends.mps.is_available():
            print("torch metal available on silicon architecture ", 1)
            which = "GPU"
            device = torch.device("mps")
        print(" -> CellPose will run on "+which)
        downsampling = 4
        diameter = 30
        model_type ="cyto"
        t=0
        rawdata = morphonet.tools.imread(join('ImagesForMorphoNetTest','raw.tiff'),verbose=2) #ALWAYS RETURN 5D
        rawdata = rawdata[..., 0, 0]
        print("image shape " + str(rawdata.shape) + " need to be in 3D")
        rawdata=rawdata[::downsampling,::downsampling,::downsampling]
        print("load the pretrained model "+model_type)
        model = models.Cellpose(gpu=True,  model_type=model_type)
        ts = time.time()
        print("predict the segmentation masks with diameter "+str(diameter))
        masks = model.eval(rawdata,diameter=diameter,  do_3D=True)
        print(" -->run CellPose take "+str(time.time()-ts))
        print(masks[0].shape)
        morphonet.tools.imsave(join('ImagesForMorphoNetTest','cellpose.tiff'),masks[0],verbose=2)
    except Exception as e:
        printred(" --> CELLPOSE ERROR")
        print(e)
        errors = True



    printblue("\n\n --> eval StarDist  ")
    try:
        from csbdeep.utils import normalize
        from stardist.models import StarDist3D
        print("load the stardist 3D model")
        model = StarDist3D.from_pretrained('3D_demo')
        rawdata = morphonet.tools.imread(join('ImagesForMorphoNetTest','raw.tiff'),verbose=2) #ALWAYS RETURN 5D
        rawdata = rawdata[..., 0, 0]
        print("image shape " + str(rawdata.shape) + " need to be in 3D")
        downsampling = 4
        rawdata = rawdata[::downsampling, ::downsampling, ::downsampling]
        print("normalize the rawdata  ")
        rawdata = normalize(rawdata)
        print("predict ")
        ts = time.time()
        data, _ = model.predict_instances(rawdata)
        print(" -->run StarDist take "+str(time.time()-ts))
        print(data.dtype)
        print(data.shape)
        morphonet.tools.imsave(join('ImagesForMorphoNetTest','startdist.tiff'),data,verbose=2)
    except Exception as e:
        printred(" --> STARTDIST ERROR")
        print(e)
        errors = True





    printblue("\n\n --> eval morphonet")
    try:
        from morphonet import Plot, Net
        from morphonet.tools import fast_convert_to_OBJ, imread, save_mesh

        print(" --> Convert in OBJ (to test VTK import) ")
        im=imread(join('ImagesForMorphoNetTest','test.tiff'),verbose=2)
        ts = time.time()
        im=im[...,0,0]
        print("image shape "+str(im.shape)+ " need to be in 3D")
        obj = fast_convert_to_OBJ(im, t=0)
        print(" -->run VTK conversion take "+str(time.time()-ts))
        save_mesh(join('ImagesForMorphoNetTest','test.obj'),obj)
    except Exception as e:
        printred(" --> MORPHONET ERROR")
        print(e)
        errors = True

    #morphonet.tools.rmrf("build")
    #morphonet.tools.rmrf("ImagesForMorphoNetTest")

    if errors:
        sys.exit(1)
