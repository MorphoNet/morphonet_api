from morphonet.data import get_local_dataset,list_local_datasets
import argparse
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-s', "--segmentation-channel",help="MorphoNet segmentation channel")
parser.add_argument('-t', "--time-point",help="Time point")
parser.add_argument('-i', "--id-cell",help="ID cell to delete")
args = parser.parse_args()

dataset_name = args.dataset_name
seg_channel = int(args.segmentation_channel)
time_point = int(args.time_point)
id_cell = int(args.id_cell)

dataset = get_local_dataset(dataset_name)
if dataset is None :
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()
min_time = dataset.get_min_time()
max_time = dataset.get_max_time()

if time_point < min_time or time_point > max_time:
    print("Time point is out of dataset time points range, please verify")
    exit()

properties_to_update = []
print("-> Loading found segmentation")
img = dataset.get_segmentation(time_point, step=-1,channel=seg_channel,load_array=True)
print("-> Erasing from segmentation")
img_array = img.ndarray
if img_array is not None:
    img_array[img_array == id_cell] = int(dataset.background_value)
    print("-> Loading properties")
    properties = dataset.get_updated_properties()
    print("-> Erasing cells from properties")
    for prop in properties:
        if prop.loaded_from_txt:
            if prop.get_object_value(time_point,id_cell,seg_channel) is not None:
                prop.remove_from_property(time_point,id_cell,seg_channel)
                properties_to_update.append(prop.name)
    print("-> Saving to new step")
    script_name = Path(__file__).stem
    step = dataset.initialize_external_step(script_name)
    dataset.write_segmentation_to_step(step,img_array,time_point,seg_channel,img.voxel_size)
    for prop in properties:
        if prop.loaded_from_txt and prop.name in properties_to_update:
            dataset.write_property_to_step(step, script_name,prop)