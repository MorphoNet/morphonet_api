import argparse
from morphonet.data import get_local_dataset,list_local_datasets
from pathlib import Path
parser = argparse.ArgumentParser()
parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
args = parser.parse_args()

dataset_name = args.dataset_name

dataset = get_local_dataset(dataset_name)
if dataset is None :
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()
dataset.delete_property("lineage_distance")