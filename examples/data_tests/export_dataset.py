import argparse
from morphonet.data import get_local_dataset,list_local_datasets
import os

parser = argparse.ArgumentParser()
parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-c', "--segmentation-channel",help="MorphoNet segmentation channel")
parser.add_argument('-o',"--output-folder",help="Output folder where segmentations should be saved")
parser.add_argument('-p',"--properties-output",help="Properties folder output")
parser.add_argument('-s',"--step",help="What step to export  (-1 for last step)")

args = parser.parse_args()

dataset_name = args.dataset_name
seg_channel = int(args.segmentation_channel)
output_folder = args.output_folder
segmentation_image_template = dataset_name.replace(" ","_")+"_seg_t%03d.nii"
step = int(args.step)
output_properties = args.properties_output

if not os.path.isdir(output_folder):
    os.makedirs(output_folder)

if not os.path.isdir(output_properties):
    os.makedirs(output_properties)

dataset = get_local_dataset(dataset_name)
if dataset is None :
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()

print("-> Exporting segmentation images")
dataset.export_dataset_at_step(seg_channel, segmentation_image_template, output_folder, step)
print("-> Exporting properties , this can take long for dataset with lots of curation steps")
dataset.export_properties_as_txt(output_properties, False, step)
