import argparse
from morphonet.data import get_local_dataset,list_local_datasets
import matplotlib.pyplot as plt
import numpy as np
parser = argparse.ArgumentParser()

parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-o', "--other-dataset",help="MorphoNet local dataset name")
parser.add_argument('-p', "--property-name",help="Name of the property to plot the distribution")
args = parser.parse_args()

dataset_name = args.dataset_name
other_dataset_name = args.other_dataset
input_property = args.property_name

dataset = get_local_dataset(dataset_name)
other_dataset = get_local_dataset(other_dataset_name)

if dataset is None or other_dataset is None:
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()
min_time = dataset.get_min_time()
max_time = dataset.get_max_time()

# build curve of mean of property
times = []
propery_for_dataset = []
property_for_other_dataset = []
property_object = dataset.get_property(input_property)
other_property_object = other_dataset.get_property(input_property)
for time in range(min_time,max_time+1):
    times.append(time)
    if property_object is not None:
        values = property_object.get_float_values_at(time)
        propery_for_dataset.append(values)
    if other_property_object is not None:
        values = other_property_object.get_float_values_at(time)
        property_for_other_dataset.append(values)
positions1 = np.arange(len(times)) * 2.5 - 0.5
positions2 = np.arange(len(times)) * 2.5 + 0.5
plt.boxplot(propery_for_dataset, positions=positions1,labels=times,label=dataset_name, patch_artist=True,showfliers=False,boxprops=dict(facecolor="blue"))
plt.boxplot(property_for_other_dataset, positions=positions2,labels=times,label = other_dataset_name, patch_artist=True,showfliers=False,boxprops=dict(facecolor="orange"))
plt.title("Evolution of property "+str(input_property)+" through time")
plt.legend()
plt.tight_layout()
plt.xticks(np.arange(len(times)) * 2.5, times, rotation=45)
plt.xlabel("Time")
plt.ylabel("Mean of property")
plt.show()
plt.clf()
