from morphonet.data import data
import numpy as np
import time
#to_test = []#["listing","segmentation","intensity","property"]
to_test = ["listing","segmentation","intensity","property","actions"]
external_json_path = "/Users/benjamin/Documents/ BACKUP_CODES/kilian_local_datasets-2-1-17.json"
external_temp_path = "/Users/benjamin/Library/CloudStorage/GoogleDrive-crbm.tunicate@gmail.com/Mon Drive/CURATION/CURATION-DATASETS/EM/20220310-Karim/INTRAREG/INTRAREG_EXP/POST/POST_01_EXPORT-20250201/"
if "listing" in to_test:
    print("-> Test for normal MorphoNet temp file ")
    print(data.list_local_datasets())
    print("\n")
    print("\n")

    print("-> Test overriding temp file")
    print(data.list_local_datasets(external_json_path))
    print("\n")
    print("\n")


if "listing" in to_test:

    print("-> Test for normal MorphoNet temp file ")
    non_existing_dataset = "euzeguzhfei"
    found_dataset = data.get_local_dataset(non_existing_dataset)
    print(found_dataset)
    print("should be None")
    print("\n")
    print("\n")
    print("-> Test overriding temp file")
    found_dataset = data.get_local_dataset(non_existing_dataset,json_override=external_json_path,temp_path_override=external_temp_path)
    print(found_dataset)
    print("should be None")
    print("\n")
    print("\n")

existing_dataset = "180824-Emilie-Nodal-St8_intrareg_post_t020"
found_dataset = data.get_local_dataset(existing_dataset)
print(found_dataset)
print("should not be None")
print("\n")
print("\n")


if "property" in to_test:
    print("-> Testing the computation of properties ")
    found_area = found_dataset.get_property("area")
    print("Area exist, should be true : ")
    print(found_area is None)

    not_found_neigh = found_dataset.get_property("connected_neighbors",34)
    print("Connected neighbors does not exist, should be false : ")
    print(not_found_neigh is None)

    print("Computing it ")
    found_neigh = found_dataset.compute_property_at("connected_neighbors",34,save=False)

    for cell in found_neigh.get_keys():
        print(str(cell) + " -> "+str(found_neigh.values[cell]))

    #volume_bbox

    #not_found_neigh = found_dataset.get_property("area_bbox")
    #print("Volume bbox does not exist, should be false : ")
    #print(not_found_neigh is None)

    print("Computing area bbox without saving ")
    found_neigh = found_dataset.compute_property("area_bbox", save=False)

    for cell in found_neigh.get_keys():
        print(str(cell) + " -> " + str(found_neigh.values[cell]))

    print("Computing it ")
    found_neigh = found_dataset.compute_property_at("area_bbox", 50, save=True)

    #for cell in found_neigh.get_keys():
    #    print(str(cell) + " -> " + str(found_neigh.values[cell]))



    not_found_neigh = found_dataset.get_property("area_bbox")
    print("area bbox does exist, should be false : ")
    print(not_found_neigh is None)

    for cell in not_found_neigh.get_keys():
        print(str(cell) + " -> " + str(not_found_neigh.values[cell]))

if "segmentation" in to_test:
    img_array = found_dataset.get_segmentation(43,0,0) # load image at 10 , step 0
    print(img_array)
    print("\n")
    print("\n")
    img_array = found_dataset.get_segmentation(43, 1000, 0)  # load image at 10 , step 0
    print(img_array)
    print("\n")
    print("\n")
if "intensity" in to_test:
    if found_dataset.has_intensities_images_at(20,0):
        img_raw = found_dataset.get_intensity_image(20,0)
        print(img_raw)
        print("\n")
        print("\n")
    if found_dataset.has_intensities_images_at(60,0):
        img_raw = found_dataset.get_intensity_image(60,0)
        print(img_raw)
        print("\n")
        print("\n")

    if found_dataset.has_intensities_images_at(20,0):
        img_raw = found_dataset.get_intensity_image(20,0,search_for_compressed=True)
        print(img_raw)
        print("\n")
        print("\n")
    if found_dataset.has_intensities_images_at(60,0):
        img_raw = found_dataset.get_intensity_image(60,0,search_for_compressed=True)
        print(img_raw)
        print("\n")
        print("\n")

if "property" in to_test:
    normal_property_name = "cell_name"
    scikit_property_name = "volume"
    step = 500
    time_point= 30
    last_time = -1
    last_step = -1
    # Find normal property , at specific time point last step, print nombre de valeur
    print("Loading property "+str(normal_property_name)+" for time "+str(time_point)+" at step : "+str(last_step))
    normal_property_last_step = found_dataset.get_property_at(normal_property_name,time=time_point,step=last_step)
    for cell_name in normal_property_last_step.get_object_ids_at(time_point):
        print(str(cell_name)+" has value : "+str(normal_property_last_step.values[cell_name]))
    #print(" Mean = "+str(np.mean(normal_property_last_step.get_values_at(time_point))))
    print("\n")
    print("\n")
    # Find normal property , at specific time point and step , print nombre de valeur
    print("Loading property "+str(normal_property_name)+" for time "+str(time_point)+" at step : "+str(step))
    normal_property_mid_step = found_dataset.get_property_at(normal_property_name,time=time_point,step=step)
    for cell_name in normal_property_mid_step.get_object_ids_at(time_point):
        print(cell_name+" has value : "+str(normal_property_mid_step.values[cell_name]))
    #print(" Mean = " + str(np.mean(normal_property_mid_step.get_values_at(time_point))))
    print("\n")
    print("\n")
    # Find normal property , all time points last step , for each time  print nombre de valeur
    print("Loading property "+str(normal_property_name)+" for times "+str(last_time)+" at step : "+str(last_step))
    normal_property_alltimes_last_step = found_dataset.get_property(normal_property_name,step=last_step)
    for timep in range(found_dataset.get_min_time(),found_dataset.get_max_time()+1):
        for cell_name in normal_property_alltimes_last_step.get_object_ids_at(timep):
            print(str(cell_name)+" has value : "+str(normal_property_alltimes_last_step.values[cell_name]))
        #print(" Mean = " + str(np.mean(normal_property_alltimes_last_step.get_values_at(timep))))
    print("\n")
    print("\n")
    # Find normal property , all time point and step , for each time print nombre de valeur
    print("Loading property "+str(normal_property_name)+" for times "+str(last_time)+" at step : "+str(step))
    normal_property_alltimes_at_step = found_dataset.get_property(normal_property_name,step=step)
    for timep in range(found_dataset.get_min_time(),found_dataset.get_max_time()+1):
        for cell_name in normal_property_alltimes_at_step.get_object_ids_at(timep):
            print(str(cell_name)+" has value : "+str(normal_property_alltimes_at_step.values[cell_name]))
        #print(" Mean = " + str(np.mean(normal_property_alltimes_at_step.get_values_at(timep))))
    print("\n")
    print("\n")
    # Find scikit property , at specific time point last step,  print nombre de valeur
    print("Loading property " + str(scikit_property_name) + " for time " + str(time_point) + " at step : " + str(
        last_step))
    normal_property_last_step = found_dataset.get_property_at(scikit_property_name, time=time_point, step=last_step)
    for cell_name in normal_property_last_step.get_object_ids_at(time_point):
        print(str(cell_name) + " has value : " + str(normal_property_last_step.values[cell_name]))
    print(" Mean = "+str(np.mean(normal_property_last_step.get_values_at(time_point))))
    print("\n")
    print("\n")
    # Find scikit property , at specific time point and step,  print nombre de valeur
    print("Loading property " + str(scikit_property_name) + " for time " + str(time_point) + " at step : " + str(step))
    normal_property_mid_step = found_dataset.get_property_at(scikit_property_name, time=time_point, step=step)
    for cell_name in normal_property_mid_step.get_object_ids_at(time_point):
        print(cell_name + " has value : " + str(normal_property_mid_step.values[cell_name]))
    print(" Mean = " + str(np.mean(normal_property_mid_step.get_values_at(time_point))))
    print("\n")
    print("\n")
    # Find scikit property , all time points last step , for each time  print nombre de valeur
    print("Loading property "+str(scikit_property_name)+" for times "+str(last_time)+" at step : "+str(last_step))
    normal_property_alltimes_at_step = found_dataset.get_property(scikit_property_name,step=last_step)
    for timep in range(found_dataset.get_min_time(),found_dataset.get_max_time()+1):
        for cell_name in normal_property_alltimes_at_step.get_object_ids_at(timep):
            print(str(cell_name)+" has value : "+str(normal_property_alltimes_at_step.values[cell_name]))
        print(" Mean = " + str(np.mean(normal_property_alltimes_at_step.get_values_at(timep))))
    print("\n")
    print("\n")
    # Find scikit property , all time point and step, for each time  print nombre de valeur
    print("Loading property "+str(scikit_property_name)+" for times "+str(last_time)+" at step : "+str(step))
    normal_property_alltimes_at_step = found_dataset.get_property(scikit_property_name,step=step)
    for timep in range(found_dataset.get_min_time(),found_dataset.get_max_time()+1):
        for cell_name in normal_property_alltimes_at_step.get_object_ids_at(timep):
            print(str(cell_name)+" has value : "+str(normal_property_alltimes_at_step.values[cell_name]))
        print(" Mean = " + str(np.mean(normal_property_alltimes_at_step.get_values_at(timep))))
    print("\n")

    t1 = time.time()
    print("Loading all properties at step")
    print("\n")
    props = found_dataset.get_updated_properties(100, -1)
    for prop in props:
        print(prop.name + " has " + str(len(prop.get_values_at(-1))) + " cells ")
    print("\n")
    t2 = time.time()
    print("time for all prop at step : " + str(t2 - t1) + "seconds")
    t1 = time.time()
    print("Loading all properties at step no scikit ")
    print("\n")
    props = found_dataset.get_updated_properties(100, -1, include_scikit=False)
    for prop in props:
        print(prop.name + " has " + str(len(prop.get_values_at(-1))) + " cells ")
    print("\n")
    t2 = time.time()
    print("time for all prop at step no scikit : " + str(t2 - t1) + "seconds")

    t1 = time.time()
    print("Loading all properties backwards at step , no scikit")
    print("\n")
    props = found_dataset.get_all_properties(100, -1, include_scikit=False)
    for prop in props:
        print(prop.name + " has " + str(len(prop.get_values_at(-1))) + " cells ")
    print("\n")
    t2 = time.time()
    print("time for all prop at step  backwards no scikit : " + str(t2 - t1) + "seconds")
    t1 = time.time()
    print("Loading all properties backwards at step")
    print("\n")
    props = found_dataset.get_all_properties(100, -1)
    for prop in props:
        print(prop.name + " has " + str(len(prop.get_values_at(-1))) + " cells ")
    print("\n")
    t2 = time.time()
    print("time for all prop at step  backwards : " + str(t2 - t1) + "seconds")

exit()

if "listing" in to_test:
    #found_dataset.print_all_properties()
    found_dataset.print_all_segmentations(channel=0)
    found_dataset.print_all_intensity_images(channel=0)

if "actions" in to_test:
    print("Listing actions ")
    max_number_of_steps = found_dataset.get_number_of_curations()
    for step in reversed(range(0, max_number_of_steps + 1)):
        print("step "+str(step))
        print(found_dataset.get_action_at(step))
        print(found_dataset.get_cells_updated_at(step))
        print(found_dataset.get_plugin_name_at(step))
        print(found_dataset.get_cells_updated_by_selection_at(step))


external_existing_dataset = "250201-20220310-Karim_intrareg_post"
found_dataset = data.get_local_dataset(external_existing_dataset,json_override=external_json_path,temp_path_override=external_temp_path)
print(found_dataset)
print("should not be None")
#print(found_dataset.name)
print("\n")
print("\n")

if "segmentation" in to_test:
    img_array = found_dataset.get_segmentation(55,0,0) # load image at 10 , step 0
    print(img_array)
    print("\n")
    print("\n")
    img_array = found_dataset.get_segmentation(55, 1000, 0)  # load image at 10 , step 0
    print(img_array)
    print("\n")
    print("\n")
if "intensity" in to_test:
    if found_dataset.has_intensities_images_at(100,0):
        img_raw = found_dataset.get_intensity_image(100,0)
        print(img_raw)
        print("\n")
        print("\n")
    if found_dataset.has_intensities_images_at(60,0):
        img_raw = found_dataset.get_intensity_image(60,0)
        print(img_raw)
        print("\n")
        print("\n")

    if found_dataset.has_intensities_images_at(100,0):
        img_raw = found_dataset.get_intensity_image(100,0,search_for_compressed=True)
        print(img_raw)
        print("\n")
        print("\n")
    if found_dataset.has_intensities_images_at(60,0):
        img_raw = found_dataset.get_intensity_image(60,0,search_for_compressed=True)
        print(img_raw)
        print("\n")
        print("\n")

if "property" in to_test:
    normal_property_name = "cell_name"
    scikit_property_name = "volume"
    step = 4
    time_point= 70
    last_time = -1
    last_step = -1
    # Find normal property , at specific time point last step, print nombre de valeur
    print("Loading property "+str(normal_property_name)+" for time "+str(time_point)+" at step : "+str(last_step))
    normal_property_last_step = found_dataset.get_property_at(normal_property_name,time=time_point,step=last_step)
    for cell_name in normal_property_last_step.get_object_ids_at(time_point):
        print(str(cell_name)+" has value : "+str(normal_property_last_step.values[cell_name]))
    #print(" Mean = "+str(np.mean(normal_property_last_step.get_values_at(time_point))))
    print("\n")
    print("\n")
    # Find normal property , at specific time point and step , print nombre de valeur
    print("Loading property "+str(normal_property_name)+" for time "+str(time_point)+" at step : "+str(step))
    normal_property_mid_step = found_dataset.get_property_at(normal_property_name,time=time_point,step=step)
    for cell_name in normal_property_mid_step.get_object_ids_at(time_point):
        print(cell_name+" has value : "+str(normal_property_mid_step.values[cell_name]))
    #print(" Mean = " + str(np.mean(normal_property_mid_step.get_values_at(time_point))))
    print("\n")
    print("\n")
    # Find normal property , all time points last step , for each time  print nombre de valeur
    print("Loading property "+str(normal_property_name)+" for times "+str(last_time)+" at step : "+str(last_step))
    normal_property_alltimes_last_step = found_dataset.get_property(normal_property_name,step=last_step)
    for timep in range(found_dataset.get_min_time(),found_dataset.get_max_time()+1):
        for cell_name in normal_property_alltimes_last_step.get_object_ids_at(timep):
            print(str(cell_name)+" has value : "+str(normal_property_alltimes_last_step.values[cell_name]))
        #print(" Mean = " + str(np.mean(normal_property_alltimes_last_step.get_values_at(timep))))
    print("\n")
    print("\n")
    # Find normal property , all time point and step , for each time print nombre de valeur
    print("Loading property "+str(normal_property_name)+" for times "+str(last_time)+" at step : "+str(step))
    normal_property_alltimes_at_step = found_dataset.get_property(normal_property_name,step=step)
    for timep in range(found_dataset.get_min_time(),found_dataset.get_max_time()+1):
        for cell_name in normal_property_alltimes_at_step.get_object_ids_at(timep):
            print(str(cell_name)+" has value : "+str(normal_property_alltimes_at_step.values[cell_name]))
        #print(" Mean = " + str(np.mean(normal_property_alltimes_at_step.get_values_at(timep))))
    print("\n")
    print("\n")
    # Find scikit property , at specific time point last step,  print nombre de valeur
    print("Loading property " + str(scikit_property_name) + " for time " + str(time_point) + " at step : " + str(
        last_step))
    normal_property_last_step = found_dataset.get_property_at(scikit_property_name, time=time_point, step=last_step)
    for cell_name in normal_property_last_step.get_object_ids_at(time_point):
        print(str(cell_name) + " has value : " + str(normal_property_last_step.values[cell_name]))
    print(" Mean = "+str(np.mean(normal_property_last_step.get_values_at(time_point))))
    print("\n")
    print("\n")
    # Find scikit property , at specific time point and step,  print nombre de valeur
    print("Loading property " + str(scikit_property_name) + " for time " + str(time_point) + " at step : " + str(step))
    normal_property_mid_step = found_dataset.get_property_at(scikit_property_name, time=time_point, step=step)
    for cell_name in normal_property_mid_step.get_object_ids_at(time_point):
        print(cell_name + " has value : " + str(normal_property_mid_step.values[cell_name]))
    print(" Mean = " + str(np.mean(normal_property_mid_step.get_values_at(time_point))))
    print("\n")
    print("\n")
    # Find scikit property , all time points last step , for each time  print nombre de valeur
    print("Loading property "+str(scikit_property_name)+" for times "+str(last_time)+" at step : "+str(last_step))
    normal_property_alltimes_at_step = found_dataset.get_property(scikit_property_name,step=last_step)
    for timep in range(found_dataset.get_min_time(),found_dataset.get_max_time()+1):
        for cell_name in normal_property_alltimes_at_step.get_object_ids_at(timep):
            print(str(cell_name)+" has value : "+str(normal_property_alltimes_at_step.values[cell_name]))
        print(" Mean = " + str(np.mean(normal_property_alltimes_at_step.get_values_at(timep))))
    print("\n")
    print("\n")
    # Find scikit property , all time point and step, for each time  print nombre de valeur
    print("Loading property "+str(scikit_property_name)+" for times "+str(last_time)+" at step : "+str(step))
    normal_property_alltimes_at_step = found_dataset.get_property(scikit_property_name,step=step)
    for timep in range(found_dataset.get_min_time(),found_dataset.get_max_time()+1):
        for cell_name in normal_property_alltimes_at_step.get_object_ids_at(timep):
            print(str(cell_name)+" has value : "+str(normal_property_alltimes_at_step.values[cell_name]))
        print(" Mean = " + str(np.mean(normal_property_alltimes_at_step.get_values_at(timep))))
    print("\n")

    t1 = time.time()
    print("Loading all properties at step")
    print("\n")
    props = found_dataset.get_updated_properties(-1, -1)
    for prop in props:
        print(prop.name + " has "+str(len(prop.get_values_at(-1)))+" cells ")
    print("\n")
    t2 = time.time()
    print("time for all prop at step : "+str(t2-t1)+"seconds")
    t1 = time.time()
    print("Loading all properties at step no scikit ")
    print("\n")
    props = found_dataset.get_updated_properties(-1, -1, include_scikit=False)
    for prop in props:
        print(prop.name + " has "+str(len(prop.get_values_at(-1)))+" cells ")
    print("\n")
    t2 = time.time()
    print("time for all prop at step no scikit : " + str(t2 - t1) + "seconds")

    t1 = time.time()
    print("Loading all properties backwards at step , no scikit")
    print("\n")
    props = found_dataset.get_all_properties(-1, -1, include_scikit=False)
    for prop in props:
        print(prop.name + " has " + str(len(prop.get_values_at(-1))) + " cells ")
    print("\n")
    t2 = time.time()
    print("time for all prop at step  backwards no scikit : " + str(t2 - t1) + "seconds")
    t1 = time.time()
    print("Loading all properties backwards at step")
    print("\n")
    props = found_dataset.get_all_properties(-1, -1)
    for prop in props:
        print(prop.name + " has "+str(len(prop.get_values_at(-1)))+" cells ")
    print("\n")
    t2 = time.time()
    print("time for all prop at step  backwards no scikit : " + str(t2 - t1) + "seconds")



if "listing" in to_test:
    print("listing")
    found_dataset.print_all_segmentations(channel=0)
    found_dataset.print_all_intensity_images(channel=0)

if "actions" in to_test:
    print("Listing actions ")
    max_number_of_steps = found_dataset.get_number_of_curations()
    for step in reversed(range(0, max_number_of_steps + 1)):
        print("step "+str(step))
        print(found_dataset.get_action_at(step))
        print(found_dataset.get_cells_updated_at(step))
        print(found_dataset.get_plugin_name_at(step))
        print(found_dataset.get_cells_updated_by_selection_at(step))


exit()