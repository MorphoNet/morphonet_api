import argparse
from morphonet.data import get_local_dataset,list_local_datasets
from morphonet.data.utils import get_object_t_id_ch,get_symetric_cells,get_node
from morphonet.data.dataproperty import DataProperty
from pathlib import Path
parser = argparse.ArgumentParser()
parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-c',"--segmentation-channel",help="On which channel should we compute lineage distance ")
parser.add_argument('-p',"--property",help="Name of the property")
parser.add_argument('-s',"--steps",help="List of steps separated by a coma")
args = parser.parse_args()

dataset_name = args.dataset_name
channel = int(args.segmentation_channel)
step_list = [int(s) for s in args.steps.split(',')]
property_name = args.property

dataset = get_local_dataset(dataset_name)
if dataset is None :
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()

for step in step_list:
    print("Computing "+str(property_name)+" at step "+str(step))
    dataset.compute_property(property_name, step=step)
