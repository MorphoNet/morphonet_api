import os
from morphonet.data import get_local_dataset,list_local_datasets
from morphonet.data.dataproperty import DataProperty
from morphonet.data.utils import get_object_t_id_ch
import argparse
from pathlib import Path
parser = argparse.ArgumentParser()


parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-s', "--segmentation-channel",help="MorphoNet segmentation channel")
args = parser.parse_args()

dataset_name = args.dataset_name
seg_channel = int(args.segmentation_channel)
temp_file_path = "temp_data_"+str(dataset_name).replace(" ", "_")
seg_folder_name = os.path.join(temp_file_path,"seg")
seg_img_name = dataset_name.replace(" ","_")+"_seg_t%03d.mha"


local_json_path_override = None
dataset = get_local_dataset(dataset_name)
if dataset is None :
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()

min_time = dataset.get_min_time()
max_time = dataset.get_max_time()

print("-> importing lineage in MorphoNet dataset")
cell_contact_surface = dataset.get_property("cell_contact_surface")

property_object = DataProperty(dataset, "cell_neighbors", "", "space")
if cell_contact_surface is not None:
    for id_cell in cell_contact_surface.get_keys():
        neigh = []
        tc,idc,channelc = get_object_t_id_ch(id_cell)
        for id_contact in cell_contact_surface.values[id_cell]:
            if not id_contact in neigh:
                neigh.append(id_contact)
        property_object.set_object_value(tc,idc,channelc,neigh)
    step = dataset.initialize_external_step(Path(__file__).stem)
    dataset.write_property_to_step(step, property_object.name + ".txt", property_object)