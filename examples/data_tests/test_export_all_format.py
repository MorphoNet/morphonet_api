from morphonet.tools import imread,imsave
import os

input_image = "/Users/benjamin/Data/Embryos/20220310-Karim/FUSE/FUSE_01/20220310-Karim_fuse_t161.nii"
folder = "temp_data/"
export_formats = [".mha",".nii",".tiff",".tif",".ometiff",".inr"]

input_image_name = input_image.split("/")[-1]
output_image = os.path.join(folder,input_image_name)
if not os.path.isdir(folder):
    os.makedirs(folder)
img_array,img_metadata = imread(input_image,return_metadata=True)
print(img_metadata)

for format_image in export_formats:
    temp_output_image = output_image.replace(".nii",format_image)
    voxel_size = (1, 1, 1)
    if "voxel_size" in img_metadata and img_metadata["voxel_size"] is not None and img_metadata[
        "voxel_size"] != "":
        voxel_size = img_metadata['voxel_size']
    if len(img_array.shape) > 3:
        img_array = img_array[:, :, :,0,0]
    imsave(temp_output_image,img_array,voxel_size=voxel_size)
    if os.path.exists(temp_output_image):
        test_array,test_metadata = imread(temp_output_image,return_metadata=True)
        print(test_metadata)
        print(test_array.shape)