import argparse
from morphonet.data import get_local_dataset,list_local_datasets
from morphonet.data.utils import get_object_t_id_ch,get_symetric_cells,get_node
from morphonet.data.dataproperty import DataProperty
import edist.ted
import edist.uted as uted
from pathlib import Path
parser = argparse.ArgumentParser()
parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-s',"--segmentation-channel",help="On which channel should we compute lineage distance ")
args = parser.parse_args()

dataset_name = args.dataset_name
channel = int(args.segmentation_channel)

dataset = get_local_dataset(dataset_name)
if dataset is None :
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()

property_object = DataProperty(dataset, "astec_lineage_distance", "", "float")
property_name = dataset.get_property("cell_name")
if property_name is None:
    print("Naming not found")
    exit()
cell_lineage = dataset.get_property("cell_lineage")
if cell_lineage is None:
    cell_lineage = dataset.get_property("temporal")
if cell_lineage is None:
    print("Lineage not found")
    exit()
    # Retrieve CellName property
def local_cost_normalized(x, y):
    if x is None and y is None:
        return 0
    elif x is None or y is None:
        return 1
    elif x not in x_nodes or y not in y_nodes:
        return 1
    xl = x_life[x_nodes.index(x)]
    yl = y_life[y_nodes.index(y)]
    return abs(xl - yl) / (xl + yl)

# LINEAGE COMPARISON
#if not cell_id in existing_regions:
#existing_regions[cell_id] = {}

for cell_key in property_name.get_keys():
    name = property_name.values[cell_key]
    #print("Trying to compute lineage dist for cell "+str(cell_key)+" with name : "+str(name))
    if name is not None and name.endswith("*"):  # ONLY GET RIGHT SIDE
        #print("found cell on right side ")
        #print("working time is "+str(time))
        tc,idc,chanc = get_object_t_id_ch(cell_key)
        #print("found cell at time : "+str(tc)+" with id "+str(idc))
        smo = get_symetric_cells(tc,property_name, name)
        #print("found symetric : "+str(smo))
        #print("smo : "+str(smo))
        #print("cell_key : "+str(cell_key))
        if smo is not None:
            ts,ids,chans = get_object_t_id_ch(smo)
            x_nodes, x_adj, x_life = get_node(cell_lineage,cell_key)
            #print(x_nodes,x_adj,x_life)
            y_nodes, y_adj, y_life = get_node(cell_lineage,smo)
            #print(y_nodes,y_adj,y_life)
            d = uted.uted(x_nodes, x_adj, y_nodes, y_adj, local_cost_normalized)
            #print(d)
            #exit()
            property_object.set_object_value(tc, idc, channel, d)
            property_object.set_object_value(ts, ids, channel, d)

step = dataset.initialize_external_step(Path(__file__).stem)
dataset.write_property_to_step(step,property_object.name+".txt",property_object)