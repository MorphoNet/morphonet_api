conda activate morphonet
python3 test_dataset.py
python3 cell_count_2_steps.py -d "180824-Emilie-Nodal-St8_intrareg_post_t020" -p "volume" -f 0 -s 1000
python3 compare_property_2_dataset_through_time.py -d "Samson_Astec_with_diff" -o "Samson_Membranes_with_diff" -s -1 -p "float_difference_ratio_connected_component"
python3 compare_property_through_time.py -d "180824-Emilie-Nodal-St8_intrareg_post_t020" -s 0,1000 -p "volume"
python3 compute_division_2_properties.py -d "Samson_Astec_with_diff" -p "float_difference_semantic_volume_connected_component" -s "float_difference_semantic_number_connected_component" -o "float_difference_ratio_connected_component"
python3 delete_cell.py -d "180824-Emilie-Nodal-St8_intrareg_post_t020" -s 0 -t 80 -i 51
python3 delta_property_through_time.py -d "180824-Emilie-Nodal-St8_intrareg_post_t020" -p "volume"
python3 distribution_property_2_dataset_through_time.py -d "Samson_Astec_with_diff" -o "Samson_Membranes_with_diff" -p "float_difference_semantic_volume_connected_componen"
python3 export_dataset.py -d "180824-Emilie-Nodal-St8_intrareg_post_t020" -c 0 -o /Users/benjamin/Data/TEST_EXPORT/seg/ -p /Users/benjamin/Data/TEST_EXPORT/properties/ -s -1
python3 generate_lineage_names.py -d "180824-Emilie-Nodal-St8_intrareg_post_t020" -s 0
python3 plot_embryo_curation_history.py -d "180824-Emilie-Nodal-St8_intrareg_post_t020"