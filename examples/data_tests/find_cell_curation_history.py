import argparse
from morphonet.data import get_local_dataset,list_local_datasets
import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-i',"--cell-identifier",help="MorphoNet cell identifier. Format t,id,ch")
args = parser.parse_args()

dataset_name = args.dataset_name
cellid = args.cell_identifier
dataset = get_local_dataset(dataset_name)

if dataset is None:
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()

number_of_steps = dataset.get_number_of_curations()

hasappeared = False
for step in range(0,number_of_steps+1):
    #print("-> Step "+str(step))
    #print("\n")
    plugin_name = dataset.get_plugin_name_at(step)
    plugin_params = dataset.get_plugin_parameters_at(step)
    #print("     Plugin : "+plugin_name)
    #print("\n")
    cells_updated = dataset.get_cells_updated_by_selection_at(step)
    for select in cells_updated:
        for cell in cells_updated[select]:
            if cell.replace("'",'').startswith(cellid):
                if not hasappeared:
                    print(" Appeared : "+"\n")
                else:
                    print(" Updated : "+"\n")
                hasappeared = True
                print("\n")
                print(" via plugin : "+str(plugin_name)+" \n")
                print("\n")
                print(" with parameters : "+str(plugin_params)+" \n")
                print("\n")
                print(" among cells : "+str(cells_updated[select])+" \n")
                print("\n")
                print(" at step : "+str(step)+"\n")
                print("----------")