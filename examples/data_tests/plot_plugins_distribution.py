import argparse
from morphonet.data import get_local_dataset,list_local_datasets
import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
args = parser.parse_args()

dataset_name = args.dataset_name
dataset = get_local_dataset(dataset_name)

if dataset is None:
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()

number_of_steps = dataset.get_number_of_curations()

plugins_count = {}
for step in range(1,number_of_steps+1):
    plugin_name = dataset.get_plugin_name_at(step)
    if plugin_name is not None:
        if plugin_name not in plugins_count:
            plugins_count[plugin_name] = 0
        else:
            plugins_count[plugin_name] += 1
    else:
        print(" plugin name is none for step : "+str(step))

x = list(plugins_count.keys())
x = [s.replace("(","\n(") for s in x]
y = list(plugins_count.values())

fig, ax = plt.subplots()

ax.bar(x, y, width=1, edgecolor="white", linewidth=0.7)
plt.xticks(rotation=70)
plt.show()