import argparse
from morphonet.data import get_local_dataset,list_local_datasets
import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
args = parser.parse_args()

dataset_name = args.dataset_name
dataset = get_local_dataset(dataset_name)

if dataset is None:
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()

number_of_steps = dataset.get_number_of_curations()

for step in range(1,number_of_steps+1):
    print("-> Step "+str(step))
    print("\n")
    plugin_name = dataset.get_plugin_name_at(step)
    print("     Plugin : "+plugin_name)
    print("\n")
    cells_by_label = dataset.get_cells_updated_by_selection_at(step)
    for label in cells_by_label:
        print("          "+str(label)+" : "+str(cells_by_label[label]))
    print("\n")