# This folder contains multiple files that interact with MorphoNet.data local API 

## cell_count_2_steps.py

This code use a property given in parameter to count cells, and plot those cell counts for 2 different steps

### Parameters : 

- -d : Dataset name 
- -p : Property name to count cell from
- -f : First step to use 
- -s : second step to use

## compare_property_2_dataset_through_time.py

Plot the mean + standard deviation through time for a specific property at a given step , from 2 local dataset. This code needs that the dataset has the same min / max time point

### Parameters : 

- -d : First dataset name 
- -o : Second dataset name
- -s : Step to compare (input -1 if you want to use last step)
- -p : name of the property to compare

## compare_property_through_time.py

Plot the mean of a property for a dataset through time, for a list of step given in parameter

### Parameters : 

- -d : Dataset name
- -s : list of the step to compare, separated by a coma (ex : 0,100,1000)
- -p : name of the property to compare

## compute_division_2_properties.py

Compute the ratio of 2 properties given in parameter, and add it to the dataset in a new step

### Parameters : 

- -d : MorphoNet local dataset name
- -p : Name of the property to compare (if you want to compute A/B , this will be A)
- -s : Name of the second property to compare (if you want to compute A/B , this will be B)
- -o : Name of the property to create, ratio of both input

## delete_cell.py

Delete a specific cell, given by time identifier and channel, in the segmentation image and in the properties
The new segmentation and properties are saved to a new step in the dataset 

### Parameters : 

- -d : MorphoNet local dataset name
- -s : Segmentation channel of the cell to delete
- -t : Time point of the cell to delete
- -i' : Identifier of the cell to delete

## delta_property_through_time.py

Compute the derivative of a property through time in the dataset, and save the new property generated to a new dataset step

### Parameters :

- -d : Name of the MorphoNet dataset
- -p : Name of the property to compute delta from

## distribution_property_2_dataset_through_time.py

Plot the distribution of a property through time for 2 different datasets. This code needs that the dataset has the same min / max time point

### Parameters : 

- -d : Dataset name
- -o : Other dataset name to compare
- -p : name of the property to compare

## export_dataset.py

Export the dataset segmentations and properties from a specific step , to a segmentation and properties folder

### Parameters :

- -d : MorphoNet local dataset name
- -c : Segmentation channel to export
- -o : Segmentation images output folder
- -p : Properties folder output
- -s : Which step to export  (-1 for last step)

## generate_lineage_names.py

Using ASTEC and ASCIDIAN tools, this code generate new properties and a new naming for the morphonet dataset, and import the generated properties to a new step in the dataset
This code needs the conda environement of the tools, that you can install here : 

ASTEC installation : https://gitlab.inria.fr/astec/astec
ASCIDIAN installation : https://gitlab.inria.fr/astec/ascidian

TO RUN THE CODE, YOU NEED TO KEEP THE "atlas_files" folder next to the python file

### Parameters :

- -d : MorphoNet local dataset name
- -s : MorphoNet segmentation channel to generate properties on

## plot_embryo_curation_history.py

Print to the terminal the curation history through all the steps 

### Parameters : 

- -d : MorphoNet local dataset name

## histogram_property_by_steps.py 

This code shows the histogram (distribution) of all values of a property, for different steps

### Parameters : 

- -d : MorphoNet local dataset name
- -s : List of steps to compare, separated by a comma (ex : 0,1,2,4)
- -p : Name of the property to compare

