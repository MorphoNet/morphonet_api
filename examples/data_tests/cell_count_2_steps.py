import argparse
import os.path

from morphonet.data import get_local_dataset,list_local_datasets
import matplotlib.pyplot as plt
import numpy as np
import time
parser = argparse.ArgumentParser()

parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-p','--property-name',help="Name of the property")
parser.add_argument('-f','--first-step',help="First step to compare")
parser.add_argument('-s','--second-step',help="Second step to compare")
args = parser.parse_args()

dataset_name = args.dataset_name
property = args.property_name
first_step = int(args.first_step)
second_step = int(args.second_step)
dataset = get_local_dataset(dataset_name)
if dataset is None :
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()


deleted_cell = []
added_cell = []
count_prev = []
count = []
times = []
previous_volume_property = dataset.get_property(property,step=first_step)
volume_property = dataset.get_property(property,step=second_step)
if volume_property is not None and previous_volume_property is not None:
    for time in range(dataset.min_time,dataset.max_time+1):
        print("Time "+str(time))
        idprev = previous_volume_property.get_object_ids_at(time)
        idcurr = volume_property.get_object_ids_at(time)
        for idc in idprev:
            if not idc in idcurr:
                deleted_cell.append(idc)
        for idc in idcurr:
            if not idc in idprev:
                added_cell.append(idc)
        print("Step "+str(first_step)+" : "+str(len(idprev)))
        print("Step "+str(second_step)+" : "+str(len(idcurr)))
        count_prev.append(len(idprev))
        count.append(len(idcurr))
        times.append(time)
    print(deleted_cell)
    print(added_cell)
    plt.plot(times,count,label="Step "+str(first_step),alpha=0.5)
    plt.plot(times,count_prev,label="Step "+str(second_step),alpha=0.5)
    plt.xlabel("Time point")
    plt.legend()
    plt.ylabel("Count")
    plt.show()
    plt.clf()
else:
    print("Property not found")
    if volume_property is not None:
        print("found current")
    if previous_volume_property is not None:
        print("found previous")