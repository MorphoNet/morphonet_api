import numpy as np
from morphonet.tools import imsave,imread
import argparse
import tifffile as tiff

def merge_images(images):
    """
    Merges multiple 3D images into a single 4D image with multiple channels.

    Parameters:
        images (numpy.ndarray): Variable number of 3D images, each with shape (depth, height, width).

    Returns:
        numpy.ndarray: A 4D image with shape (num_channels, depth, height, width),
                       where each channel corresponds to one input image.
    """
    # Ensure all images have the same shape
    shapes = [image.shape for image in images]
    if not all(shape == shapes[0] for shape in shapes):
        raise ValueError("All 3D images must have the same shape.")

    # Stack images along a new axis to represent the channel dimension
    merged_image = np.stack(images, axis=3)

    return merged_image
parser = argparse.ArgumentParser()
parser.add_argument('-i', "--image-list",help="List of input images path , separated by a comma.")
parser.add_argument('-o', "--output-image",help="Paht to output image")
args = parser.parse_args()

vsize = None
images = []
image_list = args.image_list.split(",")
if len(image_list) > 0:
    for img_path in image_list:
        im_array,metadata = imread(img_path,return_metadata=True)
        print(im_array.shape)
        images.append(im_array[:,:,:,0,0])
        if "voxel_size" in metadata:
            vsize = metadata["voxel_size"]
    swapped = merge_images(images)
    #swapped = np.swapaxes(merged_image,0,2)
    print(swapped.shape)
    imsave(args.output_image,swapped,voxel_size=vsize)