import os
import argparse
from morphonet.data.dataproperty import DataProperty
from morphonet.data.utils import get_object_t_id_ch
from morphonet.data import get_local_dataset,list_local_datasets
from pathlib import Path

parser = argparse.ArgumentParser()

parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-p', "--property",help="name of the property to compare (if you want to compute A/B , this will be A)")
parser.add_argument('-s', "--second-property",help="name of the second property to compare (if you want to compute A/B , this will be B)")
parser.add_argument('-o',"--output-property",help="name of the property to create, ratio of both input")


args = parser.parse_args()
input_property = args.property
input_property_2 = args.second_property
output_property_name = args.output_property

dataset_name = args.dataset_name

dataset = get_local_dataset(dataset_name)
if dataset is None :
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()
min_time = dataset.get_min_time()
max_time = dataset.get_max_time()
properties = dataset.get_updated_properties()
for prop in properties:
    if prop.name.lower() == output_property_name.lower():
        print("Output property already exist , please change ")
        continue
print("Loading properties to compare ")
property_1_object = dataset.get_property(input_property)
property_2_object = dataset.get_property(input_property_2)
property_final_object = DataProperty(dataset, output_property_name, "", "float")
for cell in property_1_object.get_keys():
    if cell in property_2_object.get_keys():
        prop1_val = float(property_1_object.values[cell])
        prop2_val = float(property_2_object.values[cell])
        if prop1_val is not None and prop2_val is not None and prop2_val != 0:
            ratio = prop1_val / prop2_val
            print(str(prop1_val) +" / " +str(prop2_val)+ " = "+str(ratio))
            tc, idc, ch = get_object_t_id_ch(cell)
            property_final_object.set_object_value(tc, idc, ch, ratio)
if len(property_final_object.values) > 0:
    print("Adding new property to MorphoNet dataset ")
    new_step = dataset.initialize_external_step(Path(__file__).stem)
    dataset.write_property_to_step(new_step, property_final_object.name + ".txt", property_final_object)

print("Finished creating property, continuing ... ")



