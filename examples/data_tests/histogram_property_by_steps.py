import argparse
from morphonet.data import get_local_dataset,list_local_datasets
import matplotlib.pyplot as plt
import numpy as np
parser = argparse.ArgumentParser()

parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-s',"--step-list",help="the list of steps to compare, separated by a comma (ex : 0,1,2,4)")
parser.add_argument('-p',"--property",help="name of the property to compare")
args = parser.parse_args()

step_list = [int(s) for s in args.step_list.split(',')]
dataset_name = args.dataset_name
input_property = args.property

dataset = get_local_dataset(dataset_name)
if dataset is None :
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()

min_time = dataset.get_min_time()
max_time = dataset.get_max_time()

# build curve of mean of property
property_values_by_step = {}
for int_step in step_list:
    print("-> Computing values for step : "+str(int_step))
    property_object = dataset.get_property(input_property, step=int_step)
    if property_object is not None:
        plt.hist(property_object.get_values(),bins='auto',label="Step "+str(int_step),alpha=0.5)
    else:
        print("property not found at step "+str(int_step))
        exit()
plt.title("Evolution of mean of property "+str(input_property)+" through time")
plt.legend()
plt.tight_layout()
plt.show()
plt.clf()
