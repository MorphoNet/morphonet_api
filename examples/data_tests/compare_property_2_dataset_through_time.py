import argparse
from morphonet.data import get_local_dataset,list_local_datasets
import matplotlib.pyplot as plt
import numpy as np
parser = argparse.ArgumentParser()

parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-o', "--other-dataset",help="MorphoNet local dataset name")
parser.add_argument('-s', "--step",help="the step to compare the property. If you want the last step, input -1")
parser.add_argument('-p', "--property",help="the property name to compare")
args = parser.parse_args()

dataset_name = args.dataset_name
other_dataset_name = args.other_dataset

dataset = get_local_dataset(dataset_name)
other_dataset = get_local_dataset(other_dataset_name)

if dataset is None or other_dataset is None:
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()
min_time = dataset.get_min_time()
max_time = dataset.get_max_time()
choosen_step = int(args.step)
input_property = args.property
# build curve of mean of property
times = []
propery_for_dataset = []
std_for_dataset = []
std_plus_for_dataset = []
property_for_other_dataset = []
std_for_other_dataset = []
std_plus_for_other_dataset = []
property_object = dataset.get_property(input_property, step=choosen_step)
other_property_object = other_dataset.get_property(input_property, step=choosen_step)
if property_object is None or other_property_object is None:
    print("Properties not found, quitting ")
    exit()
for time in range(min_time,max_time+1):
    print("Loading for time : "+str(time))
    times.append(time)
    if property_object is not None:
        values = property_object.get_float_values_at(time)
        if len(values)>0:
            mean = np.mean(values)
            std = np.std(values)
        else :
            mean = 0
            std = 0
        print(" time "+str(time)+ " mean : "+str(mean))
        propery_for_dataset.append(mean)
        std_for_dataset.append(mean-std)
        std_plus_for_dataset.append(mean+std)
    if other_property_object is not None:
        values = other_property_object.get_float_values_at(time)
        if len(values) > 0:
            mean = np.mean(values)
            std = np.std(values)
        else:
            mean = 0
            std = 0
        print(" time " + str(time) + " mean : " + str(mean))
        property_for_other_dataset.append(mean)
        std_for_other_dataset.append(mean-std)
        std_plus_for_other_dataset.append(mean+std)
plt.plot(times,propery_for_dataset,label=dataset_name,alpha=0.5)
plt.fill_between(times, std_for_dataset, std_plus_for_dataset, alpha=0.2)
plt.plot(times,property_for_other_dataset,label=other_dataset_name,alpha=0.5)
plt.fill_between(times, std_for_other_dataset, std_plus_for_other_dataset, alpha=0.2)
plt.title("Evolution of property "+str(input_property)+" through time at step : "+str(choosen_step))
plt.legend()
plt.tight_layout()
plt.xlabel("Time")
plt.ylabel("Mean of property")
plt.show()
plt.clf()
