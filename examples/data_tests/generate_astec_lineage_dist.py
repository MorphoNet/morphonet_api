import os
from morphonet.data import get_local_dataset,list_local_datasets
from morphonet.data.dataproperty import DataProperty
import xml.etree.ElementTree as ET
from morphonet.data.utils import get_object_id,indent_xml,generate_init_naming_parameters,remove_folder,generate_prop_naming_parameters,load_properties_from_xml,get_object_t_id_ch,get_node
import argparse
from pathlib import Path
import edist.ted
import edist.uted as uted
import time
def get_symetric_cells(property,name):
    #print(name)
    for cell_key in property.keys():
        sname = property[cell_key]
        if sname is not None:
            if sname!=name and sname[0:-1] == name[0:-1]:
                #print("found symetric "+str(sname))
                return cell_key
    return None

parser = argparse.ArgumentParser()

t1 = time.time()
parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-s', "--segmentation-channel",help="MorphoNet segmentation channel")
args = parser.parse_args()

dataset_name = args.dataset_name
seg_channel = int(args.segmentation_channel)
temp_file_path = "temp_data_"+str(dataset_name).replace(" ", "_")
seg_folder_name = os.path.join(temp_file_path,"seg")
seg_img_name = dataset_name.replace(" ","_")+"_seg_t%03d.mha"


local_json_path_override = None
dataset = get_local_dataset(dataset_name)
if dataset is None :
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()

property_object = DataProperty(dataset, "astec_lineage_distance", "", "float")
treated_cells_names = {}
print("Loading needed properties")
property_name = dataset.get_property("cell_name")
cell_lineage = dataset.get_property("cell_lineage")
if cell_lineage is None :
    cell_lineage = dataset.get_property("temporal")
dict_name_by_t = {}
dict_cells_by_name = {}
dict_mincell_id_by_name = {}
if property_name is not None and cell_lineage is not None:
    def local_cost_normalized(x, y):
        if x is None and y is None:
            return 0
        elif x is None or y is None:
            return 1
        elif x not in x_nodes or y not in y_nodes:
            return 1
        xl = x_life[x_nodes.index(x)]
        yl = y_life[y_nodes.index(y)]
        return abs(xl - yl) / (xl + yl)


    # LINEAGE COMPARISON
    # if not cell_id in existing_regions:
    # existing_regions[cell_id] = {}
    # Split the property into subs dict
    print("Splitting names by time point")
    for cell_key in property_name.values:
        tc, idc, chanc = get_object_t_id_ch(cell_key)
        name = property_name.values[cell_key]
        if not name in dict_cells_by_name:
            dict_cells_by_name[name] = []
        dict_cells_by_name[name].append(cell_key)
        if not int(tc) in dict_name_by_t:
            dict_name_by_t[int(tc)] = {}
        dict_name_by_t[int(tc)][cell_key] = name
        if not name in dict_mincell_id_by_name:
            dict_mincell_id_by_name[name] = cell_key
        else:
            tp,idcp,chanp = get_object_t_id_ch(dict_mincell_id_by_name[name])
            if int(tc) < int(tp):
                dict_mincell_id_by_name[name] = cell_key
    print(dict_mincell_id_by_name)
    for cell_key in property_name.get_keys():
        name = property_name.values[cell_key]
        # print("Trying to compute lineage dist for cell "+str(cell_key)+" with name : "+str(name))
        if name is not None and name.endswith("*"):  # ONLY GET RIGHT SIDE
            print("Working on cell : "+str(name))
            # print("found cell on right side ")
            # print("working time is "+str(time))
            tc, idc, chanc = get_object_t_id_ch(cell_key)
            mother_key = dict_mincell_id_by_name[name]
            # print("found cell at time : "+str(tc)+" with id "+str(idc))
            smo = name.replace('*','_')
            if smo is not None:
                # print("found symetric : "+str(smo))
                # print("smo : "+str(smo))
                # print("cell_key : "+str(cell_key))
                ts = None
                ids = None
                chans = None
                if smo in dict_mincell_id_by_name:
                    for cell_f in dict_cells_by_name[smo]:
                        tt, idt, chant = get_object_t_id_ch(cell_f)
                        if ts is None or int(tt) < int(ts):
                            ts = tt
                            ids = idt
                            chans = chant
                    if mother_key is not None and not mother_key in treated_cells_names:
                        x_nodes, x_adj, x_life = get_node(cell_lineage, mother_key)
                        # print(x_nodes,x_adj,x_life)
                        sym_key = get_object_id(ts,ids,chans)
                        print(mother_key + " -> "+sym_key)
                        y_nodes, y_adj, y_life = get_node(cell_lineage, sym_key)
                        # print(y_nodes,y_adj,y_life)
                        d = uted.uted(x_nodes, x_adj, y_nodes, y_adj, local_cost_normalized)
                        # print(d)
                        # exit()
                        treated_cells_names[mother_key] = d
                        property_object.set_object_value(tc, idc, seg_channel, d)
                        property_object.set_object_value(ts, ids, seg_channel, d)
                    else :
                        property_object.set_object_value(tc, idc, seg_channel,treated_cells_names[mother_key])
                        property_object.set_object_value(ts, ids, seg_channel, treated_cells_names[mother_key])

    step = dataset.initialize_external_step(Path(__file__).stem)
    dataset.write_property_to_step(step, property_object.name + ".txt", property_object)
t2 = time.time()
print("computed in : "+str(t2-t1)+" seconds")