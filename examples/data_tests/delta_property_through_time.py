import os
from morphonet.data import get_local_dataset,list_local_datasets
from morphonet.data.utils import get_object_t_id_ch
from morphonet.data.dataproperty import DataProperty
import xml.etree.ElementTree as ET
from morphonet.data.utils import get_previous_cell_in_lineage
import argparse
from pathlib import Path
parser = argparse.ArgumentParser()

parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-p',"--property-name",help="Property to compute delta from")
args = parser.parse_args()

dataset_name = args.dataset_name
property_name = args.property_name

dataset = get_local_dataset(dataset_name)
if dataset is None :
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()
min_time = dataset.get_min_time()
max_time = dataset.get_max_time()


lineage_property = dataset.get_property("temporal")
if lineage_property is None:
    lineage_property = dataset.get_property("cell_lineage")

if lineage_property is None:
    print("No lineage property found in the dataset, it is mandatory to compute the delta. Exiting ...")
    exit()

found_property = dataset.get_property(property_name)
if found_property is None:
    print("Property not found, please verify the name and try again")
    exit()
new_property_name = "delta_"+str(property_name)
new_property = dataset.get_property(new_property_name)
if new_property is not None:
    print("Property already found, updating it")
new_property = DataProperty(dataset, new_property_name, "", "float")
for cell_key in found_property.values:
    cell_mother_key = get_previous_cell_in_lineage(cell_key,lineage_property)
    if cell_mother_key in found_property.values:
        print("delta from "+str(cell_key)+" to "+str(cell_mother_key))
        cell_mother_value = float(found_property.values[cell_mother_key])
        cell_value = float(found_property.values[cell_key])
        delta = cell_value - cell_mother_value
        tc,idc,ch = get_object_t_id_ch(cell_key)
        new_property.set_object_value(tc,idc,ch,delta)
if len(new_property.values) > 0:
    print("Saving generated property to new step")
    new_step = dataset.initialize_external_step(Path(__file__).stem)
    dataset.write_property_to_step(new_step, new_property.name + ".txt", new_property)



