import os
from morphonet.data import get_local_dataset,list_local_datasets
from morphonet.data.dataproperty import DataProperty
import xml.etree.ElementTree as ET
from morphonet.data.utils import indent_xml,generate_init_naming_parameters,remove_folder,generate_prop_naming_parameters,load_properties_from_xml,get_object_t_id_ch,get_symetric_cells,get_node
import argparse
from pathlib import Path
import edist.ted
import edist.uted as uted
def compute_atlas():
    """
    Returns the list of files
    """
    return [
        "atlas_files/pm1.xml",
        "atlas_files/pm3.xml",
        "atlas_files/pm4.xml",
        "atlas_files/pm5.xml",
        "atlas_files/pm7.xml",
        "atlas_files/pm8.xml",
        "atlas_files/pm9.xml",
    ]

parser = argparse.ArgumentParser()


parser.add_argument('-d', "--dataset-name",help="MorphoNet local dataset name")
parser.add_argument('-s', "--segmentation-channel",help="MorphoNet segmentation channel")
args = parser.parse_args()

dataset_name = args.dataset_name
seg_channel = int(args.segmentation_channel)
temp_file_path = "temp_data_"+str(dataset_name).replace(" ", "_")
seg_folder_name = os.path.join(temp_file_path,"seg")
seg_img_name = dataset_name.replace(" ","_")+"_seg_t%03d.mha"


local_json_path_override = None
dataset = get_local_dataset(dataset_name)
if dataset is None :
    datasets_available = list_local_datasets()
    print("Dataset not found, please make sure it exists or there are no typos in the dataset name")
    print(" Here is the list of dataset available : ")
    for dataset in datasets_available:
        print(dataset)
    exit()

min_time = dataset.get_min_time()
max_time = dataset.get_max_time()

output_xml = os.path.join(seg_folder_name,"output.xml")



print("-> Looking for a previous naming")
found_name = False
name_property = dataset.get_property_at("cell_name",dataset.min_time)
if name_property is not None and len(name_property.get_values_at(dataset.min_time)) > 0:
    print("-> Found naming, only keeping t=0")
    found_name = True

cell_lineage = None
property_name = None
cell_contact_surface = None

print("-> Creating missing folders ")

if os.path.isdir(temp_file_path):
    remove_folder(temp_file_path)

if not os.path.isdir(temp_file_path):
    os.makedirs(temp_file_path)

if not os.path.isdir(seg_folder_name):
    os.makedirs(seg_folder_name)


print("-> Extracting images at time point :")
dataset.export_dataset_at_step(seg_channel,seg_img_name,seg_folder_name)

print("-> starting compute of lineage using astec")

command = "mc-cellProperties -segmentation-format "+os.path.join(seg_folder_name,seg_img_name)+" -first "+str(min_time)+" -last "+str(max_time)+" -o "+str(output_xml)+" -property lineage volume surface barycenter principal-value principal-vector contact-surface -v -v -v -v -v"
print(command)
os.system("conda run -n astec "+command)
atlas_path = compute_atlas()

if found_name:
    print("Naming at min time found in the dataset, saving to xml")
    source = open(output_xml)
    tree = ET.parse(source)
    tree = tree.getroot()
    name_property.generate_xml(tree)
    indent_xml(tree)
    mydata = ET.tostring(tree, encoding='utf8', method='xml').decode("utf8")
    myfile = open(output_xml, "w+")
    myfile.write(mydata)
    myfile.close()
else:
    print("No initial naming found, automatically generating it")
    vol_prop = dataset.get_property("volume")
    if vol_prop is not None:
        cell_count = len(vol_prop.get_object_ids_at(dataset.min_time))
        param_file = generate_init_naming_parameters(cell_count,seg_folder_name,"output.xml",atlas_path)
        os.system("conda run -n ascidian ascidian_naming_timepoint -v -v -v -p " + str(
            param_file))
        os.system("rm " + str(param_file))
        os.system("rm " + str(param_file.replace(".", "*.")))
        os.system("rm ascidian_naming_timepoint*.log")
print("Naming propagation")
prop_param_file = generate_prop_naming_parameters(seg_folder_name,"output.xml",atlas_path)
os.system("conda run -n ascidian ascidian_naming_propagation -v -v -v -p " + str(
    prop_param_file))
os.system("rm "+str(prop_param_file))
os.system("rm "+str(prop_param_file.replace(".","*.")))
os.system("rm ascidian_naming_propagation*.log")
if os.path.exists(output_xml):
    print("-> importing lineage in MorphoNet dataset")
    properties = load_properties_from_xml(output_xml)
    step = dataset.initialize_external_step(Path(__file__).stem)
    for property_object in properties:
        if property_object.name == "cell_lineage" or property_object.name == "temporal":
            cell_lineage = property_object
        if property_object.name == "cell_name":
            property_name = property_object
        if property_object.name == "cell_contact_surface":
            cell_contact_surface = property_object
        dataset.write_property_to_step(step,property_object.name+".txt",property_object)
else:
    print("No property file generated")
    os.system("rm -rf " + str(temp_file_path))
    exit()
property_object = DataProperty(dataset, "astec_lineage_distance", "", "float")

if property_name is not None and cell_lineage is not None:
    def local_cost_normalized(x, y):
        if x is None and y is None:
            return 0
        elif x is None or y is None:
            return 1
        elif x not in x_nodes or y not in y_nodes:
            return 1
        xl = x_life[x_nodes.index(x)]
        yl = y_life[y_nodes.index(y)]
        return abs(xl - yl) / (xl + yl)


    # LINEAGE COMPARISON
    # if not cell_id in existing_regions:
    # existing_regions[cell_id] = {}

    for cell_key in property_name.get_keys():
        name = property_name.values[cell_key]
        # print("Trying to compute lineage dist for cell "+str(cell_key)+" with name : "+str(name))
        if name is not None and name.endswith("*"):  # ONLY GET RIGHT SIDE
            # print("found cell on right side ")
            # print("working time is "+str(time))
            tc, idc, chanc = get_object_t_id_ch(cell_key)
            # print("found cell at time : "+str(tc)+" with id "+str(idc))
            smo = get_symetric_cells(tc, property_name, name)
            # print("found symetric : "+str(smo))
            # print("smo : "+str(smo))
            # print("cell_key : "+str(cell_key))
            if smo is not None:
                ts, ids, chans = get_object_t_id_ch(smo)
                x_nodes, x_adj, x_life = get_node(cell_lineage, cell_key)
                # print(x_nodes,x_adj,x_life)
                y_nodes, y_adj, y_life = get_node(cell_lineage, smo)
                # print(y_nodes,y_adj,y_life)
                d = uted.uted(x_nodes, x_adj, y_nodes, y_adj, local_cost_normalized)
                # print(d)
                # exit()
                property_object.set_object_value(tc, idc, seg_channel, d)
                property_object.set_object_value(ts, ids, seg_channel, d)

    dataset.write_property_to_step(step, property_object.name + ".txt", property_object)


property_object = DataProperty(dataset, "cell_neighbors", "", "space")
if cell_contact_surface is not None:
    for id_cell in cell_contact_surface.get_keys():
        neigh = []
        tc,idc,channelc = get_object_t_id_ch(id_cell)
        for id_contact in cell_contact_surface.values[id_cell]:
            if not id_contact in neigh:
                neigh.append(id_contact)
        property_object.set_object_value(tc,idc,channelc,neigh)
    dataset.write_property_to_step(step, property_object.name + ".txt", property_object)
# DELETE ALL TEMP FILES
os.system("rm -rf " + str(temp_file_path))