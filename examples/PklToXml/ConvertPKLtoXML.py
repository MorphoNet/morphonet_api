#!/usr/bin/env python3

import os
import pickle
import time
import sys


from morphonet.tools import write_XML_properties


filename="190919-Emilie_seg_lineage.pkl"


with open(filename, "rb") as f:
    properties = pickle.load(f, encoding="latin1")
        
print(properties)
write_XML_properties(properties,"190919-Emilie_seg_lineage.xml")