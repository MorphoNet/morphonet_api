import os,sys
sys.path.append("../../")
import morphonet
import numpy as np
from morphonet.tools import *

""" This file is used to test MorphoNet API , simply by running it

"""

""" Connection test to MorphoNet

Parameters
----------
mn_login : string
    login on morphonet website
mn_password : string
    password on morphonet website

Returns
-------
mn : MorphoNet.Net
	Instance of the MorphoNet session
"""
def read_secrets(filename):
	if not isfile(filename):filename="/Users/efaure/Projects/160606-MorphoNet/GIT/morphonet_api/build-standalone/secrets.txt"
	Secrets={}
	for line in open(filename,"r"):
		Secrets[line.strip().split(";")[0]]=line.strip().split(";")[1]
	return Secrets

print("->> OK Net Connection")
Secrets=read_secrets("/builds/secrets.txt")

mn=morphonet.Net(Secrets['morphonet-login'],Secrets['morphonet-password'])

""" Retrieve users test to MorphoNet

Parameters
----------
id_guy : int
    id to retrive

Returns
-------
guy : string
	"surname name" of the user , or "User not found"

"""

print("->> OK get_guy_by_id  "+str(mn.get_guy_by_id(1)))
print("->> NO get_guy_by_id "+str(mn.get_guy_by_id(1111111)))

""" Retrieve users test to MorphoNet

Parameters
----------
name : string
    name + surname of user to retrieve
    
Returns
-------
id : int
	id of the user

"""

print("->> OK get_guy_by_name "+str(mn.get_guy_by_name("Gallean Benjamin")))
print("->> OK get_guy_by_name "+str(mn.get_guy_by_name("Faure Emmanuel")))
print("->> NO get_guy_by_name "+str(mn.get_guy_by_name("no one")))

""" Retrieve group test to MorphoNet

Parameters
----------
name : string
    name of the group to retrieve
    
Returns
-------
id : int
	id of the group


"""

print("->> NO1 get_group_by_name "+str(mn.get_group_by_name("")))
print("->> OK get_group_by_name "+str(mn.get_group_by_name("ASTEC")))
print("->> NO2 get_group_by_name "+str(mn.get_group_by_name("zer")))

""" List all dataset created by me

"""

print("->> OK list_my_dataset"); mn.list_my_dataset()

""" List all dataset I can access


"""
#print("->> OK list_dataset"); mn.list_dataset() #BUG

""" Select a dataset as working

Parameters
----------
id : int
    id of the dataset

"""

print("->> OK select_dataset_by_id"); mn.select_dataset_by_id(1)
print("->> NO select_dataset_by_id"); mn.select_dataset_by_id(1111111)

""" Select a dataset as working

Parameters
----------
name : string
    name of the dataset

"""

print("->> OK select_dataset_by_name"); mn.select_dataset_by_name("Phallusia mammillata embryo (Wild type, live SPIM imaging, stages 8-17)")
print("->> NO select_dataset_by_name"); mn.select_dataset_by_name("NONO")

""" Create an empty dataset with 3 time steps and select it

Parameters
----------
name : string 
    name of the dataset on MorphoNet
minTime : int
	begin time point
maxTime : int 
	ending time point

Returns
-------
id : int
	id of the dataset

"""

print("->> OK create_dataset"); id_datset=mn.create_dataset("TEST UNITARY",minTime=0,maxTime=2)

""" Add  a description on created dataset

Parameters
----------
description : string
    description of the dataset on MorphoNet

"""
print("->> OK upload_description"); mn.upload_description("the uploaded description")

""" Update a dataset name (for selected dataset)

Parameters
----------
dataset_name : string
    new name

"""
print("->> OK update_dataset"); mn.update_dataset(dataset_name="NEW TEST NAME")

""" Share the selected dataset with a MorphoNet user

Parameters
----------
id_user : int
    id of the user
role : int
	Role of the user : 0 to share as a reader, 1 as a manager


"""

id_user=1
print("->> OK share_dataset_with_user"); mn.share_dataset_with_user(id_user,1)
print("->> OK unshare_dataset_with_user"); mn.unshare_dataset_with_user(id_user)
#TODO Test other roles 


""" Retrieve a group id by its name 

Parameters
----------
name : string
    name of the group


"""

id_group=mn.get_group_by_name("ASTEC")

""" Share the selected dataset with a MorphoNet group

Parameters
----------
id_group : int
    id of the user
role : int
	Role of the user : 0 to share as a reader, 1 as a manager

"""
print("->> OK share_dataset_with_group"); mn.share_dataset_with_group(id_group,1)
print("->> OK unshare_dataset_with_group"); mn.unshare_dataset_with_group(id_group)



#TODO get_number_of_mesh_at as to return the number, not print it
#MESHES
fileobj="seg_t{:03d}.inr.gz"
for t in range(0,3):
	if mn.get_number_of_mesh_at(t)==0:
		print("->> OK Read Image"); im=imread(fileobj.format(t))
		""" Convert image to morphonet 3d data format

		Parameters
		----------
		im : np.array
			image data
		t : int
			time point in the image name
		background : int
			value of the background in the image
		factor : int
			factor of the downscaling applied to the data (the higher the faster, but data loss could appear)
		
		Returns
		-------
		obj : bytes
			data in the morphonet 3d format, ready for upload
		"""
		print("->> OK convert_to_OBJ"); obj=convert_to_OBJ(im,t,background=1,factor=2)
		""" Upload 3d data on morphonet

		Parameters
		----------
		t : int
		    time of the Dataset to upload
		obj : bytes
			3D data

		"""
		print("->> OK upload_mesh_at "+str(t)); mn.upload_mesh_at(t,obj)
		#TAKE SOME TIMES FOR UPDATE 
		if t==0:
			""" Retrieve the 3d data for the selected dataset at a time point t

			Parameters
			----------
			t : int
				time point on the dataset

			Returns
			-------
			obj : bytes
				data on the morphonet server
			"""
			print("->> OK get_mesh_at "+str(t)); objout=mn.get_mesh_at(t);
			if objout is not None:
				print(" --> "+str(len(objout))+" chars")
			else:
				print(" --> None ")

print("->> NO get_mesh_at "+str(t)); objout=mn.get_mesh_at(5); 
if objout is not None:
	print(" --> "+str(len(objout))+" chars")
else:
	print(" --> None ")

""" Retrieve the number of mesh for a time point in the selected dataset
Parameters
----------
t : int
	time point on the dataset

Returns
-------
number : int
	number of mesh
"""

print("->> OK get_number_of_mesh_at 0 : "+str(mn.get_number_of_mesh_at(0)))
print("->> OK get_number_of_mesh_at 5 : "+str(mn.get_number_of_mesh_at(5)))

""" DELETE all 3d data on MorphoNet , for the selected dataset at the given time point
Parameters
----------
t : int
	time point on the dataset

"""

print("->> OK clear_mesh_at 0 "); mn.clear_mesh_at(0)
print("->> OK clear_mesh_at 5 "); mn.clear_mesh_at(5)


""" Read the 3d data in a file
Parameters
----------
path : string
	path to the data

Returns
-------
data : bytes
	3d data for the file
"""

print("->> OK read_mesh"); data=mn.read_mesh("sphere.obj")
#print("TODO ->> OK upload_primitive ");mn.upload_primitive("sphere",prim)

print("->> OK read_mesh"); prim_sphere=mn.read_mesh("primwithsphere.obj")

""" Add a primitive for the dataset on MorphoNet at given time point
Parameters
----------
t : int
	time point
prim_sphere : bytes
	3d data for the primitive

"""

print("->> OK upload_mesh_with_primitive_at");mn.upload_mesh_with_primitive_at(0,prim_sphere)

""" Delete a primitive by its name on the server
Parameters
----------
name : string
	name of the primitive mesh to remove on MorphoNet

"""

print("->> OK delete_primitive ");mn.delete_primitive("sphere")
print("->> OK delete_primitive ");mn.delete_primitive("cube")

""" Removes all primitive on MorphoNet

"""
print("->> OK delete_primitives ");mn.delete_primitives()

#RAWIMAGES
factor=2 #specify the rescale Factor
filenameraw="fuse_t{:03d}.inr.gz"
for t in range(3):
	im = imread(filenameraw.format(t))[:,:,:,0,0]
	im=np.uint8(255*np.float32(im[::factor,::factor,::factor])/im.max())  #Convert it in 8 bits
	""" Upload a raw image for the selected dataset at time point
	Parameters
	----------
	t : int
		time point
	im : np.array with uint8 value format
		Raw images data
	scale : int
		scaling of the given image
	"""
	print("->> OK upload_image_at "+str(t));mn.upload_image_at(t,im,scale=factor)
	""" Check if given time point contains a raw image 
	Parameters
	----------
	t : int
		time point

	Returns
	-------
	flag : bool
		True if Raw Images exist on MorphoNet server at this time point , False otherwise
	"""
	print("->> OK is_image_at "+str(t)); mn.is_image_at(t)
	""" If exist, retrieve image for the given time point
	Parameters
	----------
	t : int
		time point

	Returns
	-------
	rawim : np.array
		data for the raw image
	"""
	print("->> OK get_image_at "+str(t)); rawim=mn.get_image_at(t)

""" delete raw image on the server for a given time point
Parameters
----------
t : int
	time point

"""
print("->> OK delete_image_at "+str(0)); mn.delete_image_at(0)
print("->> OK delete_image_at "+str()); mn.delete_image_at(5)

""" Delete all raw images on the server

"""

print("->> OK delete_images "); mn.delete_images()

#PROPERTIES
""" List all properties type available

"""
print("->> OK show_properties_type ");mn.show_properties_type()
volume=read_file("volume.txt")

""" Upload an property for the selected dataset
Parameters
----------
name : string
	name of the property 
text : string
	list of lines for the property, matching morphonet property format
Returns
-------
id : int
	id of the property created
"""
print("->> OK upload_property ");id_property=mn.upload_property("volume",volume); print(" --> id_property="+str(id_property))

""" List all of the properties current user can access on the dataset

"""
print("->> OK get_properties ");print(mn.get_properties())

""" Retrieve property by name on the dataset

name : string
	name of the property 

Returns
-------
data : string
	text of the property stored on the server
"""
print("->> OK get_property_by_name ");print(mn.get_property_by_name("volume"))

""" Retrieve property by name on the dataset

name : string
	name of the property 

Returns
-------
data : string
	text of the property stored on the server
"""
print("->> OK get_property_by_id ");print(mn.get_property_by_id(id_property))
print("->> NO get_property_by_id ");print(mn.get_property_by_id(0))

""" Get the list of object of an property specified by its id

Parameters
----------
id_property : int
    ID of the property 

Returns
-------
objects : list
    List of key/value corresponding to a split to the property data

"""""
print("->> OK get_objects_from_property_by_id ");print(mn.get_objects_from_property_by_id(id_property))
print("->> NO get_objects_from_property_by_id ");print(mn.get_objects_from_property_by_id(0))

""" Get the list of object of an property specified by its name

Parameters
----------
name : string
    name of the property 

Returns
-------
objects : list
    List of key/value corresponding to a split to the property data

"""
print("->> OK get_objects_from_property_by_name ");print(mn.get_objects_from_property_by_name("volume"))

""" The property specified by its id become accessible to everyone you shared it (or public if you shared the property with public)

Parameters
----------
id_property : int
    ID of the property 

"""
print("->> OK share_property_by_id ");mn.share_property_by_id(id_property)
print("->> NO share_property_by_id ");mn.share_property_by_id(0)

""" The property specified by its id become unaccessible to everyone you shared it (or public if you shared the property with public)

Parameters
----------
id_property : int
	ID of the property 

"""
print("->> OK unshare_property_by_id ");mn.unshare_property_by_id(id_property)

#TODO TEST WITH REAL CURATION
#print("TODO ->> OK get_curation ");mn.get_curation_by_id(id_property)
#print("TODO ->> OK get_property_curated_by_id ");mn.get_property_curated_by_id(id_property)

""" Delete an property specified by its name on the server

Parameters
----------
name : string
    Name of the property 
"""
print("->> OK delete_property_by_name ");mn.delete_property_by_name("volume")
print("->> NO delete_property_by_name ");mn.delete_property_by_name("sds")

volume=read_file("volume.txt")
print("->> OK upload_property ");id_property=mn.upload_property("volume",volume); print(" --> id_property="+str(id_property))
""" Delete an property specified by its id on the server

Parameters
----------
id : int
	Id of the property 

"""
print("->> OK delete_property_by_id ");mn.delete_property_by_id(id_property)
print("->> NO delete_property_by_id ");mn.delete_property_by_id(0)

mn.update_dataset(id_NCBI=0)
""" Retrieve the corresponding developmental table of the specie of the dataset (avaible only for Ascidian )
 return the list of developmentale table property (id,id_datasettype,period,stage,developmentaltstage,description,hpf)
"""
print("->> NO get_developmental_table ="+str(mn.get_developmental_table()))

""" Retrieve the list of stages for this specie
FROM "anissed all stages"
return a dictionnary with stage database id as key and  (Stage) as value
"""
print("->> NO get_stages  ="+str(mn.get_stages()))

""" Retrieve the list of cells (with their expression value) for the gene passed in argument (gene id is the id inside the database)
return a dictionnary with database id as key as value tuple containing (cell,stage,value)
"""
print("->> NO get_cells_by_gene "+str(mn.get_cells_by_gene(15270)))
""" Retrieve the list of cells (with their expression value) for the gene and the stage passed in argument (gene id and stage id are the id inside the database)
return a dictionnary with database id as key as value tuple containing (cell,value)
"""

print("->> NO get_cells_by_gene_by_stage "+str(mn.get_cells_by_gene_by_stage(15270,52)))

mn.update_dataset(id_NCBI=59560)
print("->> OK get_developmental_table "+str(len(mn.get_developmental_table())));
print("->> OK get_stages "+str(len(mn.get_stages())))
print("->> OK get_cells_by_gene "+str(mn.get_cells_by_gene(15270)))
print("->> NO get_cells_by_gene "+str(mn.get_cells_by_gene(1)))
print("->> OK get_cells_by_gene_by_stage "+ str(mn.get_cells_by_gene_by_stage(15270,52)))

""" Retrieve the list of genes for this specie
return a list with (id,Gene Model, Gene Name, Unique Gene id)
"""
print("->> OK get_genes "+str(len(mn.get_genes())))

""" Retrieve the list of genes (with their expression value) for the cell name in argument
return a dictionnary with database id as key as value tuple containing (stage,gene,value)
"""
print("->> OK get_genes_by_cell "+str(len(mn.get_genes_by_cell("a7.8"))))

""" Retrieve the list of genes (with their expression value) for the stage id  in argument
return a dictionnary with database id as key as value tuple containing (gene,cell,value)
"""
print("->> OK get_genes_by_stage "+str(len(mn.get_genes_by_stage(43))))

""" Remove the 3D data and all properties for the selected dataset
"""
print("->> OK clear_dataset"); mn.clear_dataset()
""" Remove the selected dataset from the server using its id
"""
print("->> OK delete_dataset"); mn.delete_dataset()

