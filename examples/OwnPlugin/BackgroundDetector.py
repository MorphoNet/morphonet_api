#!/usr/bin/env python3
import numpy as np
from morphonet.plugins import MorphoPlugin


class BackgroundDetector(MorphoPlugin):# The plugin has to heritate from the MorphoPlugin class

    def __init__(self): #PLUGIN DEFINITION
        MorphoPlugin.__init__(self)
        self.set_name("Background Detector") #Set the Name of the plugin
        self.set_parent("Create Segmentation") #Set the type of plugin
        self.set_description("This plugin perform a segmentation of the background") #Set a description
        self.add_inputfield("Voxel Offset", default=10) # Define plugin variables

    def process(self, t, dataset, objects): #PLUGIN EXECUTION
        if not self.start(t,dataset,objects,objects_require=False): #Start function required
            return None

        from skimage.morphology import binary_dilation,binary_erosion
        from morphonet.tools import printv
        offset = float(self.get_inputfield("Voxel Offset")) #Retrieve the paramets value
        rawdata = dataset.get_raw(t) #Retrieve the raw data at this time point
        (unique, counts) = np.unique(rawdata, return_counts=True) #Look at the pixels distribution
        idx = np.where(counts == counts.max()) #Take the one which the highest number (supposed that the images contains mainly background...)
        background=unique[idx][0] #Get the value of the background
        printv("Found background value at "+str(background),0) #Send the message to the 3D viewer
        background_mask=rawdata<background+offset #Create a firsrt mask of the background
        background_mask=binary_erosion(binary_dilation(background_mask)) #Remove some noise
        data=1-background_mask # We want the inverse the mask of the background
        dataset.set_seg(t, data) # Set the segmentation as this time point
        self.restart() #Restart function required


#Now we can launch MorphoNet in Dev mode in order to test the plugin
from morphonet import Plot #Import the communcation protocol
mc=Plot(verbose=3) # Sepecify the verbose mode
mc.set_dataset(raw="Astec-pm1_intrareg_fuse_t001.tiff.gz") #Create the dataset  with only one intensity imagge
mc.add_plugin(BackgroundDetector()) #Add the Plugin
mc.curate(load_default_plugins=False) #Launch the communication with the 3D Viewer (without the default plugins)

