# Add your own plugin !

## In order to create your own plugin, you have to  : 

Create a conda environnement (with the pip package)
```
conda create -n morphonet_env  pip
```

Activate the environnement 
```
conda activate morphonet_env
```

Install the morhponet python API
```
pip install morphonet
```



Launch your code 
```
python BackgroundDetector.py
```


and finally open the MorphoNet Standalone application and click on the dev mode ! 
