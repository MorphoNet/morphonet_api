from Particle import Particle
from Spring import Spring
from Vector3D import Vector3D
from Attraction import Attraction
from RungeKuttaIntegrator import RungeKuttaIntegrator
class ParticleSystem:

  RUNGE_KUTTA = 0
  MODIFIED_EULER = 1

  DEFAULT_GRAVITY = 0
  DEFAULT_DRAG = 0.001	


  def __init__( self, grav=Vector3D(), somedrag=DEFAULT_DRAG ):
    self.integrator = RungeKuttaIntegrator( self )
    self.particles = []
    self.springs = []
    self.attractions = []
    self.gravity = grav
    self.drag = somedrag
    self.customForces = []



  def setGravity( self, x,  y,  z ):
    self.gravity.set( x, y, z )

  # default down gravity
  def setGravity(self,  g ):
    self.gravity.set( 0, g, 0 )

  def setDrag( self, d ):
    self.drag = d

  def tick(self,  t=1 ):
    self.integrator.step( t )

  def makeParticle(self,  mass,  x,  y,  z ):
    p = Particle( mass )
    p.position.set( x, y, z )
    self.particles.append( p )
    return p

  def makeSpring( self, a,  b,  ks,  d,  r ):
    s =  Spring( a, b, ks, d, r )
    self.springs.append( s )
    return s

  def makeAttraction(  self,a,  b,  k,  minDistance ):
    m =  Attraction( a, b, k, minDistance )
    self.attractions.append( m )
    return m

  def clear(self):
    self.particles.clear()
    self.springs.clear()
    self.attractions.clear()

  def tension(self):
    avtension=0.0
    for s in self.springs:
        avtension+=s.stress()
    return avtension/len(self.springs)

  def applyForces(self):
    
    '''
    if not self.gravity.isZero() : 
      for p in self.particles:
        p.force.add( self.gravity )
  
    for p in self.particles:
      p.force.add( p.velocity.x * -self.drag, p.velocity.y * -self.drag, p.velocity.z * -self.drag )
    '''

    for f in self.springs:
      f.apply()

    
    for f in self.attractions:
      f.apply()

    for f in self.customForces:
      f.apply()
    


  def clearForces(self):
    for p in self.particles:
      p.force.clear()


  def numberOfParticles(self):
    return len(self.particles)

  def numberOfSprings(self):
    return len(self.springs)

  def numberOfAttractions(self):
    return len(self.attractions)

  def getParticle( self, i ):
    return self.particles[i]

  def getSpring( self, i ):
    return self.springs[i]

  def getAttraction( self, i ):
    return self.attractions[i]

  def addCustomForce( self, f ):
    self.customForces.append( f )

  def numberOfCustomForces(self):
    return len(self.customForces)

  def getCustomForce( self, i ):
    return len(self.customForces[i])

  def removeCustomForce( self, i ):
    return self.customForces.remove( i )

  def removeParticle( self, p ):
    self.particles.remove( p )

  def removeSpring( self, a ):
    self.springs.remove( a )

  def removeAttraction(self, s ):
    self.attractions.remove( s )

  

  

