from math import sqrt

class Spring :

	def __init__(self,A,  B,  ks,  d,  r ):
		self.a = A
		self.b = B
		self.springConstant = ks
		self.damping = d
		self.restlength = r
		self.on = True


	def turnOff(self):
		self.on = False


	def turnOn(self):
		self.on = True


	def isOn(self):
		return self.on

	def isOff(self):
		return not self.on


	def getOneEnd(self):
		return self.a


	def getTheOtherEnd(self):
		return self.b


	def currentLength(self):
		return self.a.position.distanceTo( self.b.position )


	def restLength(self):
		return self.restlength

	def stress(self):
		cl=self.currentLength()
		if self.restlength>cl:
			return self.restlength/cl
		return cl/self.restlength

	def strength(self):
		return self.springConstant


	def setStrength( self, ks ):
		self.springConstant = ks


	def damping(self):
		return self.damping


	def setDamping( self, d ):
		self.damping = d


	def setRestLength( self, l ):
		self.restlength = l

	  
	def apply(self):
		if self.on and ( self.a.isFree() or self.b.isFree() ):
			a2bX = self.a.position.x - self.b.position.x
			a2bY = self.a.position.y - self.b.position.y
			a2bZ = self.a.position.z - self.b.position.z
		
			a2bDistance = sqrt( a2bX*a2bX + a2bY*a2bY + a2bZ*a2bZ )
			
			if  a2bDistance == 0 :
				a2bX = 0
				a2bY = 0
				a2bZ = 0
			else:
				a2bX /= a2bDistance
				a2bY /= a2bDistance
				a2bZ /= a2bDistance
			
			#spring force is proportional to how much it stretched 
			
			springForce = -( a2bDistance - self.restlength ) * self.springConstant 
			
			#want velocity along line b/w a & b, damping force is proportional to this
			
			Va2bX = self.a.velocity.x - self.b.velocity.x
			Va2bY = self.a.velocity.y - self.b.velocity.y
			Va2bZ = self.a.velocity.z - self.b.velocity.z
			               		
			dampingForce = -self.damping * ( a2bX*Va2bX + a2bY*Va2bY + a2bZ*Va2bZ )
			
			#forceB is same as forceA in opposite direction
			
			r = springForce + dampingForce
			
			a2bX *= r
			a2bY *= r
			a2bZ *= r
		    
			if  self.a.isFree():
				self.a.force.add( a2bX, a2bY, a2bZ )
			if self.b.isFree():
				self.b.force.add( -a2bX, -a2bY, -a2bZ )



	def setA( self, p ):
		self.a = p


	def setB( self, p ):
		self.b = p
