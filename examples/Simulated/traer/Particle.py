from Vector3D import Vector3D


class Particle:

  def __init__ (self,  m ):
    self.position = Vector3D()
    self.velocity = Vector3D()
    self.force =  Vector3D()
    self.mass = m
    self.fixed = False
    self.age = 0
    self.dead = False


   
  def distanceTo(self, p ):
    return self.position.distanceTo( p.position )

  def makeFixed(self):
    self.fixed = True
    self.velocity.clear()


  def setPosition(self,x,y,z):
    self.position.x=x
    self.position.y=y
    self.position.z=z


  def resetVelocity(self):
    self.velocity.clear()

  def isFixed(self):
    return self.fixed

  def isFree(self):
    return not self.fixed

  def makeFree(self):
    self.fixed = False


  def setMass(self,m):
    self.mass = m


  def age(self):
    return self.age

  def reset(self):
    self.age = 0
    self.dead = False
    self.position.clear()
    self.velocity.clear()
    self.force.clear()
    self.mass = 1.0

  def toString(self):
    return "Position :"+self.position.toString()+", Velocity : "+self.velocity.toString()
