from Vector3D import Vector3D
class  RungeKuttaIntegrator :
	
	def __init__( self,  s ):
		self.s = s
		
		self.originalPositions = []
		self.originalVelocities = []
		self.k1Forces = []
		self.k1Velocities = []
		self.k2Forces = []
		self.k2Velocities = []
		self.k3Forces = []
		self.k3Velocities = []
		self.k4Forces = []
		self.k4Velocities = []
	
	
	def allocateParticles(self):
		while len(self.s.particles) > len(self.originalPositions) :
			self.originalPositions.append(  Vector3D() )
			self.originalVelocities.append(  Vector3D() )
			self.k1Forces.append(  Vector3D() )
			self.k1Velocities.append(  Vector3D() )
			self.k2Forces.append(  Vector3D() )
			self.k2Velocities.append(  Vector3D() )
			self.k3Forces.append(  Vector3D() )
			self.k3Velocities.append(  Vector3D() )
			self.k4Forces.append(  Vector3D() )
			self.k4Velocities.append(  Vector3D() )

	def  step( self, deltaT ):
		self.allocateParticles()
		# save original position and velocities
		
		for i in range(len(self.s.particles)):
			p = self.s.particles[i]
			if p.isFree():
				self.originalPositions[i].set( p.position )
				self.originalVelocities[i].set( p.velocity )
			
			p.force.clear()	# and clear the forces
		
		
		# get all the k1 values
		self.s.applyForces()
		
		#save the intermediate forces
		for i in range(len(self.s.particles)):
			p = self.s.particles[i]
			if p.isFree():
				self.k1Forces[i].set( p.force )
				self.k1Velocities[i].set( p.velocity )
			p.force.clear()
		
		
		# get k2 values
		for i in range(len(self.s.particles)):
			p = self.s.particles[i]
			if p.isFree():
				originalPosition = self.originalPositions[i]
				k1Velocity = self.k1Velocities[i]
				
				p.position.x = originalPosition.x + k1Velocity.x * 0.5 * deltaT
				p.position.y = originalPosition.y + k1Velocity.y * 0.5 * deltaT
				p.position.z = originalPosition.z + k1Velocity.z * 0.5 * deltaT
				
				originalVelocity = self.originalVelocities[i]
				k1Force = self.k1Forces[i]
				
				p.velocity.x = originalVelocity.x + k1Force.x * 0.5 * deltaT / p.mass
				p.velocity.y = originalVelocity.y + k1Force.y * 0.5 * deltaT / p.mass
				p.velocity.z = originalVelocity.z + k1Force.z * 0.5 * deltaT / p.mass
		
		self.s.applyForces()

		#save the intermediate forces
		for i in range(len(self.s.particles)):
			p = self.s.particles[i]
			if p.isFree():
				self.k2Forces[i].set( p.force )
				self.k2Velocities[i].set( p.velocity )
			p.force.clear()	# and clear the forces now that we are done with them
		
		# get k3 values
		for i in range(len(self.s.particles)):
			p = self.s.particles[i]
			if p.isFree():
				originalPosition = self.originalPositions[i]
				k2Velocity = self.k2Velocities[i]
				
				p.position.x = originalPosition.x + k2Velocity.x * 0.5 * deltaT
				p.position.y = originalPosition.y + k2Velocity.y * 0.5 * deltaT
				p.position.z = originalPosition.z + k2Velocity.z * 0.5 * deltaT
				
				originalVelocity = self.originalVelocities[i]
				k2Force = self.k2Forces[i]
				
				p.velocity.x = originalVelocity.x + k2Force.x * 0.5 * deltaT / p.mass
				p.velocity.y = originalVelocity.y + k2Force.y * 0.5 * deltaT / p.mass
				p.velocity.z = originalVelocity.z + k2Force.z * 0.5 * deltaT / p.mass
	
		
		self.s.applyForces()
		
		#save the intermediate forces
		for i in range(len(self.s.particles)):
			p = self.s.particles[i]
			if p.isFree():
				self.k3Forces[i].set( p.force )
				self.k3Velocities[i].set( p.velocity )
			p.force.clear()	# and clear the forces now that we are done with them
		
		
		
		#get k4 values
		for i in range(len(self.s.particles)):
			p = self.s.particles[i]
			if p.isFree():
				originalPosition = self.originalPositions[i]
				k3Velocity = self.k3Velocities[i]
				
				p.position.x = originalPosition.x + k3Velocity.x * deltaT
				p.position.y = originalPosition.y + k3Velocity.y * deltaT
				p.position.z = originalPosition.z + k3Velocity.z * deltaT
				
				originalVelocity = self.originalVelocities[i]
				k3Force = self.k3Forces[i]
				
				p.velocity.x = originalVelocity.x + k3Force.x * deltaT / p.mass
				p.velocity.y = originalVelocity.y + k3Force.y * deltaT / p.mass
				p.velocity.z = originalVelocity.z + k3Force.z * deltaT / p.mass

		self.s.applyForces()
		
		#save the intermediate forces
		for i in range(len(self.s.particles)):
			p = self.s.particles[i]
			if p.isFree():
				self.k4Forces[i].set( p.force )
				self.k4Velocities[i].set( p.velocity )
		
		#put them all together and what do you get?
		
		for i in range(len(self.s.particles)):
			p = self.s.particles[i]
			p.age += deltaT
			if p.isFree():
				# update position
				
				originalPosition = self.originalPositions[i]
				k1Velocity = self.k1Velocities[i]
				k2Velocity = self.k2Velocities[i]
				k3Velocity = self.k3Velocities[i]
				k4Velocity = self.k4Velocities[i]
				
				p.position.x = originalPosition.x + deltaT / 6.0 * ( k1Velocity.x + 2.0*k2Velocity.x + 2.0*k3Velocity.x + k4Velocity.x )
				p.position.y = originalPosition.y + deltaT / 6.0 * ( k1Velocity.y + 2.0*k2Velocity.y + 2.0*k3Velocity.y + k4Velocity.y )
				p.position.z = originalPosition.z + deltaT / 6.0 * ( k1Velocity.z + 2.0*k2Velocity.z + 2.0*k3Velocity.z + k4Velocity.z )
				
				#update velocity
				
				originalVelocity = self.originalVelocities[i]
				k1Force = self.k1Forces[i]
				k2Force = self.k2Forces[i]
				k3Force = self.k3Forces[i]
				k4Force = self.k4Forces[i]
				
				p.velocity.x = originalVelocity.x + deltaT / ( 6.0 * p.mass ) * ( k1Force.x + 2.0*k2Force.x + 2.0*k3Force.x + k4Force.x )
				p.velocity.y = originalVelocity.y + deltaT / ( 6.0 * p.mass ) * ( k1Force.y + 2.0*k2Force.y + 2.0*k3Force.y + k4Force.y )
				p.velocity.z = originalVelocity.z + deltaT / ( 6.0 * p.mass ) * ( k1Force.z + 2.0*k2Force.z + 2.0*k3Force.z + k4Force.z )
