#attract positive repel negative

from Particle import Particle
from math import sqrt
class Attraction :
	
	def __init__( self, a,  b,  k,  distanceMin ):
		self.a = a
		self.b = b
		self.k = k
		self.on = True
		self.distanceMin = distanceMin
		self.distanceMinSquared = distanceMin*distanceMin


	def setA( self, p ):
		self.a = p

	def setB(  self, p ):
		self.b = p

	def getMinimumDistance(self):
		return self.distanceMin

	def setMinimumDistance(  self, d ):
		self.distanceMin = d
		self.distanceMinSquared = d*d

	def turnOff(self):
		self.on = False

	def turnOn(self):
		self.on = True

	def setStrength( self,  k ):
		self.k = k

	def getOneEnd(self):
		return self.a

	def getTheOtherEnd(self):
		return self.b

	def apply(self):
		if self.on and (self.a.isFree() or self.b.isFree() ) :
			a2bX =self.a.position.x - self.b.position.x
			a2bY = self.a.position.y - self.b.position.y
			a2bZ = self.a.position.z - self.b.position.z

			a2bDistanceSquared = a2bX*a2bX + a2bY*a2bY + a2bZ*a2bZ

			if a2bDistanceSquared < self.distanceMinSquared :
				a2bDistanceSquared = self.distanceMinSquared

			force = self.k * self.a.mass * self.b.mass / a2bDistanceSquared

			length = sqrt( a2bDistanceSquared )
			
			#make unit vector
			
			a2bX /= length
			a2bY /= length
			a2bZ /= length
			
			#multiply by force 
			
			a2bX *= force
			a2bY *= force
			a2bZ *= force

			#apply
			
			if self.a.isFree():
				self.a.force.add( -a2bX, -a2bY, -a2bZ )
			if self.b.isFree():
				self.b.force.add( a2bX, a2bY, a2bZ )
		
	def getStrength(self):
		return k

	def isOn():
		return on

	def isOff():
		return not on

