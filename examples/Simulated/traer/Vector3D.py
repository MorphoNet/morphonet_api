
from math import sqrt

class Vector3D:

	def __init__(self, *args, **kwargs):
		if len(args)==1:
			p=args[0]
			self.x = p.x
			self.y = p.y
			self.z = p.z
		elif len(args)==3:
			self.x = args[0]
			self.y = args[1]
			self.z = args[2]
		else :
			self.x = 0
			self.y = 0
			self.z = 0

	
	def setX(  self,X ) :
		self.x = X
	def setY( self, Y ) :
		self.y = Y
	def setZ( self, Z ) :
		self.z = Z

	def set( self,*args, **kwargs):
		if len(args)==1:
			p=args[0]
			self.x = p.x
			self.y = p.y
			self.z = p.z
		elif len(args)==3:
			self.x = args[0]
			self.y = args[1]
			self.z = args[2]


	def add(self, *args, **kwargs):
		if len(args)==1:
			p=args[0]
			self.x += p.x
			self.y += p.y
			self.z += p.z
		elif len(args)==3:
			self.x += args[0]
			self.y += args[1]
			self.z += args[2]


	def subtract(self, p ):
		if len(args)==1:
			p=args[0]
			self.x -= p.x
			self.y -= p.y
			self.z -= p.z
		elif len(args)==3:
			self.x -= args[0]
			self.y -= args[1]
			self.z -= args[2]


	def multiplyBy(self, f ):
		self.x *= f
		self.y *= f
		self.z *= f

	def distanceSquaredTo(self,p):
		 dx = self.x-p.x
		 dy = self.y-p.y
		 dz = self.z-p.z
		 return dx*dx + dy*dy + dz*dz;

	def distanceTo(self,p):
		return sqrt( self.distanceSquaredTo( p ) )

	'''
	def distanceTo(self,X, Y, Z ):
		dx = self.x - X;
		dy = self.y - Y;
		dz = self.z - Z;
		return sqrt( dx*dx + dy*dy + dz*dz )
	'''

	def dot(self,p ):   
		return x*p.x + y*p.y + z*p.z

	def length(self) :  
		return sqrt( self.x*self.x + self.y*self.y + self.z*self.z )

	def lengthSquared(self)	:
		return self.x*self.x + self.y*self.y + self.z*self.z 
	  
	def clear(self) :
		self.x = 0
		self.y = 0
		self.z = 0

	def toString(self)   :    
		return  "(" + str(self.x) + ", " +str(self.y) + ", " + str(self.z) + ")"


	def cross(self, p ):
		return Vector3D( 	self.y*p.z - self.z*p.y,  self.x*p.z - self.z*p.x, self.x*p.y - self.y*p.x )

	def isZero(self):
		if self.x==0 and self.y==0 and self.z==0:
			return True
		return False


