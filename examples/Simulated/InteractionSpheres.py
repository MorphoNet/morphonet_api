#!/usr/bin/env python
# -*- coding: latin-1 -*-

import sys,os
import numpy as np
import pickle as pkl
import math
sys.path.append('traer')
from ParticleSystem import ParticleSystem



id_cell=0

def volume(radius):
	return 4.0/3.0*math.pi*math.pow(radius,3)
def radius(vol):
	return math.pow(vol/(math.pi*(4.0/3.0)),1.0/3.0)


class Cell():
	def __init__(self,radius,life,coord): # CONSTRUCTATORRRR
		global id_cell
		self.id=id_cell
		id_cell+=1
		self.radius=radius
		self.neis=[] #Neigbhors
		self.springsNeis=[]
		self.p=physics.makeParticle(1.0,coord[0], coord[1], coord[2])
		self.cell_life=life
		self.life=life

	def center(self):
		return [self.p.position.x,self.p.position.y,self.p.position.z]

	def addNeigbhor(self,nei):
		if nei not in self.neis:
			self.neis.append(nei)
			self.springsNeis.append(physics.makeSpring(self.p,nei.p, 0.001, 0.001,self.radius+nei.radius))
		
	def clearNeigbhors(self):
		self.neis=[] #Clear Neigbhors
		for s in self.springsNeis: #Remove Neigbhors Spring
			physics.removeSpring(s)	
		self.springsNeis=[]




class Embryo():
	def __init__(self,cell_radius=100,cell_life=50,step_physics=1,maxTime=500,plotTime=10): # CONSTRUCTATORRRR
		self.cells=[]
		self.cell_life=cell_life
		c=Cell(cell_radius,cell_life,[0,0,0])
		self.cells.append(c)
		self.step_physics=step_physics
		self.maxTime=maxTime
		self.t=0
		self.plotTime=plotTime
		self.lineage="#Lineage Simulated\n"
		self.lineage+="type:time\n"


	def clearNeigbhors(self):
		for c in self.cells:
			c.clearNeigbhors()

	def Neigbhors(self,coeff=1.5):
		self.clearNeigbhors()
		nbNei=0
		for c1 in self.cells:
			distanceNeis={}
			for c2 in self.cells:
				if c1.id<c2.id:
					d=c1.p.distanceTo(c2.p)
					if d>0 and d<coeff*(c1.radius+c2.radius):
						c1.addNeigbhor(c2)
						nbNei+=1

		print('----> Total Number Of Neigbhors ='+str(nbNei) +" -> "+str(physics.numberOfSprings())+" springs ")



	def live(self):
		while self.t<self.maxTime:
			if self.t%self.plotTime==0:
				self.plot()
			physics.tick(self.step_physics)

			if len(self.cells)<1000:
				self.isDivide()
			#self.Neigbhors()

			if len(self.cells)>1:
				print('-> at '+str(self.t)+ ", " +str(len(self.cells))+ " cells ->  Average forces "+str(physics.tension()))
			else:
				print('-> at '+str(self.t)+ "-> no springs")
			self.t+=1
		print("Done !")

	def getTime(self):
		return int(self.t/self.plotTime) #Time of plot

	def plot(self):
		tp=self.getTime()
		obj="#Test Primitives\n"
		for c in self.cells:
			obj+="p "+str(tp)+","+str(c.id)+" sphere ("+str(c.p.position.x)+","+str(c.p.position.y)+","+str(c.p.position.z)+") "+str(c.radius)+" (1,1,1,1) \n"
		mp.plotAt(tp,obj)

	def plot_lineage(self):
		f=open('lineage.txt',"w");f.write(self.lineage);f.close()
		mp.plot_properties("Lineage",self.lineage)

	def divide(self,c,aug_life=5):#Create a new Cell after Division
		c.radius=radius(volume(c.radius)/2)
		sister=Cell(c.radius,c.cell_life+np.random.randint(aug_life),c.center()+np.random.rand(3))
		s=physics.makeSpring(c.p,sister.p, 0.04, 0.1,c.radius+sister.radius) #0.05,0.1
		c.life=c.cell_life+np.random.randint(aug_life)
		self.cells.append(sister)
		return sister
		

	def isDivide(self):
		tp=self.getTime()
		for c in self.cells:
			#if self.t%self.plotTime==0:
			self.lineage+=str(tp)+","+str(c.id)+":"+str(tp+1)+","+str(c.id)+"\n"
			if c.life<=0:#or np.random.randint(c.life)==0:
				newcell=self.divide(c)
				#if self.t%self.plotTime==0:
				self.lineage+=str(tp)+","+str(c.id)+":"+str(tp+1)+","+str(newcell.id)+"\n"
			c.life-=1
	

with open("sphere.obj",'r') as f:
    obj = f.read()

import morphonet
mp=morphonet.Plot()
mp.addPrimitive("sphere",obj)

physics = ParticleSystem()

e=Embryo(maxTime=500,plotTime=10)
e.live()
e.plot_lineage()



