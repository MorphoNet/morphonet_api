#!/usr/bin/env python3
# -*- coding: latin-1 -*-
from morphonet.tools import read_XML_properties,get_fate_colormap,write_property
import argparse,os
ap = argparse.ArgumentParser()
ap.add_argument("xml", type=str, help="XML properties files")
ap.add_argument("-f", "--fate", default="2020",type=str,   help="fate version (2020 or 2009) Default: 2020")
ap.add_argument("-p", "--property", default="", type=str, help="Property to extract from properties files")
ap.add_argument("-t", "--type", default="", type=str, help="Type of the property the property to extract  ('list' to visualize all infos")

args = vars(ap.parse_args())
fate_version=args['fate']
xml_file=args['xml']
property_todo=args['property']
property_todo_type=args['type']
if property_todo_type=="":
    property_todo_type=None

prop_path=os.path.dirname(xml_file)
properties=read_XML_properties(xml_file)
if properties is not None:
    if property_todo=="list":#LIST ALL propertyS
        for elt in properties:
            print(" -> found '"+str(elt)+"'")
    elif property_todo!="": #ONE property
        if property_todo not in properties:
            print( " ->> Miss "+property_todo + " in "+xml_file)
        else:
            write_property(property_todo + ".txt",properties[property_todo], property_todo, property_todo_type)
            if property_todo.find("fate") >= 0:  # SEPECIF CONVERTER TO FATE
                write_property(property_todo + "_selection_"+str(fate_version)+".txt", properties[property_todo], property_todo, "label",convert=get_fate_colormap(fate_version))

    else: #PROCESS ALL propertyS
        for property_name in properties:
            write_property( property_name + ".txt",properties[property_name], property_name, None)
            if property_name.find("fate") >= 0:  # SEPECIF CONVERTER TO FATE
                write_property(property_name +"_selection_"+str(fate_version)+".txt", properties[property_name], property_name, "label",convert=get_fate_colormap(fate_version))

