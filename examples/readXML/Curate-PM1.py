# -*- coding: latin-1 -*-
import morphonet
mc=morphonet.Plot(clear_temp=False)
mc.set_dataset(begin=1,end=2,segment="SEG/Astec-Pm1_post_f1_t{:03d}.inr.gz",xml_file="Astec-pm1_correction_man_KB_fate.xml")

for info in mc.get_infos():
    print(info)
fate=mc.get_info("cell_fate4") #Fate Name
fate_selection=mc.create_info("cell_fate_colored","selection")

from morphonet.tools import get_fate_colormap
colormap=get_fate_colormap("2020")
for el in fate.data:
    f=fate.get(el)
    if f not in colormap:
        print(" --> Miss "+f)
    else:
        fate_selection.set(el,colormap[f])


mc.save()
mc.curate()

