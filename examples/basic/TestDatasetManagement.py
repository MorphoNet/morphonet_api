import morphonet

""" This file show the basic functions to interact with your dataset

"""

""" Login to MorphoNet

"""
#try correct connextion
mn_login="login"
mn_password="password"
mn=morphonet.Net(mn_login,mn_password)

""" List the user dataset

"""
print("-------------------------------")
print("List of dataset i can access")
mn.list_dataset()
input("Press Enter to continue...")
print("-------------------------------")
print("List of my dataset")
mn.list_my_dataset()

""" Enter your dataset name than select it

"""
print("-------------------------------")
print("Select a dataset by name")
d_name = ""
while d_name != "next":
    d_name = input("Enter a dataset name to select,next to stop this while ")
    if d_name != "next":
        mn.select_dataset_by_name(d_name)

""" Enter your dataset id than select it

"""
print("-------------------------------")
print("Select a dataset by id for test : 1452")
d_id = 999
while d_id != -1:
    d_id = int(input("Enter a dataset id to select,-1 to stop this while "))
    if d_id != -1:
        mn.select_dataset_by_id(d_id)
print("-------------------------------")

""" Once it's selected, you can share with a user by its id, in read mode only

"""
print("Share with an existing user (reader)")
mn.share_dataset_with_user(30,0)
input("Press Enter to continue...")

""" Or you can share it in edit mode too

"""
print("-------------------------------")
print("Share with an existing user (manager)")
mn.share_dataset_with_user(56,1)
input("Press Enter to continue...")
print("-------------------------------")

""" You can share in the exact same way but using a group this time (group can be created or modified in you user page on MorphoNet

"""
print("-------------------------------")
print("Share with an existing group (reader)")
mn.share_dataset_with_group(56,0)
input("Press Enter to continue...")
print("-------------------------------")

""" You can add a description for your dataset

"""
print("-------------------------------")
print("Update description for the dataset")
mn.upload_description("New description for api test")
input("Press Enter to continue...")

""" You can update different values for your dataset, in order : name, begin time point, end time point , id id NCBI category (if needed) , type of dataset 0 = observated, 1 = simulated , 2=created

"""
print("-------------------------------")
print("update tests")
mn.update_dataset("TestModifName",1,12,522,2)

""" And you can change the owner of the dataset to someone else , using an id of user. If you wish to know how to retrieve the id , refers to TestUser.py file

"""
owner_id = -1
mn.change_dataset_owner(owner_id)

""" If you don't need it anymore, you can delete the selected dataset 

"""
mn.delete_dataset()