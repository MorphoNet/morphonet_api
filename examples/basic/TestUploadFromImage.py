import morphonet
import os
from morphonet.tools import convert_to_OBJ,imread,read_XML_properties,get_name,get_id_t
import re
""" This file show how to create a dataset , than upload 1 or multiple mesh at the first time point

"""
mn_login="login"
mn_password="password"
mn=morphonet.Net(mn_login,mn_password)
image_path = "/path/to/segmentation/folders/"
datasetname='datasetname'

def get_txt_from_dict(property_name,data, time_begin=-1, time_end=-1,property_type="label"):
        Text= "#"+ property_name + '\n'
        Text += "type:" + property_type + "\n"
        for long_id in data.keys():
            t,id=get_id_t(long_id)
            value=data[long_id]
            if (time_begin == -1 or (time_begin >= 0 and t >= time_begin)) and (
                    time_end == -1 or (time_end >= time_begin and t <= time_end)):
                if property_type == "time":
                    if type(value) == dict or type(value) == list:
                        for longid_ds in value:
                            tds,ids=get_id_t(longid_ds)
                            Text += get_name(t,id) + ':' + get_name(tds,ids)
                            Text += '\n'
                    else:
                        tds, ids = get_id_t(value)
                        Text += get_name(t,id) + ':' + get_name(tds,ids)
                        Text += '\n'
                else:
                    Text += get_name(t, id) + ':' + str(value)
                    Text += '\n'
        return Text

""" Select the dataset : if it exists, clear it than select, otherwise create it than select it

"""

id_dataset=mn.select_dataset_by_name(datasetname)
if mn.id_dataset==-1 :
	print("Dataset not found ")
	mn.create_dataset(datasetname)
else:
	print("Clearing previous dataset")
	mn.clear_dataset()

""" Upload multiple mesh at the time point 0

"""

images = os.listdir().sort()
for objf in os.listdir(image_path):
	if objf.endswith(".xml"):
		properties_by_name = read_XML_properties(os.path.join(image_path,objf))
		for propname in properties_by_name:
			txt = get_txt_from_dict(propname, properties_by_name[propname])
			mn.upload_property(propname,txt)
	if objf.endswith(".mha") or objf.endswith(".nii") or objf.endswith(".inr"):
		print("found image : "+str(objf))
		time = int(re.findall(r'\d+', objf)[-1])
		image = imread(os.path.join(image_path,objf))[:,:,:,0,0]
		obj = convert_to_OBJ(image,t=time,background=1,factor=2)
		print("upload at : "+str(time))
		mn.upload_mesh_at(time,obj)