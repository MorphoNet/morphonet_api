import morphonet

""" This file show you how to upload a dataset on your own server , as long as you add your server on MorphoNet website : https://morphonet.org/distantservers

"""
#try correct connextion
mn_login="login"
mn_password="password"
mn=morphonet.Net(mn_login,mn_password)

datasetname="MyExternalData"

""" delete previous dataset

"""
mn.select_dataset_by_name(datasetname)
if mn.id_dataset!=-1:
	mn.delete_dataset()

""" create a new dataset on a sever your previously created 

"""
mn.create_dataset(datasetname,serveradress="macmanu.freeboxos.fr")

obj=mn.read_mesh("DATASET/Astec-Pm1_post_t001.obj")

""" upload the mesh on your server, but for the dataset

"""
mn.upload_mesh_at(0,obj)