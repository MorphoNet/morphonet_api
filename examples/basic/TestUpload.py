import morphonet
import os


""" This file show how to create a dataset , than upload 1 or multiple mesh at the first time point

"""
mn_login="login"
mn_password="password"
mn=morphonet.Net(mn_login,mn_password)

""" Select the dataset : if it exists, clear it than select, otherwise create it than select it

"""
datasetname='TESTUPLOADMULTIPLEMESHES'
id_dataset=mn.select_dataset_by_name(datasetname)
if mn.id_dataset==-1 :
	mn.create_dataset(datasetname)
else:
	mn.clear_dataset()

""" Upload multiple mesh at the time point 0

"""
for objf in os.listdir('DATASET/Multiple_Objs/'):
	if objf.find(".obj"):
		obj = mn.read_mesh('DATASET/Multiple_Objs/'+objf)
		mn.upload_mesh_at(0,obj)