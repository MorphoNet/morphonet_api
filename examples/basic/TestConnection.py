import morphonet
from morphonet.tools import load_credentials

""" This file show you the different way to try to connect to MorphoNet

"""


""" Using login and password

"""
mn_login="login"
mn_password="password"
mn=morphonet.Net(mn_login,mn_password)

""" Using a config file

"""
json_path = "DATASET/credentials.json"
mn_login,mn_password = load_credentials(json_path)
mn2 = morphonet.Net(mn_login,mn_password)