import morphonet

""" This file is an example of how to use MorphoPlot curation system, for a data on your hard drive. You just need to copy and edit this file, than start it running : python3 TestMorphoPlot.py

"""

""" Path to raw images files name , where time step is replaced for automatic completion. for example if images names are : fuse_t000.inr.gz , fuse_t001.inr.gz, fuse_t010.inr.gz, ....  writting fuse_t{:03d}.inr.gz as a value for raw_files is enough

"""
raw_files = "DATASET/PLOT/RAW/fuse_t{:03d}.inr.gz"

""" Path to segmentation image files name , where time step is replaced for automatic completion. for example if images names are : seg_t000.inr.gz , seg_t001.inr.gz, seg_t010.inr.gz, ....  writting seg_t{:03d}.inr.gz as a value for segment_files is enough

"""
segment_files = "DATASET/PLOT/SEG/raw_t{:03d}.inr.gz"
""" Path to properties file, with xml format as described in MorphoNet format help

"""
properties_file = "DATASET/PLOT/properties.xml"

""" This signal to plot system what will be the start time point to look into the image files

"""
begin = 0

""" This signal to plot system what will be the end time point to look into the image files. All interger values between start and end will be loaded in MorphoPlot if image exists

"""
end = 0

""" This create the MorphoPlot instance, waiting for values and waiting for the start of the curation

Parameters
----------
start_browser : bool
    if True, starting the curation will load firefox browser with the curation page open
    
clear_temp : bool
    MorphoNet use temporary files to increase the speed of the loading. If True, this will clear previous temporary files and create them again when starting curation (so will be slower)

"""
mc=morphonet.Plot(start_browser=True)

""" This call will bind the values created before to the curation, but will still wait for the start of the curation

Parameters
----------
begin : int
    First time point to load for curation (same begin than previously explained above)

end : int
    Last time point to laod for curation, every int between begin and end. Every interger between begin and end will be considered as a time point. If you have only one time point, set end=begin
    
background : int (optional)
    Will explicitly tell the curation what values of the segmentation images correspond to background. Change this value only if you know what you are doing.

segment : string
    Path written above to segment files in your hard drive
    
raw : string
    Path written above to raw images files in your hard drive

xml_file : string
    Path written above to properties file in your hard drive

factor : int
    decimation factor applied to the data, a high value will generate data faster but with a lower quality

memory : int
    the number of time point the system can hold in memory, if more time points are loaded, the oldest loaded one will be unloaded (and will automatically reload if needed)
"""
mc.set_dataset(begin=begin,end=end,background=1,segment=segment_files,raw=raw_files,xml_file=properties_file,factor=5)

""" This call will create a new property to be used in the curation

Parameters
----------
name : string
    name of the property

type : string
    type of information to load : string , float , selection , time , group , ...
"""
prop=mc.create_property("preloaded_empty","string")

""" Than curation is started ! Data is computed and will be sent to a MorphoNet viewer instance ready for a curation
"""
mc.curate()
