import morphonet

""" MorphoNet has a system named primitive that allow you to upload a mesh as a primitive, and instantiate this primitive instead of instantiating multiple times the same mesh. This is very efficient for simulation

"""

mn_login="login"
mn_password="password"
mn=morphonet.Net(mn_login,mn_password)


""" Let's create a dataset with only 1 time step

Parameters
----------

name : string
    name of the new dataset

minTime : int
    begin time point 

maxTime : int
    end time point , here the same than begin because we don't have time informations
"""

mn.create_dataset("api_test_primitive",minTime=0,maxTime=0)


""" Than we will load into memory the primitives source we want to upload

"""

prim1 = mn.read_mesh("DATASET/Multiple_Objs/obj502.obj")
prim2 = mn.read_mesh("DATASET/Multiple_Objs/obj503.obj")

""" Than we can upload the mesh that will be the primitive in the dataset 

Parameters
----------

name : string
    Name of the primitive
obj : bytes
    3d data of the primitive
"""

mn.upload_primitive("object1",prim1)
mn.upload_primitive("object2",prim2)

""" Than we can upload the mesh for the time point that use the primitives. You can check on MorphoNet format help to create a mesh file with the correct format to use primitives

"""

mesh1 = mn.read_mesh("DATASET/Multiple_Objs/obj504.obj")
mn.upload_mesh_with_primitive_at(0,mesh1)

""" If a primitive is not used you can delete a primitive by it's name for the dataset

Parameters
----------

name : string
    name of the primitive to delete
"""

mn.delete_primitive("object1")

""" Or you can delete all primitive of current dataset

"""

mn.delete_primitives()