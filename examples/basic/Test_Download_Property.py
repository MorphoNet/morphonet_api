import morphonet
import os

""" This file show how to retrieve a specific property on the server and save the property and all its curration

"""
mn_login="login"
mn_password="password"
mn=morphonet.Net(mn_login,mn_password)

""" Select the dataset, mandatory to interact with properties. Here we select the dataset number 1.

"""
mn.select_dataset_by_id(1)

""" For the selected dataset, we get the property uploaded with id 15  + the list of curated value for the property

"""
prop_annotated=mn.get_property_annotated_by_id(15)

""" We save the property on the hard drive

"""
f=open('property_15_dataset_1.txt','w')
f.write(prop_annotated)
f.close()
