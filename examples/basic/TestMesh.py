import morphonet

""" This file show how to check the mesh status on the dataset

"""
#try correct connextion
mn_login="login"
mn_password="password"
mn=morphonet.Net(mn_login,mn_password)

""" Select your dataset

"""
print("-------------------------------")
print("Select a dataset by id for test : 1452")
mn.select_dataset_by_id(1)

""" Retrieve the number of mesh for time 1, for quality 1 at channel 0

"""
print("-------------------------------")
print("Get number mesh at t = 1 quality = 1 and channel = 0")
mn.get_number_of_mesh_at(1,1,0)
input("Press Enter to continue...")

""" Retrieve 3d data for the same mesh

"""
print("-------------------------------")
print("Get mesh at t = 1 quality = 1 and channel = 0 (existing but none obj)")
print(mn.get_mesh_at(1,1,0))
input("Press Enter to continue...")



""" If you need to add a mesh , you need first of all to read the data in a file

"""
obj = mn.read_mesh("DATASET/Multiple_Objs/obj502.obj")

""" Than you can upload it 

Parameters
----------
time : int
    time point that will receive the mesh

obj : bytes
    3d data of the mesh itself
    
quality : int
    for a time point of the dataset, you can have multiple quality. Default value is 0
"""
time = 0
mn.upload_mesh_at(time,obj,quality=0)

""" For a reason you may need to clear a mesh for a specific time point

Parameters
----------
time : int
    time point o be cleared

quality : int
    for a time point of the dataset, you can have multiple quality. Default value is 0
    
"""

mn.clear_mesh_at(time,quality=0)