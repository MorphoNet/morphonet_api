import morphonet

""" This file show you how to access users or group in MorphoNet

"""
#try correct connextion
mn_login="login"
mn_password="password"
mn=morphonet.Net(mn_login,mn_password)
print(mn.id_people)

""" You can choose one user by it's name

"""
print("-------------------------------")
print("Select a user by name")
d_name = ""
while d_name != "next":
    d_name = input("Enter a user name to select,next to stop this while, format : Name surname ")
    if d_name != "next":
        print(mn.get_guy_by_name(d_name))

""" Than in the same way, you can choose one user by it's name

"""

print("-------------------------------")
print("Select a group by name")
g_name = ""
while g_name != "next":
    g_name = input("Enter a group name to select,next to stop this while, format : Name")
    if g_name != "next":
        print(mn.get_group_by_name(g_name))
print("-------------------------------")